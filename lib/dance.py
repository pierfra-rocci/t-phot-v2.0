#! /usr/bin/env python

import sys,os
from astropy.io import fits,ascii
from subprocess import call
import shutil

def main(templcat,refname,imname,psfname,zonesize,tolerance,
         scale_ratio,lutname,diagname,logname,kerntxt,FFT,mkext,
         nneighinterp,fitlog):

    # Get Xoff and Yoff and sizes
    Xoff=0
    Yoff=0    
    LoRes=fits.open(refname)
    try:
        Xoff=LoRes[0].header['XOFF']
        Yoff=LoRes[0].header['YOFF']
    except KeyError:
        print "WARNING: No Xoff/Yoff in LRI, assuming 0"

    """
    try:1
        os.mkdir('multikern')
    except OSError:
        print "WARNING: Error creating multikernel directory, directory already exists"
    """

    if kerntxt:
        txt=1
    else:
        txt=0
    if FFT:
        iFFT=1
    else:
        iFFT=0 

    if not mkext:
        prefix=psfname
        while ("/" in prefix):
            param,prefix=prefix.split("/",1)
        while ("." in prefix):
            prefix,param=prefix.split(".",1)
        mkcatname='None'
        multicutscatname='None'
    else:
        prefix='None'
        psfname='None'
        mkcatname='None'
        multicutscatname='None'

    # Check if background must be added
    bkg=str(0.0)
    try:
        f=open(fitlog)
        ff=f.readlines()
        for line in ff:
            if "Background" in line:
                junk,bkg=line.rsplit(":",1)
                bkg,junk=bkg.split(")",1)
                print "With background per pixel:",bkg
                break
    except:
        print "No background"
                
    cmd=' '.join(['tphot_dance',refname,imname,"5 5",str(zonesize),
                  str(zonesize),str(tolerance),lutname,diagname,
                  psfname,prefix,"multikern",str(Xoff),str(Yoff),
                  str(scale_ratio),str(txt),str(iFFT),
                  templcat,str(nneighinterp),bkg])
    print cmd
    stat=call(cmd,shell=True)        
    if not (stat==0):
        print "ERROR during execution of tphot_dance routine."
        print "Aborting."
        sys.exit()

           

if __name__ == '__main__':
    import sys, futil

    if len(sys.argv) == 2:
        d=futil.get_pars(sys.argv[1])
        fitlog,junk=d['fitpars'].split('.',1)
        fitlog=fitlog+'_1.log'
        main(d['templatecat'],
             d['loresfile'],
             d['modelfile'],
             d['kernelimage'],
             int(d['dzonesize']),
             float(d['maxshift']),
             int(d['relscale']),
             d['kernellookup'],
             d['ddiagfile'],
             d['logfile'],
             d['kerntxt'],
             d['dancefft'],
             d['mkext'],
             d['nneighinterp'],
             fitlog
        )
    else:
        main(sys.argv[1:])
