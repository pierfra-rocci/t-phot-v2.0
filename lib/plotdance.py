#! /usr/bin/env python

import sys,os
import numpy as N
from astropy.io import ascii
import matplotlib.pyplot as plt

def main(dancefile):
    c=ascii.read(dancefile)
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.quiver(c['cx'],c['cy'],c['xshift'],c['yshift'])
    nam=dancefile+'_plot.eps'
    fig.savefig(nam, format='eps', dpi=1000)

if __name__ == '__main__':
    dancefile=sys.argv[1]
    if len(sys.argv)==3:
        main(dancefile)
