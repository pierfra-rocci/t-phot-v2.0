#!/usr/bin/python

"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as N
from numpy import diag
import sextutils, futil
import os,sys
import shutil
from astropy.io import fits

class covarcell:
    def __init__(self, id, objects, covar):
        self.id=id
        self.objects=objects
        self.covar=covar
        self.diag=diag(covar)
        self.nobj=len(self.objects)
        self.obdict={}
        for k in range(self.nobj):
            self.obdict[self.objects[k]]=k
        
    def __str__(self):
        return "%i   %i   %s"%(self.id, len(self.objects), str(self.covar.shape))

    def fitswrite(self,fname,**kwds):
        """ Write to a FITS file with the covariance array stored in an
        image extension, and the ids stored in a table extension.
        Preliminary implementation is sleazy & should be cleaned up.
        Also would be nice to use extension names."""

        phdu=fits.PrimaryHDU(self.covar)
        #phdu.header.update('cellid',self.id)
        phdu.header['cellid']=self.id
        for k in kwds:
            #phdu.header.update(k,kwds[k])
            phdu.header[k]=kwds[k]
            
        c1=fits.Column(name='object',format='J',array=self.objects)
        cdef=fits.ColDefs([c1])
        #tbhdu=fits.new_table(cdef)
        tbhdu=fits.BinTableHDU.from_columns(cdef)
        tbhdu.name='IDS'
        hdulist=fits.HDUList([phdu,tbhdu])
        hdulist.writeto(fname)

    def covarworst(self,obid):
        """ Computes the figure of merit abs(covar[j,k])/var[k,k], and
        returns this value for the worst offender, along with its id."""
        if self.nobj > 1:
            #Find the object 
            k=self.obdict[obid]
            #Compute the vector of interest
            cvect=abs(self.covar[:,k])/self.diag[k]
            #Exclude yourself
            notme=((self.objects != obid))
            #Determine the highest value
            max_ratio=cvect[notme].max()
            #and the id of the corresponding object
            idx=((cvect == max_ratio))
            worst_neighbor = self.objects[idx]

            if N.isnan(max_ratio):
                return 'Nan', 0
            else:
                return max_ratio, worst_neighbor[0]
            
        else:
            return 0.0, obid
                                        

    
    def covarthresh(self,obid,thresh):
        """ Checks all objects in the cell for their covariance with
        the passed-in object. Test against the condition
              abs(covar[j,k])/var[k,k] >= thresh
        where object k is the object passed in.
        Returns a list of the covariant object's ids. """

        #Find the object in question
        k=self.obdict[obid]

        #Compute the vector of interest
        cvect=abs(self.covar[k,:])/self.diag[k]

        #See if any objects pass the covariance test
        idx=((cvect >= thresh) & (self.objects != obid))

        #If so, return a list of them
        if idx.sum() > 0:
            return self.objects[idx]
    
def getnextcell(fh):
    """ The text file must already be opened on the file handle. """

    line=fh.readline()
    if line == '':
        raise IOError,"End of file"
    
    #Get the cell id
    while not line.startswith('#Cell'):
        line=fh.readline()
        if line == '':
            raise IOError,"End of file"
    
    cellid = int(line.split(':')[-1])

    #Get the array dims
    line=fh.readline()
    if line == '':
        raise IOError,"End of file"

    junk,nx,ny=line.split()
    nx=int(nx)-1
    ny=int(ny)
    ids=N.zeros((nx,),'Int32')
    covar=N.zeros((nx,ny),'Float32')

    #Skip till the data starts
    while not line.startswith('#Covar'):
        line=fh.readline()
        if line == '':
            raise IOError,"End of file"

    #Here we go
    for iy in range(ny):
        line=fh.readline()
        if line == '':
            raise IOError,"End of file"

        cols=line.split()
        ids[iy]=int(cols[0])
        for ix in range(nx):
            covar[ix,iy]=float(cols[ix+1])

    return covarcell(cellid,ids, covar)

def get_covar(fname):
    fh=open(fname)
    cdict={}
    e=None
    while e is None:
        try:
            tmp = getnextcell(fh)
            cdict[tmp.id]=tmp
        except IOError,e:
            break
    fh.close()
    return cdict



def run(covarname,catname,cdict=None,cat=None):
    if cdict is None:
        cdict=get_covar(covarname)
    if cat is None:
        cat=sextutils.se_catalog(catname)
    outlow=open('%s_%s'%(os.path.basename(catname),'covar_0.5'),'w')
    outhigh=open('%s_%s'%(os.path.basename(catname),'covar_1.0'),'w')
    cat.covarlowflag=N.zeros((len(cat),))
    cat.covarhighflag=N.zeros((len(cat),))
    for ob in cat:
        mycell=cdict[cat.cell[ob]]
        lovar=mycell.covarthresh(cat.objectid[ob],0.5)
        if lovar is not None:
            cat.covarlowflag[ob]=1
            funkywrite(outlow,cat.objectid[ob],lovar)
        highvar=mycell.covarthresh(cat.objectid[ob],1.0)
        if highvar is not None:
            cat.covarhighflag[ob]=1
            funkywrite(outhigh,cat.objectid[ob],highvar)
        print "Object %d: high %s low %s "%(cat.objectid[ob],lovar,highvar)
    outlow.close()
    outhigh.close()
    return cdict, cat




def run2(covarname,catname,cdict=None,cat=None):
    if cdict is None:
        cdict=get_covar(covarname)
    if cat is None:
        cat=sextutils.rw_catalog(catname)

    #Object dictionary
    cat.obdict={}
    for i in range(len(cat)):
        cat.obdict[cat.objectid[i]]=i

    #New fields
    cat.addemptycolumn('maxcovar','Float32')
    cat.addemptycolumn('maxcvid','Int32')
    cat.addemptycolumn('maxcvflux','Float32')

    for ob in cat:
        obid=cat.objectid[ob]
        print obid
        mycell=cdict[cat.cell[ob]]
        print mycell
        maxcv, mcv_id=mycell.covarworst(obid)
        cat.maxcovar[ob]=maxcv
        cat.maxcvid[ob]=mcv_id
        cat.maxcvflux[ob]=cat.fitqty[cat.obdict[mcv_id]]

    return cdict, cat
                                           
def funkywrite(fh,id,vararray):
    fh.write("%5d"%id)
    for ob in vararray:
        fh.write("%5d"%ob)
    fh.write("\n")

def gen_covar_fits(fname,dirname):
    if os.path.isdir(dirname):
        print "WARNING: Removing existing directories"
        shutil.rmtree(dirname)
    os.mkdir(dirname)
    cdict=get_covar(fname)
    for k in cdict:
        cell=cdict[k]
        cell.fitswrite("%s/%d.fits"%(dirname,cell.id),cvfile=fname)

if __name__ == '__main__':
    fname, dirname = sys.argv[1:]
    gen_covar_fits(fname,dirname)
