#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "fits.h"
#define NR_END_ME 1
#define FREE_ARG_ME char*
#include "structs.h"

#define FLUXPRIOR

Grid::Grid(long xs, long ys, long bx, long by) {
  count = 0; maxelem = 0;
  xbin = bx; ybin = by;
  xsize = (xs/xbin)+1; ysize = (ys/ybin)+1;
  grid = gmatrix(0,ysize-1,0,xsize-1);
}

Grid::Grid(char infile[STRING_LENGTH],long xs, long ys, long bx, long by) { //, int Xoff, int Yoff) {
  // xs, ys are the sizes of the full input image
  // bx, by are the bin sizes /// THEY ARE FIXED! I.xsize/20, I.ysize/20
  // scale is the relative pixel scale between the hires catalog & lores image
  // 28 mar 03: Modified to actually count objects, instead of using object id.
  // 9 May 03: modified to read in only id, x, y, flux, in that order.
  // 17 Mar 04: Modified to add one to the grid size so as not to lose any
  //            objects that would otherwise appear on the edges.
  long oldobjid = -10;
  char inputLine[LINE_SIZE];
  long objid = 0;
  count = 0;
  maxelem = 0;
  int xmin, ymin, xmax, ymax;
  double tx, ty, tbg, flux, detf;
#ifdef FLUXPRIOR
  float flagpriorflux,priorflux,errpriorflux;
#endif
  xbin = bx; ybin = by;
  xsize = (long)ceil((float)xs/(float)xbin) + 1; 
  ysize = (long)ceil((float)ys/(float)ybin) + 1;

  grid = gmatrix(0,ysize-1,0,xsize-1);
  long l=0,m=0;
  ifstream objin(infile);
  if (!objin) cout << "Bad input file: " << infile << " in GRID" << endl;
  else {
    while (objin.peek() == '#') {
      objin.getline(inputLine,LINE_SIZE);
    }
    objin >> objid;
    cout << "Reading templates list: " << infile <<endl;
    while (oldobjid != objid && objin) {
      count++;
      
      objin >> tx >> ty >> xmin >> ymin >> xmax >> ymax >> detf >> flux;
      xmin=xmin-1; 
      ymin=ymin-1; 
      xmax=xmax-1; 
      ymax=ymax-1; 

#ifdef FLUXPRIOR
      objin >> flagpriorflux >> priorflux >> errpriorflux;
#endif
      objin.getline(inputLine,LINE_SIZE);
      
      l = (long)floor(tx/(float)xbin);
      m = (long)floor(ty/(float)ybin);
#ifdef FLUXPRIOR
      grid[m][l]->push(objid,(float)tx,(float)ty,xmin,ymin,xmax,ymax,(float)detf,(float)flux,(float)flagpriorflux,(float)priorflux,(float)errpriorflux,0);
#else
      grid[m][l]->push(objid,(float)tx,(float)ty,xmin,ymin,xmax,ymax,(float)detf,(float)flux,0);
#endif      

      if (grid[m][l]->size > maxelem) 
	maxelem = grid[m][l]->size;
      oldobjid = objid;
      objin >> objid;
      
    }

  }
  objin.close();
}

Grid::~Grid() {
  free_gmatrix(grid,0,ysize-1,0,xsize-1);
}

gNode* Grid::Find(int num, int x, int y) {
  int i,j,done=0;
  gNode *current;
  current = grid[y][x]->Top();

  while (current != NULL) {
    if (current->Num() == num) return current;
    current = grid[y][x]->Next(current);
  }
  for (i=0;i<xsize;i++)
    for (j=0;j<ysize;j++)
      if ((i!=x) || (j!=y)) {
	current = grid[j][i]->Top();
	while (current != NULL) {
	  if (current->Num() == num) return current;
	  current = grid[j][i]->Next(current);
     	}
      }
  cout << "Warning: I could not locate object number " << num << endl;
  cout << "Returning a NULL pointer -- ";
  cout << endl;
  return NULL;
}

void Grid::list() {
  gNode *temp;
  for (int i = 0; i<xsize; i++)
    for (int j = 0; j<ysize; j++) {
      temp = grid[j][i]->Top();
      while(temp != NULL) {
	//cout << "Element: " << i << " " << j  << " " << temp << endl;
	temp=grid[j][i]->Next(temp);
      }
    }
}

long Grid::Count() {return count;}

long* Grid::objlist() {
  gNode *temp;
  long* mylist;
  int k=0;
  mylist = (long *)malloc(count*sizeof(long));

  for (int i = 0; i<xsize; i++)
    for (int j = 0; j<ysize; j++) {
      temp = grid[j][i]->Top();
      while(temp != NULL) {
        mylist[k]=temp->Num();
	temp=grid[j][i]->Next(temp);
	k++;
      }
    }
  return mylist;
}


List*** Grid::gmatrix(long nrl, long nrh, long ncl, long nch) {
  long i,j,k, nrow = nrh-nrl+1, ncol = nch-ncl+1;
  List ***L;
  L = (List ***) malloc((size_t)(nrow+NR_END_ME)*sizeof(List**));
  if (!L) {
    cout << "bad: unable to allocate memory in Grid::gmatrix, step 1" << endl;
  }
  L += NR_END_ME;
  L -= nrl;
  L[nrl]=(List **) malloc((size_t)(nrow*ncol+NR_END_ME)*sizeof(List*));
  if (!L) {
    cout << "bad: unable to allocate memory in Grid::gmatrix, step 2" << endl;
  }
  L[nrl] += NR_END_ME;
  L[nrl] -= ncl;

  for(i=nrl+1;i<=nrh;i++) L[i]=L[i-1]+ncol;
  for (j = nrl; j<=nrh; j++)
    for (k = ncl; k<=nch; k++)
      L[j][k]  = new List();


  return L;
}

void Grid::free_gmatrix(List ***L, long nrl, long nrh, long ncl, long nch) {
  long i,j;
  for (i = nrl; i<=nrh; i++)
    for (j = ncl; j<=nch; j++) {
      delete L[i][j];
    }
  free((FREE_ARG_ME) (L[nrl]+ncl-NR_END_ME));
  free((FREE_ARG_ME) (L+nrl-NR_END_ME));
}

int * Grid::Contains(long xc, long yc) {
  // matrix is indexed from 0, not from 1.
  int * cell_coords;
  cell_coords=ivector(1,2);
  cell_coords[1] = xc/xbin; // + 1; //integer division truncates by default;
  cell_coords[2] = yc/ybin; // + 1; // there seems to be no way to force it to be sure.
  return cell_coords;
  }







