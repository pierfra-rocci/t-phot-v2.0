// Header file containing the prototypes for the data structures
// used to make a matrix of linked-lists
//
// Last Update: 28 Jan 1999

#ifndef STRUCTS_H
#define STRUCTS_H
#include "box.h"

#define FLUXPRIOR

class gNode{
  friend class List;
  friend class Box;
  friend ostream &operator<<(ostream &stream, gNode *g);

 public:
  gNode();
  gNode(long,float,float,float,float,float);
  gNode(long,float,float,float,float,float,gNode*);
  gNode(long,float,float,float,float,float,int,gNode*);
  gNode(long,long,int,float,float,float,float,float,gNode*);
  gNode(long,float,float,int,int,int,int,float,float,int,gNode*);
#ifdef FLUXPRIOR
  gNode(long,float,float,int,int,int,int,float,float,
	float,float,float,int,gNode*);
#endif
  ~gNode();

  void  get(long *,float *, float *, float *, float *, float *, int *);
  void  getCoords(float *,float *);
  void  getLimits(int*, int*, int*, int*);
#ifdef FLUXPRIOR
  void  getfluxprior(float*,float*,float*);
#endif
  long  getGroup();
  float getBG();
  float getTBG();
  void  setTBG(float);
  float getSExFlux();
  void  setISOFlux(float);
  float getISOFlux();
  float getScale();
  void  changeGroup(long);
  void  addGroupMems(int);
  void  setGroupMems(int);
 int   GroupSize();
  int   isFlagged();
  void  setDistanceFlag(int);
  long  Num();
  void setRange(Fits*);
  Box* Range();

private:
  long  objnum,groupnum;
  float x,y,bg,tbg,sexflux,isoflux;
  int xmin,ymin,xmax,ymax;
#ifdef FLUXPRIOR
  float fpf,pf,epf;
#endif
  int   
    numInGroup,
    distanceFlag;
  /*********************************************************************
   * distanceFlag =  0: original value. No fit has yet been done
   * distanceFlag =  1: Fit has been done -- object not near border
   * distanceFlag = -1: Fit has been done -- object near border, fit again!
   *********************************************************************/
  float fwhm,SCALE;
  Box *xyrange;
  gNode *next;
};


//...................................................................

class List {
  friend class Grid;

 public:
  List();
  List(gNode *);
  ~List();

  long Size();
  void push(long,float,float,float,float,float,int);
#ifdef FLUXPRIOR
  void push(long,float,float,int,int,int,int,float,float,float,float,float,int);
#else
  void push(long,float,float,int,int,int,int,float,float,int);
#endif
  void build(long,long,int,float,float,float,float,float,float);
  void pushToEnd(long,float,float,float,float,float,int,long);

  gNode *Find(long);
  gNode *pop();
  gNode *Top();
  gNode *Next(gNode *);

 private:
  long size;
  gNode *top;
};  

//...................................................................
class Grid {
  // xsize is the number of xelements so that Grid[1..ysize][1..xsize]
  // xbin, ybin are the number of pixels assigned to each grid element
  friend class tLibrary;
 public:
  List ***grid;
  Grid(long,long,long,long);
  Grid(char [],long,long,long,long); //,int,int);
  ~Grid();

  long xsize,ysize;      // dimensions of grid
  long count,maxelem;
  long xbin, ybin;       // size of grid cell


  gNode* Find(int,int,int);
  int* Contains(long, long);  // returns location of cell containing the x, y passed in
  void list();
  long *objlist();
  long Count();
  //  void setRanges(tLibrary*);

 private:

  List ***gmatrix(long,long,long,long);
  void free_gmatrix(List ***,long,long,long,long);
};

#endif
