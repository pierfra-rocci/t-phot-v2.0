#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "fits.h"
#include "structs.h"
#include "par.h"
#define XGRID_SIZE 400
#define YGRID_SIZE 400
#define TINY 1.0e-20;
#include "funcs.h"

double Funcs::sq(double x) { return x*x; }

void funcs(double *x, double a[], int m, int fit_to_bg) {
  // this is used by the chi^2 fitting routine - see numerical recipes
  int r=0;
  if (fit_to_bg > 0) {
    a[1] = 1.0; // dc term (ie sky background)
    for (int i = 2; i<= m ; i++)
      a[i] = x[i-1];
  } else { // otherwise we just return x as is.
    for (int i = 1; i <= m ; i++)
      a[i] = x[i];
  }
}



/********************************************************************
 *
 * Singular Value Decomposition
 *
 ********************************************************************/


#define MAX_IT 1000
#define TOL 1.0e-9
// Used to be TOL 1.0e-5

void Funcs::svdfit(double  *x[], double y[], double sig[], int ndata, 
		  double a[],int ma,double **u, double **v, double w[], 
		  double *chisq, void (*funcs)(double [], double [], int, int),
		  int fit_to_bg) {
  // x[1..ndata], y[1..ndata], sig[1..ndata] -> arrays of data points/errors
  // a[1..ma] fitting parameters (coefficients)
  // funcs - fitting function

  int i,j;
  double wmax,tmp,thresh,sum,*b,*afunc;
  
  b=dvector(1,ndata);
  afunc=dvector(1,ma);
  for (i=1;i<=ndata;i++) {
    (*funcs)(x[i],afunc,ma,fit_to_bg);
    tmp=1.0/sig[i];
    for(j=1;j<=ma;j++) u[i][j]=afunc[j]*tmp;
    b[i]=y[i]*tmp;
  }
  svdcmp(u,ndata,ma,w,v);
  wmax=0.0;
  for(j=1;j<=ma;j++) if (w[j] > wmax) wmax=w[j];
  thresh=TOL*wmax;
  for(j=1;j<=ma;j++) if (w[j] < thresh) w[j]=0.0;
  svbksb(u,w,v,ndata,ma,b,a);
  *chisq=0.0;
  for (i=1;i<ndata;i++) {
    (*funcs)(x[i],afunc,ma,fit_to_bg);
    for (sum=0.0,j=1;j<=ma;j++) sum += a[j]*afunc[j];

    // Here is the chi-squared calculation.
    // y[i] = the measured flux in the pixel in the low-resolution image
    // sum = the sum of all the (template*scale factor) in that pixel
    // sig[i] = the rms map at that pixel.
    *chisq += (tmp=(y[i]-sum)/sig[i],tmp*tmp);

  }
  free_dvector(afunc,1,ma);
  free_dvector(b,1,ndata);
}

void Funcs::svbksb(double **u, double w[], double **v, int m, int n, double b[], 
		   double x[]) {
  int jj,j,i;
  double s, *tmp;

  tmp = dvector(1,n);
  for (j=1; j<=n; j++ ) {
    s=0.0;
    if(w[j]) {
      for (i=1;i<=m;i++) s += u[i][j]*b[i];
      s /= w[j];
    }
    tmp[j]=s;
  }
  for (j=1;j<=n;j++) {
    s=0.0;
    for (jj=1;jj<=n;jj++) s += v[j][jj]*tmp[jj];
    x[j]=s;
  }
  free_dvector(tmp,1,n);
}

void Funcs::svdcmp(double **a, int m, int n, double w[], double **v) {
  int flag,i,its,j,jj,k,l,nm;
  double anorm,c,f,g,h,s,scale,x,y,z,*rv1;

  rv1=dvector(1,n);
  g=scale=anorm=0.0;
  for (i=1;i<=n;i++) {
    l=i+1;
    rv1[i]=scale*g;
    g=s=scale=0.0;
    if (i <= m) {
      for (k=i;k<=m;k++) scale += fabs(a[k][i]);
      if (scale) {
	for (k=i;k<=m;k++) {
	  a[k][i] /= scale;
	  s += a[k][i]*a[k][i];
	}
	f=a[i][i];
	g = -SIGN(sqrt(s),f);
	h=f*g-s;
	a[i][i] = f-g;
	for (j=l;j<=n;j++) {
	  for (s=0.0,k=i;k<=m;k++) s += a[k][i]*a[k][j];
	  f=s/h;
	  for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
	}
	for (k=i;k<=m;k++) a[k][i] *= scale;
      }
    }
    w[i]=scale *g;
    g=s=scale=0.0;
    if (i<=m && i !=n) {
      for (k=l;k<=n;k++) scale += fabs(a[i][k]);
      if (scale) {
	for (k=l;k<=n;k++) {
	  a[i][k] /= scale;
	  s += a[i][k]*a[i][k];
	}
	f=a[i][l];
	g = -SIGN(sqrt(s),f);
	h=f*g-s;
	a[i][l]=f-g;
	for (k=l;k<=n;k++) rv1[k]=a[i][k]/h;
	for (j=l;j<=m;j++) {
	  for (s=0.0,k=l;k<=n;k++) s += a[j][k]*a[i][k];
	  for (k=l;k<=n;k++) a[j][k] += s*rv1[k];
	}
	for (k=l;k<=n;k++) a[i][k] *= scale;
      }
    }
    anorm=FMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
  }
  for (i=n;i>=1;i--) {
    if (i < n) {
      if (g) {
	for (j=l;j<=n;j++)
	  v[j][i] = (a[i][j]/a[i][l])/g;
	for (j=l;j<=n;j++) {
	  for (s=0.0,k=l;k<=n;k++) s += a[i][k]*v[k][j];
	  for (k=l;k<=n;k++) v[k][j] += s*v[k][i];
	}
      }
      for (j=l;j<=n;j++) v[i][j] = v[j][i]=0.0;
    }
    v[i][i] = 1.0;
    g=rv1[i];
    l=i;
  }
  for (i=IMIN(m,n);i>=1;i--) {
    l=i+1;
    g=w[i];
    for (j=l;j<=n;j++) a[i][j] = 0.0;
    if (g) {
      g=1.0/g;
      for (j=l;j<=n;j++) {
	for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
	f=(s/a[i][i])*g;
	for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
      }
      for (j=i;j<=m;j++) a[j][i] *= g;
    } else for (j=i;j<=m;j++) a[j][i]=0.0;
    ++a[i][i];
  }
  for (k=n;k>=1;k--) {
    for (its=1;its<=MAX_IT;its++) {
      flag=1;
      for (l=k;l>=1;l--) {
	nm=l-1;
	if ((double)(fabs(rv1[l])+anorm) == anorm) {
	  flag=0;
	  break;
	}
	if ((double)(fabs(w[nm])+anorm) == anorm) break;
      }
      if (flag) {
	c=0.0;
	s=1.0;
	for (i=l;i<=k;i++) {
	  f=s*rv1[i];
	  rv1[i]=c*rv1[i];
	  if ((double)(fabs(f)+anorm) == anorm) break;
	  g=w[i];
	  h=pythag(f,g);
	  w[i]=h;
	  h=1.0/h;
	  c=g*h;
	  s = -f*h;
	  for (j=1;j<=m;j++) {
	    y=a[j][nm];
	    z=a[j][i];
	    a[j][nm]=y*c+z*s;
	    a[j][i]=z*c-y*s;
	  }
	}
      }
      z=w[k];
      if (l==k) {
	if (z < 0.0) {
	  w[k] = -z;
	  for (j=1;j<=n;j++) v[j][k] = -v[j][k];
	}
	break;
      }
      if (its == MAX_IT) {
	cout << "No convergence in " << MAX_IT << " svdcmp interations" << endl;
	cout << "Fix it!" << endl;
      }
      x=w[l];
      nm=k-1;
      y=w[nm];
      g=rv1[nm];
      h=rv1[k];
      f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
      g=pythag(f,1.0);
      f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
      c=s=1.0;
      for (j=l;j<=nm;j++) {
	i=j+1;
	g=rv1[i];
	y=w[i];
	h=s*g;
	g=c*g;
	z=pythag(f,h);
	rv1[j]=z;
	c=f/z;
	s=h/z;
	f=x*c+g*s;
	g=g*c-x*s;
	h=y*s;
	y *= c;
	for (jj=1;jj<=n;jj++) {
	  x=v[jj][j];
	  z=v[jj][i];
	  v[jj][j]=x*c+z*s;
	  v[jj][i]=z*c-x*s;
	}
	z=pythag(f,h);
	w[j]=z;
	if (z) {
	  z=1.0/z;
	  c=f*z;
	  s=h*z;
	}
	f=c*g+s*y;
	x=c*y-s*g;
	for (jj=1;jj<=m;jj++) {
	  y=a[jj][j];
	  z=a[jj][i];
	  a[jj][j]=y*c+z*s;
	  a[jj][i]=z*c-y*s;
	}
      }
      rv1[l]=0.0;
      rv1[k]=f;
      w[k]=x;
    }
  }
  free_dvector(rv1,1,n);
}

double Funcs::pythag(double a, double b) {
  double absa,absb;
  absa=fabs(a); absb=fabs(b);
  if (absa > absb) return absa*sqrt(1.0+sq(absb/absa));
  else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+sq(absa/absb)));
}


void Funcs::svdvar(double **v, int ma, double w[], double **cvm) {
  int k,j,i;
  double sum, *wti;
  
  wti=dvector(1,ma);
  for (i=1;i<=ma;i++) {
    wti[i]=0.0;
    if (w[i]) wti[i]=1.0/(w[i]*w[i]);
  }
  for (i=1;i<=ma;i++) {
    for (j=1;j<=i;j++) {
      for (sum=0.0,k=1;k<=ma;k++) sum += v[i][k]*v[j][k]*wti[k];
      cvm[j][i]=cvm[i][j]=sum;
    }
  }
  free_dvector(wti,1,ma);
}

#undef MAX_IT
#undef TOL

/********************************************************************
 *
 * Gamma Functions
 *
 ********************************************************************/

double Funcs::gammq(double a, double x) {
  double gamser,gammcf,gln;
  
  if (x<0.0 || a<= 0.0) {
    cout << "Invalid arguments in routine gammq" << endl;
    exit(0);
  }
  if (x < (1+1.0)) {
    gser(&gamser,a,x,&gln);
    return 1.0-gamser;
  } else {
    gcf(&gammcf,a,x,&gln);
    return gammcf;
  }
}

#define ITMAX 100
#define EPS 3.0e-7

void Funcs::gser(double *gamser, double a, double x, double *gln) {
  int n;
  double sum,del,ap;

  *gln=gammln(a);
  if (x <=0.0) {
    if (x < 0.0) {
      cout << "x less than 0 in routine gser" << endl;
      exit(0);
    }
    *gamser=0.0;
    return;
  } else {
    ap=a;
    del=sum=1.0/a;
    for (n=1;n<=ITMAX;n++) {
      del += x/ap;
      sum += del;
      if (fabs(del) < fabs(sum)*EPS) {
        *gamser=sum*exp(-x+a*log(x)-(*gln));
        return;
      }
    }
    cout << "a too large, ITMAX too small in routine gser" << endl;
    return;
  }
}

#undef ITMAX
#undef EPS

#define ITMAX 100
#define EPS 3.0e-7
#define FPMIN 1.0e-30

void Funcs::gcf(double *gammcf, double a, double x, double *gln) {
  int i;
  double an,b,c,d,del,h;
  
  *gln=gammln(a);
  b=x+1.0-a;
  c=1.0/FPMIN;
  d=1.0/b;
  h=d;
  for (i=1;i<ITMAX;i++) {
    an= -i*(i-a);
    b += 2.0;
    d=an*d+b;
    if (fabs(d) < FPMIN) d=FPMIN;
    c=b+an/c;
    if (fabs(c) < FPMIN) c=FPMIN;
    d=1.0/d;
    del=d*c;
    h *= del;
    if (fabs(del-1.0) < EPS) break;
  }
  if (i > ITMAX) {
    cout << "a too large, ITMAX too small in gcf" << endl;
    exit(0);
  }
  *gammcf=exp(-x+a*log(x)-(*gln))*h;
}

#undef ITMAX
#undef EPS
#undef FPMIN

double Funcs::gammln(double xx) {
  double x,y,tmp,ser;
  static double cof[6]={76.1800917294716,-86.50532032941677,
    24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
			-0.5395239384953e-5};
  int j;
  
  y=x=xx;
  tmp=x+5.5;
  tmp -= (x+0.5)*log(tmp);
  ser=1.000000000190015;
  for (j=0;j<=5;j++) ser += cof[j]/++y;
  return -tmp+log(2.5066282746310005*ser/x);
}

/********************************************************************
 *
 * Linear minimization Fitting
 *
 ********************************************************************/

void Funcs::lfit(double *x[], double y[], double sig[], int ndat, double a[], 
	  int ia[], int ma, double **covar, double *chisq, 
	  void (*funcs)(double [], double [], int, int), int fit_to_bg)
	  
{
  //  void covsrt(double **covar, int ma, int ia[], int mfit);
  //  void gaussj(double **a, int n, double **b, int m);
  int i,j,k,l,m,mfit=0;
  double ym,wt,sum,sig2i,**beta,*afunc;
  
  beta=dmatrix(1,ma,1,1);
  afunc=dvector(1,ma);
  for (j=1;j<=ma;j++)
    if (ia[j]) mfit++;
  if (mfit == 0) {
    cout << "% ERROR in lfit: no parameters to be fitted" << endl;
    return;
  }
  for (j=1;j<=mfit;j++) {
    for (k=1;k<=mfit;k++) covar[j][k]=0.0;
    beta[j][1]=0.0;
  }
  for (i=1;i<=ndat;i++) {
    (*funcs)(x[i],afunc,ma,fit_to_bg);
    ym=y[i];
    if (mfit < ma) {
      for (j=1;j<=ma;j++)
	if (!ia[j]) ym -= a[j]*afunc[j];
    }
    sig2i=1.0/SQR(sig[i]);
    for (j=0,l=1;l<=ma;l++) {
      if (ia[l]) {
	wt=afunc[l]*sig2i;
	for (j++,k=0,m=1;m<=l;m++)
	  if (ia[m]) covar[j][++k] += wt*afunc[m];
	beta[j][1] += ym*wt;
      }
    }
  }
  for (j=2;j<=mfit;j++)
    for (k=1;k<j;k++)
      covar[k][j]=covar[j][k];
  gaussj(covar,mfit,beta,1);
  for (j=0,l=1;l<=ma;l++)
    if (ia[l]) a[l]=beta[++j][1];
  *chisq=0.0;
  for (i=1;i<=ndat;i++) {
    (*funcs)(x[i],afunc,ma,fit_to_bg);
    for (sum=0.0,j=1;j<=ma;j++) sum += a[j]*afunc[j];
    *chisq += SQR((y[i]-sum)/sig[i]);
  }
  covsrt(covar,ma,ia,mfit);
  free_dvector(afunc,1,ma);
  free_dmatrix(beta,1,ma,1,1);
}


#define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}

void Funcs::covsrt(double **covar, int ma, int ia[], int mfit)
{
  int i,j,k;
  double swap;
  
  for (i=mfit+1;i<=ma;i++)
    for (j=1;j<=i;j++) covar[i][j]=covar[j][i]=0.0;
  k=mfit;
  for (j=ma;j>=1;j--) {
    if (ia[j]) {
      for (i=1;i<=ma;i++) SWAP(covar[i][k],covar[i][j])
      for (i=1;i<=ma;i++) SWAP(covar[k][i],covar[j][i])
      k--;
    }
  }
}

void Funcs::gaussj(double **a, int n, double **b, int m)
{
  int *indxc,*indxr,*ipiv;
  int i,icol,irow,j,k,l,ll;
  double big,dum,pivinv,swap;
  
  indxc=ivector(1,n);
  indxr=ivector(1,n);
  ipiv=ivector(1,n);
  for (j=1;j<=n;j++) ipiv[j]=0;
  for (i=1;i<=n;i++) {
    big=0.0;
    for (j=1;j<=n;j++)
      if (ipiv[j] != 1)
	for (k=1;k<=n;k++) {
	  if (ipiv[k] == 0) {
	    if (fabs(a[j][k]) >= big) {
	      big=fabs(a[j][k]);
	      irow=j;
	      icol=k;
	    }
	  } else if (ipiv[k] > 1) {
	    cout << "% ERROR in gaussj: Singular Matrix-1" << endl;
	    return;
	  }
	}
    ++(ipiv[icol]);
    if (irow != icol) {
      for (l=1;l<=n;l++) SWAP(a[irow][l],a[icol][l])
      for (l=1;l<=m;l++) SWAP(b[irow][l],b[icol][l])
    }
    indxr[i]=irow;
    indxc[i]=icol;
    if (a[icol][icol] == 0.0) {
      cout << "% ERROR in gaussj: Singular Matrix-2" << endl;
      return;
    }
    pivinv=1.0/a[icol][icol];
    a[icol][icol]=1.0;
    for (l=1;l<=n;l++) a[icol][l] *= pivinv;
    for (l=1;l<=m;l++) b[icol][l] *= pivinv;
    for (ll=1;ll<=n;ll++)
      if (ll != icol) {
	dum=a[ll][icol];
	a[ll][icol]=0.0;
	for (l=1;l<=n;l++) a[ll][l] -= a[icol][l]*dum;
	for (l=1;l<=m;l++) b[ll][l] -= b[icol][l]*dum;
      }
  }
  for (l=n;l>=1;l--) {
    if (indxr[l] != indxc[l])
      for (k=1;k<=n;k++)
	SWAP(a[k][indxr[l]],a[k][indxc[l]]);
  }
  free_ivector(ipiv,1,n);
  free_ivector(indxr,1,n);
  free_ivector(indxc,1,n);
}

#undef SWAP

/********************************************************************/

void Funcs::sprsin(double **a, int n, double thresh, unsigned long nmax, double sa[], unsigned long ija[]) {
  void nrerror(char error_text[]);
  int i,j;
  unsigned long k;
  
  for (j=1;j<=n;j++) sa[j]=a[j][j]; /// diagonal elements
  ija[1]=n+2;
  k=n+1;
  for (i=1;i<=n;i++) {
    for (j=1;j<=n;j++) {
      if (fabs(a[i][j]) >= thresh && i != j) {
	if (++k > nmax) nrerror("sprsin: nmax too small");
	sa[k]=a[i][j];
	ija[k]=j;
      }
    }
    ija[i+1]=k+1;
  }
}


void Funcs::ludcmp(double **a, int n, int *indx, double *d)

/// **a is coefficient matrix, n is number of sources

{
	int i,imax,j,k;
	double big,dum,sum,temp;
	double *vv;

	vv=dvector(1,n);
	*d=1.0;
	for (i=1;i<=n;i++) {
		big=0.0;
		for (j=1;j<=n;j++)
			if ((temp=fabs(a[i][j])) > big) big=temp;
		if (big == 0.0) nrerror("Singular matrix in routine ludcmp");
		vv[i]=1.0/big;
	}
	for (j=1;j<=n;j++) {
		for (i=1;i<j;i++) {
			sum=a[i][j];
			for (k=1;k<i;k++) sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
		}
		big=0.0;
		for (i=j;i<=n;i++) {
			sum=a[i][j];
			for (k=1;k<j;k++)
				sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
			if ( (dum=vv[i]*fabs(sum)) >= big) {
				big=dum;
				imax=i;
			}
		}
		if (j != imax) {
			for (k=1;k<=n;k++) {
				dum=a[imax][k];
				a[imax][k]=a[j][k];
				a[j][k]=dum;
			}
			*d = -(*d);
			vv[imax]=vv[j];
		}
		indx[j]=imax;
		if (a[j][j] == 0.0) a[j][j]=TINY;
		if (j != n) {
			dum=1.0/(a[j][j]);
			for (i=j+1;i<=n;i++) a[i][j] *= dum;
		}
	}
	free_dvector(vv,1,n);
}

void Funcs::lubksb(double **a, int n, int *indx, double b[])
{
	int i,ii=0,ip,j;
	double sum;

	for (i=1;i<=n;i++) {
		ip=indx[i];
		sum=b[ip];
		b[ip]=b[i];
		if (ii)
			for (j=ii;j<=i-1;j++) sum -= a[i][j]*b[j];
		else if (sum) ii=i;
		b[i]=sum;
	}
	for (i=n;i>=1;i--) {
		sum=b[i];
		for (j=i+1;j<=n;j++) sum -= a[i][j]*b[j];
		b[i]=sum/a[i][i];
	}
}

void Funcs::choldc(double **a, int n, double p[])
{
	void nrerror(char error_text[]);
	int i,j,k;
	double sum;

	for (i=1;i<=n;i++) {
		for (j=i;j<=n;j++) {
			for (sum=a[i][j],k=i-1;k>=1;k--) sum -= a[i][k]*a[j][k];
			if (i == j) {
				if (sum <= 0.0)
					nrerror("choldc failed");
				p[i]=sqrt(sum);
			} else a[j][i]=sum/p[i];
		}
	}
}

void Funcs::cholsl(double **a, int n, double p[], double b[], double x[])
{
	int i,k;
	double sum;

	for (i=1;i<=n;i++) {
		for (sum=b[i],k=i-1;k>=1;k--) sum -= a[i][k]*x[k];
		x[i]=sum/p[i];
	}
	for (i=n;i>=1;i--) {
		for (sum=x[i],k=i+1;k<=n;k++) sum -= a[k][i]*x[k];
		x[i]=sum/p[i];
	}
}


#define NRANSI
#define EPS 1.0e-14
void Funcs::linbcg(double sa[], unsigned long ija[], unsigned long n, double b[], double x[], int itol, double tol, int itmax, int *iter, double *erro)
{
  void asolve(double sa[], unsigned long ija[], unsigned long n, double b[], double x[], int itrnsp);
  void atimes(double sa[], unsigned long ija[], unsigned long n, double x[], double r[], int itrnsp);
  double snrm(unsigned long n, double sx[], int itol);
  unsigned long j;
  double ak,akden,bk,bkden,bknum,bnrm,dxnrm,xnrm,zm1nrm,znrm;
  double *p,*pp,*r,*rr,*z,*zz;

  p=dvector(1,n);
  pp=dvector(1,n);
  r=dvector(1,n);
  rr=dvector(1,n);
  z=dvector(1,n);
  zz=dvector(1,n);
  
  *iter=0;
  atimes(sa, ija, n,x,r,0);
  for (j=1;j<=n;j++) {
    r[j]=b[j]-r[j];
    rr[j]=r[j];
  }
  znrm=1.0;
  if (itol == 1) bnrm=snrm(n,b,itol);
  else if (itol == 2) {
    asolve(sa, ija, n,b,z,0);
    bnrm=snrm(n,z,itol);
  }
  else if (itol == 3 || itol == 4) {
    asolve(sa, ija, n,b,z,0);
    bnrm=snrm(n,z,itol);
    asolve(sa, ija, n,r,z,0);
    znrm=snrm(n,z,itol);
  } else nrerror("illegal itol in linbcg");
  asolve(sa, ija, n,r,z,0);
  while (*iter <= itmax) {
    ++(*iter);
    zm1nrm=znrm;
    asolve(sa, ija, n,rr,zz,1);
    for (bknum=0.0,j=1;j<=n;j++) bknum += z[j]*rr[j];
    if (*iter == 1) {
      for (j=1;j<=n;j++) {
	p[j]=z[j];
	pp[j]=zz[j];
      }
    }
    else {
      bk=bknum/bkden;
      for (j=1;j<=n;j++) {
	p[j]=bk*p[j]+z[j];
	pp[j]=bk*pp[j]+zz[j];
      }
    }
    bkden=bknum;
    atimes(sa, ija, n,p,z,0);
    for (akden=0.0,j=1;j<=n;j++) akden += z[j]*pp[j];
    ak=bknum/akden;
    atimes(sa, ija, n,pp,zz,1);
    for (j=1;j<=n;j++) {
      x[j] += ak*p[j];
      r[j] -= ak*z[j];
      rr[j] -= ak*zz[j];
    }
    asolve(sa, ija, n,r,z,0);
    if (itol == 1 || itol == 2) {
      znrm=1.0;
      *erro=snrm(n,r,itol)/bnrm;
    } else if (itol == 3 || itol == 4) {
      znrm=snrm(n,z,itol);
      if (fabs(zm1nrm-znrm) > EPS*znrm) {
	dxnrm=fabs(ak)*snrm(n,p,itol);
	*erro=znrm/fabs(zm1nrm-znrm)*dxnrm;
      } else {
	*erro=znrm/bnrm;
	continue;
      }
      xnrm=snrm(n,x,itol);
      if (*erro <= 0.5*xnrm) *erro /= xnrm;
      else {
	*erro=znrm/bnrm;
	continue;
      }
    }
    if (*erro <= tol) break;
  }

  free_dvector(p,1,n);
  free_dvector(pp,1,n);
  free_dvector(r,1,n);
  free_dvector(rr,1,n);
  free_dvector(z,1,n);
  free_dvector(zz,1,n);
}
#undef EPS
#undef NRANSI

/* ------------------------------------------------------------------------------ */

/// this multiplies matrix A by a vector 
void atimes(double sa[], unsigned long ija[], unsigned long n, double x[], double r[], int itrnsp) {
  void dsprsax(double sa[], unsigned long ija[], double x[], double b[],
	       unsigned long n);
  void dsprstx(double sa[], unsigned long ija[], double x[], double b[],
	       unsigned long n);

  if (itrnsp) dsprstx(sa,ija,x,r,n);
  else dsprsax(sa,ija,x,r,n);
}


/// this solves A_tilde * x = B where A_tilde is the preconditioner matrix
void asolve(double sa[], unsigned long ija[], unsigned long n, double b[], double x[], int itrnsp) {
  unsigned long i;

  for(i = 1; i <= n; i++) 
    x[i] = (sa[i] != 0.0 ? b[i]/sa[i] : b[i]);
}

/// this computes vector norms
double snrm(unsigned long n, double sx[], int itol) {
  unsigned long i,isamax;
  double ans;
  
  if (itol <= 3) {
    ans = 0.0;
    for (i=1;i<=n;i++) ans += sx[i]*sx[i];
    return sqrt(ans);
  } 
  else {
    isamax=1;
    for (i=1;i<=n;i++) {
      if (fabs(sx[i]) > fabs(sx[isamax])) isamax=i;
    }
    return fabs(sx[isamax]);
  }
}

/// this multiplies the transpose of matrix a (in sa storage) by vector x giving vector b
void dsprstx(double sa[], unsigned long ija[], double x[], double b[], unsigned long n) {
  void nrerror(char error_text[]);
  unsigned long i,j,k;
  if (ija[1] != n+2) nrerror("mismatched vector and matrix in dsprstx");
  for (i=1;i<=n;i++) b[i]=sa[i]*x[i];
  for (i=1;i<=n;i++) {
    for (k=ija[i];k<=ija[i+1]-1;k++) {
      j=ija[k];
      b[j] += sa[k]*x[i];
    }
  }
}

/// this multiplies matrix a (in sa storage) by vector x giving vector b
void dsprsax(double sa[], unsigned long ija[], double x[], double b[], unsigned long n) {
  void nrerror(char error_text[]);
  unsigned long i,k;
  
  if (ija[1] != n+2) nrerror("dsprsax: mismatched vector and matrix");
  for (i=1;i<=n;i++) {
    b[i]=sa[i]*x[i];
    for (k=ija[i];k<=ija[i+1]-1;k++) b[i] += sa[k]*x[ija[k]];
  }
}



void Funcs::matrix_2_colmaj(double **a, double c[], int M, int N) 
{
	// convert matrix a into an array c with column-major format (for LAPACK routines)
	// a is a matrix as defined in nrutil.h (indices start at 1)
	// M, N are the numbers of rows and columns of a
	// c is modified in place; c should have the length M * N
	int i=0, j=0, k=0;
	for (i=1; i<=M; i++) {
		for (j=1; j<=N; j++) {
			k = (j-1) * M + i - 1;  // note that c is zero-offset
			c[k] = a[i][j];
		}
	}

}

void Funcs::colmaj_2_matrix(double c[], double **a, int M, int N)
{
	// convert a column-major array into a matrix a
	// c should have the length M * N
	// a is modified in place; a should have the dimension M x N
	int i, j, k;
	for (k=0; k<M*N; k++)
	{
		i = k % M + 1;
		j = k / M + 1;
		a[i][j] = c[k];
	}
}

void print_matrix(double a[], int M, int N)
{
	// print a M x N matrix (M rows and N cols)
	// a is stored in COLUMN-MAJOR format
	int i, j, k;
	for (i=0; i<M; i++) {
		for (j=0; j<N; j++) {
			k = i + j * N;
			printf("%.4f ", a[k]);
			}
		printf("\n");
	}
}
