#include <iostream> 
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
//#include "box.h"
#include "fits.h"


Fits::Fits(int x0, int x1, int y0, int y1,int pixtype, float initval) {
  int i,j;
  title = "Temporary";
  PixType = pixtype; totflux=0.0;
  xmin = x0; ymin = y0;
  xsize = x1-x0+1; ysize = y1-y0+1;
  // xcorner = 0; ycorner = 0.0;
  xmax=x1; ymax=y1;
  xyrange.Set(xmin, ymin, xmax, ymax);
  switch(PixType) {
  case TFLOAT :
    data = matrix(y0,y1,x0,x1);
    for (i = xmin; i <= xmax; i++) 
      for (j = ymin; j <= ymax; j++)
	data[j][i] = initval;
    break;
  case TINT :
    idata = imatrix(ymin,ymax,xmin,xmax);
    for (i = xmin; i <= xmax; i++) 
      for (j = ymin; j <= ymax; j++)
	idata[j][i] = 0;
    break;
  case TLONG :
    ldata = lmatrix(ymin,ymax,xmin,xmax);
    for (i = xmin; i <= xmax; i++) 
      for (j = ymin; j <= ymax; j++)
	ldata[j][i] = 0;
    break;
  default:
    cout << "\tUNKNOWN PIXTYPE = " << pixtype << " IN FITS " << endl;
  }
} 


Fits::~Fits() {
  switch(PixType) {
  case TFLOAT :
    free_matrix(data,ymin,ymax,xmin,xmax);
    break;
  case TINT :
    free_imatrix(idata,ymin,ymax,xmin,xmax);
    break;
  case TLONG :
    free_lmatrix(ldata,ymin,ymax,xmin,xmax);
    break;
  default:
    cout << "Something weird in Fits " << title << endl;
    cout << "pixtype = " << PixType << endl;
  }
}






