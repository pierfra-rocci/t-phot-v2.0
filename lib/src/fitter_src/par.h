// Header file controlling the number of parameters
// in the chi^2 fitting
//
// Last Edited: 26 Jan 1999

#ifndef PAR_H
#define PAR_H

class Par {
public:
  double *data;
  int *usedata;
  int size;
  Par(int);
  ~Par();
  Par(double [],int [], int);
};

#endif
