#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "fits.h"
#define NR_END_ME 1
#define FREE_ARG_ME char*

#include "structs.h"

#define FLUXPRIOR

gNode::gNode() { 
  objnum=0; groupnum=0; x=0; y=0; bg=0; sexflux=0; fwhm=0; next=NULL;
  numInGroup=1;
  distanceFlag=0;
  xyrange = new Box();
}

gNode::gNode(long c, float xx, float yy, float bb, float fx, float ff) { 
  objnum = c; groupnum=c; x = xx; y = yy; bg = bb; sexflux=fx; 
  fwhm = ff; next=NULL;
  numInGroup=1;
  distanceFlag=0;
  xyrange = new Box();
}
gNode::gNode(long c, float xx, float yy, float bb, float fx, float ff, 
	     gNode *n) { 
  objnum = c; groupnum=c; x = xx; y = yy; bg = bb; sexflux=fx; 
  fwhm = ff; next=n;
  numInGroup=1; 
  distanceFlag=0;
  xyrange = new Box();
}
gNode::gNode(long c, float xx, float yy, float bb, float fx, float ff, 
	     int flag, gNode *n) { 
  objnum = c; groupnum=c; x = xx; y = yy; bg = bb; 
  sexflux=fx; 
  fwhm = ff; 
  next=n;
  numInGroup=1; 
  distanceFlag=flag;
  xyrange = new Box();
}

#ifdef FLUXPRIOR
gNode::gNode(long c, float xx, float yy, int xxmin, int yymin, int xxmax, 
	     int yymax, float bb, float fx, float fpff, float pff, float epff, 
	     int flag, gNode *n) { 
  objnum = c; groupnum=c; x = xx; y = yy; bg = bb; 
  sexflux=fx;  
  next=n;
  numInGroup=1; 
  distanceFlag=flag;
  xmin=xxmin; ymin=yymin; xmax=xxmax; ymax=yymax;
  xyrange = new Box();
  fpf=fpff;
  pf=pff;
  epf=epff;
}
#else
gNode::gNode(long c, float xx, float yy, int xxmin, int yymin, int xxmax, 
	     int yymax, float bb, float fx, int flag, gNode *n) { 
  objnum = c; groupnum=c; x = xx; y = yy; bg = bb; 
  sexflux=fx; 
  next=n;
  numInGroup=1; 
  distanceFlag=flag;
  xmin=xxmin; ymin=yymin; xmax=xxmax; ymax=yymax;
  xyrange = new Box();
}
#endif

gNode::gNode(long c, long g, int num, float xx, float yy, float bb, float flx, 
	     float scale, gNode *n) { 
  objnum = c; groupnum=g; x = xx; y = yy; bg = bb; sexflux=flx; 
  SCALE = scale;
  fwhm = 0.0; next=n;
  numInGroup=num;
  distanceFlag=0;
  xyrange = new Box();
}

gNode::~gNode() { 
  if (next == NULL) delete next;
  else {
    gNode *temp = next;
    next = NULL;
    delete temp;
    delete next;
  }
}

void gNode::get(long *l, float *xx, float *yy, float *bb, float *fx, 
		float *fw, int *fg) {
  *l = objnum; *xx = x; *yy = y; *bb = bg; *fx=sexflux; *fw = fwhm;
  *fg = distanceFlag;
}

void gNode::getLimits(int *xxmin, int *yymin, int *xxmax, int *yymax) {
  *xxmin=xmin; *yymin=ymin; *xxmax=xmax; *yymax=ymax;
}

#ifdef FLUXPRIOR
void gNode::getfluxprior(float *flagpriorflux, float *priorflux, float *errpriorflux){
  *flagpriorflux=fpf; *priorflux=pf; *errpriorflux=epf;
}
#endif

void gNode::getCoords(float *xx, float *yy) {
  *xx = x; *yy = y;
}

long  gNode::getGroup() { return groupnum; }
long  gNode::Num() { return objnum; }
float gNode::getBG() { return bg; }
float gNode::getTBG() { return tbg; }
float gNode::getSExFlux() { return sexflux; }
float gNode::getISOFlux() { return isoflux; }
float gNode::getScale() { return SCALE; }
void  gNode::setTBG(float t) { tbg = t; }
void  gNode::setISOFlux(float flux) { isoflux = flux; }
void  gNode::changeGroup(long num) { groupnum = num; }
void  gNode::addGroupMems(int newMems) { numInGroup += newMems; }
void  gNode::setGroupMems(int newMems) { numInGroup = newMems; }
void  gNode::setDistanceFlag(int Flag) { distanceFlag = Flag; }
int   gNode::GroupSize() { return numInGroup; }
int   gNode::isFlagged() { return distanceFlag; }

ostream &operator<<(ostream &stream, gNode *g) {
  stream << g->objnum << " " << g->groupnum << " ";
  stream << g->x << " " << g->y << " ";
  stream << g->bg << " " << g->fwhm;
  return stream;
}

void gNode::setRange(Fits *mycutout) {
  *xyrange = mycutout->xyrange;
//   xyrange->Set(mycutout->xcorner, 
//                  mycutout->ycorner,
// 		 mycutout->xcorner+mycutout->xsize,
// 		 mycutout->ycorner+mycutout->ysize);
}

Box* gNode::Range() { return xyrange; }
