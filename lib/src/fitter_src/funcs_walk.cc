
/*
Copyright © 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "fits.h"
#include "structs.h"
#include "par.h"
#include "funcs.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include "tphot.h"
#include "globals.h"


//#define DEBUG
//#define VERBOSE
#define FLUXPRIOR
#define BUFFERSIZE 2048  
using std::vector;

float total, TOL=1.e-9, nsigma=3.0;
int *contaminant, *cl;
double thresh = SMALL_NUMBER;
long nmax;
int itmax = 1000000;
int iter, clipped, cliter;
double tol = INFINITESIMAL_NUMBER;
int itol = 4;
double *sa;
unsigned long *ija;
double erro;

#ifdef FLUXPRIOR
float *flagprior,*fprior,*efprior;
#endif

int buildlinearsystem(double**, double*, 
		      double*, double*, char*, char*, char*, int,
		      long, long, float, long, long, long, long,
		      catalogstruct*, int, int, int);

void apertures(int, double*, double*, 
	       float, int, float*, double*, double*, 
	       int, int, float, float);

double Funcs::getmaxcomp( double **V, int ndim, int tcount ) 
{
  int j;
  double maxcomp;

  maxcomp = fabs(V[tcount][tcount]);
  for (j=1; j<=ndim; j++){
    if (fabs(V[j][tcount]) > maxcomp) {maxcomp=V[j][tcount];}
  }

  return maxcomp;
}

int * Funcs::cellmask(float mask_floor, int max_flag, long *flag, double **temp, int crowdsize, int npix, int &new_npix)
{
  int i,j;
  float sum;
  int *mask;

  // Make a new output array
  mask = ivector(1,npix);
  
  // Walk through the templates
  new_npix = 0;
  for (j=1; j<=npix; j++) {
    // Apply the flag logic first
    if (flag[j] <= max_flag) {
      // If the flag image shows good data, examine the templates
      sum=0.0;
      for (i=1; i<=crowdsize; i++) { 
	if (temp[j][i] > mask_floor){
	  sum+=temp[j][i];}
      }

      if (sum != 0.0) 
	{mask[j]=1;
	  new_npix++;
	}  
      else
	{mask[j]=0;}
    } 
    else {mask[j]=0;} // Otherwise, just mask it out.
  }
  return mask;
}

/* ------------------------------------------------------------------------------
   FUNCTION FOR CELLS ON OBJECTS FITTING
------------------------------------------------------------------------------ */

void Funcs::walkGrid_coo(char *SexFile, char *TemplateDir, 
			 char *LoResFits, char *WeightFits,
			 double *pixels, double *pixels_rms, 
			 long *pixels_flag, long *pixels_seg,
			 int Width, int Height, int USESIG, 
			 char *outfile, char *cellfile, char *apfile, Grid *BigGrid,
			 float RMS_CONSTANT, float BG_CONSTANT, double facZP,
			 int use_cellmask, float mask_floor, int dithercell,
			 int max_flag, int fit_to_bg,
			 int exportCovariance, char *CovarFile, 
			 int XBUFFERSIZE, int YBUFFERSIZE, int FLAGDIST, 
			 int dxopt, int dyopt, 
			 int nc, int nr, int xxx, int yyy, int xrange, int yrange, 
			 int MEPROC, float ThresholdFactor, int linsyssolver, 
			 int CLIP, char *FluxOrderedCatalog, 
			 int *blendedpriors, int nblended, 
			 int naper, float *APER, int nsourcesopt,
			 int *status) {
  
  /*********************************************************************
   *
   *  The Fits images are designated as:
   *    I: The full LoRes image (x,y) values over the whole image
   *    E: The full weight map on the lores image
   *    F: The full flag map on the lores image
   *
   *********************************************************************/

  register int a,b,i,j,k,l,m,n,mm,nn;
  static long GRIDCOUNT = 0;
  long TOTCELLS = 0;
  static int XX=1, YY=2, NRHS=1;
  float maxflux, fluxthresholdcheck=0.9, drfrac=0.3;
  long oldid=-10;
  long 
    crowdsize, growcount, *imflag,
    XMIN, XMAX,
    YMIN, YMAX,
    NXGRIDS,NYGRIDS;
  int 
    XMINo,YMINo,XMAXo,YMAXo,
    XMINP, XMAXP, YMINP, YMAXP,
    index,beginindex,endindex,
    thisid,thisobj,
    idj,jj,fitcount,
    flag=0, kk, ll,
    flaggedCount=0,
    groupedCount=0,
    myi, myj, dither, xditherwidth, yditherwidth,
    x0, y0, oxd, oyd, oxu, oyu,
    xlo, xhi, ylo, yhi, cx, cy,
    bxlo=0, bxhi=0, bylo=0, byhi=0,
    xcorner, ycorner,
    totnumsources,
    *lowcell, *highcell,*thiscell,
    *lowcellsmall, *highcellsmall,
    npix, cellindex, pcounter, coo_to_single=0, coo_fix=0,
    *dofflag, *mask, new_npix=0, **overlap_kl, 
    *objincell, *listincell, **DataInt, *Fitted;
  char pme[STRING_LENGTH];
  double *faper, *errfaper;
  int nap;
  float 
    xx, yy, obx, oby, 
    xc, yc,
    xpos=0.0, ypos=0.0, 
    background=0.0, 
    fwhm=0.0,
    flux=0.0,
    isoflux,
    **Data,
    cellX,cellY,
    diagjj2,distijj2,
    XCEN, YCEN, rcell;
  double 
    **covar, **temp, **newtemp,
    totflux=0.0, oberr, 
    **A,**Acopy,
    sumkl=0.0, sumb=0.0, *tempkl, *tempb, sum=0.0;
  char inputLine[200];
  ofstream datout, covout, cellout, newerr, apout;
  Fits *thisone, *thatone;
  Par *dof; 
  List *theList;  
  char modname[BUFFERSIZE];
  long naxes[2] = {1,1};
  int bitpix, naxis;
  double *pixels_th0;
  
  //FitsManip fitsmanip;
  // for Cholesky decomposition
  double *B,*Bcopy, *p;
  // for LU decomposition
  int *indx;
  double *col, d=1.0;
  // If use LAPACK...
  //double *Acm, *WORK, *dofdata; // column-major array for A
  //__CLPK_integer info=0, LWORK, *ipiv, M;
  //char *transpose="N", *UPLO="L";
  int id, NX, NY, lcx, lcy, hcx, hcy;
  float xmin,xmax,ymin,ymax;
  int stat;
  int xmini, ymini, xmaxi, ymaxi, drX, drY;
  double txi, tyi, tbgi, fluxi, bkgi;
  fitsfile fthis;

  Box cell, newcell;
  vector <gNode *>  crowd;
  long picker, pickerj, tcount;

  catalogstruct *pCatalog;
 
  fitsfile *fptr0;
  long fpixel[2] = {1,1};

  /*********************************************************************
   *
   * funcs returns the m basis functions in a[] evaluated at x 
   * degree of freedom array set with one degree of freedom
   */
  
  void funcs(double *, double [], int, int );
  //*********************************************************************
  
  // Open the files.
  char outfile1[STRING_LENGTH];
  char cellfile1[STRING_LENGTH];
  char CovarFile1[STRING_LENGTH];
  char apfile1[STRING_LENGTH];
  
  sprintf(pme,"_%d",MEPROC);
  snprintf(outfile1,STRING_LENGTH,"%s%s",outfile,pme);
  datout.open(outfile1);
  snprintf(cellfile1,STRING_LENGTH,"%s%s",cellfile,pme);
  cellout.open(cellfile1);
  if (exportCovariance) 
    {
      snprintf(CovarFile1,STRING_LENGTH,"%s%s",CovarFile,pme);
      covout.open(CovarFile1);
    }
  if (naper>0)
    {
      snprintf(apfile1,STRING_LENGTH,"%s%s",apfile,pme);
      apout.open(apfile1);
    }

  int MAXAPER=0;
  for(nap=0;nap<naper;nap+=1)
    MAXAPER=MAX(MAXAPER,(int)(floor(0.5*APER[nap]))+1); // radius

  // Find the region this CPU is interested in
  XMINP=0;
  YMINP=0;
  XMAXP=Width;
  YMAXP=Height;
  if (nc>1 || nr>1) {
    XMINP=MAX(0,(xxx-1)*xrange-100);
    YMINP=MAX(0,(yyy-1)*yrange-100);
    XMAXP=MIN(xxx*xrange+100,Width);
    YMAXP=MIN(yyy*yrange+100,Height);
  }
  cout << "CPU xmin, ymin, xmax, ymax: " << XMINP <<" "<< YMINP<<" "<< XMAXP<<" "<< YMAXP << endl;

  datout << "#  1   ObjectID" << endl;
  datout << "#  2   X" << endl;
  datout << "#  3   Y" << endl;
  datout << "#  4   Cell "  << endl;
  datout << "#  5   cx          X pos in cell "  << endl;
  datout << "#  6   cy          Y pos in cell "  << endl;
  datout << "#  7   Rcell "  << endl;
  datout << "#  8   FitQty      Fit Quantity (Flux or Scale Factor)"  << endl;
  datout << "#  9   FitQuErr    Variance of Fit Quantity"  << endl;
  datout << "# 10   SExF        SExtractor flux_best in hires catalog"  << endl;
  datout << "# 11   Totflux     Total flux in hires cutout" << endl;
  datout << "# 12   Flag       Flag for saturation and/or prior blending" << endl;

  cellout << "#  1   CellID " << endl;
  cellout << "#  2   Nobj        Num objects in cell " << endl;
  cellout << "#  3   NX          XDim of cell " << endl;
  cellout << "#  4   NY          YDim of cell " << endl;
  cellout << "#  5   LX          X pos of lower left corner of cell " << endl;
  cellout << "#  6   LY          Y pos of lower left corner of cell " << endl;
  cellout << "#  7   lx0         X pos of original (ungrown) llc " << endl;
  cellout << "#  8   ly0         Y pos of original (ungrown) llc " << endl;
  cellout << "#  9   Background  Background value for this cell " << endl;
  cellout << "# 10   BGErr       Error in background value " << endl;
  cellout << "# 11   DoF         Degrees of freedom for this cell " << endl;
  cellout << "# 12   new_npix    Npix used in fit (after cellmask applied) " << endl;

  if (naper>0) {
    apout << "# ID ";
    for(nap=1;nap<=naper;nap++){
      apout << "F_APER" << nap << " ";
    }
    for(nap=1;nap<=naper;nap++){
      apout << "ERRF_APER" << nap << " ";
    }
    apout << endl;
  }

  cout << "******* Fitting with cells centered on objects *******" << endl;
  
  totnumsources=BigGrid->Count();
  TOTCELLS=totnumsources;
  GRIDCOUNT = (MEPROC-1) * (TOTCELLS/(nc*nr) + 10000);
#ifdef FLUXPRIOR  
  Data = matrix(1, TOTCELLS, 1, 7);
#else
  Data = matrix(1, TOTCELLS, 1, 4);
#endif
  DataInt = imatrix(1, TOTCELLS, 1, 5);

  Fitted = ivector(1,TOTCELLS);
  for (i=1;i<=TOTCELLS;i++)
    for (j=1;j<=1000;j++){
      Fitted[i]=0;
    }

#ifdef FLUXPRIOR
  flagprior=fvector(1, TOTCELLS);
  fprior=fvector(1, TOTCELLS);
  efprior=fvector(1, TOTCELLS);
#endif

  ifstream objin (FluxOrderedCatalog); 
  if (!objin) {
    cout << "Bad input file: " << FluxOrderedCatalog << " in WalkGrid" << endl;
    exit(1);}
  while (objin.peek()=='#' || objin.peek()=='i'){
    objin.getline(inputLine,LINE_SIZE);
  }
  objin >> id;
  i=0;
  while (oldid != id && objin) {
    i++;
    objin >> txi >> tyi >> xmini >> ymini >> xmaxi >> ymaxi >> bkgi >> fluxi; 
    DataInt[i][1]=id;
    DataInt[i][2]=xmini-1; // Because array starts from 0 
    DataInt[i][3]=ymini-1; 
    DataInt[i][4]=xmaxi-1; 
    DataInt[i][5]=ymaxi-1; 
    Data[i][1]=txi; 
    Data[i][2]=tyi; 
    Data[i][3]=bkgi;
    Data[i][4]=fluxi;
#ifdef FLUXPRIOR
    objin >> Data[i][5] >> Data[i][6] >> Data[i][7];
#endif
    objin.getline(inputLine,LINE_SIZE);
    oldid = id;
    objin >> id;
  }
  objin.close();

#ifdef VERBOSE
  cout<<"Catalog Read"<<endl;
#endif


  int jmax=0;
  // Now make two loops to find all overlaps and store them    
  for (i=1;i<=TOTCELLS;i++){
    
    xmini=DataInt[i][2];
    ymini=DataInt[i][3];
    xmaxi=DataInt[i][4];
    ymaxi=DataInt[i][5];
    j=0;
    for (k=1;k<=TOTCELLS;k++){
      if(DataInt[k][2]<=xmaxi){
	if(DataInt[k][3]<=ymaxi){
	  if(DataInt[k][4]>=xmini){
	    if(DataInt[k][5]>=ymini){
	      if(k!=i){
		j+=1;
		if (j>jmax){jmax=j;}
	      }
	    }
	  }
	}
      }
    }
  }

  overlap_kl = imatrix(1, TOTCELLS, 1, jmax+10);
  for (i=1;i<=totnumsources;i++){
    for (k=1;k<=jmax+10;k++){overlap_kl[i][k]=0;}
#ifdef VERBOSE
    cout << "Finding overlaps for obj " << i << endl;
#endif
    id=DataInt[i][1];
    xmini=DataInt[i][2];
    ymini=DataInt[i][3];
    xmaxi=DataInt[i][4];
    ymaxi=DataInt[i][5];
    j=0;
    for (k=1;k<=TOTCELLS;k++){
      if(DataInt[k][2]<=xmaxi){
	if(DataInt[k][3]<=ymaxi){
	  if(DataInt[k][4]>=xmini){
	    if(DataInt[k][5]>=ymini){
	      if(k!=i){
#ifdef DEBUG
		cout << "Object " << DataInt[k][1] << " is a contaminant of obj " << id<<endl;
#endif
		j+=1;
		overlap_kl[i][j]=k;
	      }
	    }
	  }
	}
      }
    }
  }
  
  cout<< "Done overlaps; max number of contaminants: "<< jmax <<endl;
  cout << "Starting fitting..."<<endl;

  // Now start main loop to build fitting cells.
  for (i=1;i<=TOTCELLS;i++){

#ifdef VERBOSE
    cout<<">>>>>>>>>>>>> Fitting object "<< i<<endl;
#endif

    crowdsize=0;
    // Do we need to fit this guy?
    if (Fitted[i]){
#ifdef VERBOSE
      cout << "Object " << i << " already fitted"<< endl;
#endif
    }
    
    else{
      
      Fitted[i]=1;
      
      id=DataInt[i][1];
      maxflux=Data[i][4];
      
      GRIDCOUNT++;
      
#ifdef VERBOSE
      cout << "Beginning buffer "<< GRIDCOUNT << "," << endl;
      cout << "Centered on object "<< id << endl;
#endif
      
      // Now, build the list of objects for this cell centered on object i      
      objincell = ivector(1,TOTCELLS);
      for (j=1;j<=TOTCELLS;j++){objincell[j]=0;}
      listincell= ivector(1,TOTCELLS);
      for (j=1;j<=TOTCELLS;j++){listincell[j]=0;}
      objincell[i]=1; // i is the position of item0 in the input flux-ordered catalog
      listincell[1]=i;
      int beginindex=1;
      int endindex=1;
      int thisobj;

      XMIN=DataInt[i][2];
      YMIN=DataInt[i][3];
      XMAX=DataInt[i][4];
      YMAX=DataInt[i][5];

#ifdef VERBOSE
      cout<<"Beginning loop on contaminants"<<endl;
#endif
      
      index=beginindex+1;
      do{
	for (k=beginindex;k<=endindex;k++){
	  thisobj=listincell[k];
#ifdef VERBOSE
	  cout<<"Obj number "<<thisobj<<" -> ID "<<DataInt[thisobj][1]<<endl;
#endif
	  kk=1;
	  while (overlap_kl[thisobj][kk]!=0){ // Fill listcell array with id's of overlapping sources
	    if (beginindex>1){ // These are higher order overlappers: check their flux
	      jj=overlap_kl[thisobj][kk];
	      //if (Data[jj][4]>maxflux){maxflux=Data[jj][4];}
	      if (objincell[jj]!=1){
		if (!Fitted[jj]){
		  //if (Data[jj][4]>fluxthresholdcheck*Data[thisobj][4]){ // To the obj
		  //if (Data[jj][4]>fluxthresholdcheck*Data[i][4]){ // To central object
		  if (Data[jj][4]>fluxthresholdcheck*maxflux){ // To brightest so far
		    /*diagjj2=(float)(pow((DataInt[jj][4]-DataInt[jj][2]),2)+pow((DataInt[jj][5]-DataInt[jj][3]),2));
		      distijj2=
		      pow(MIN(fabs(Data[jj][1]-(float)XMIN),fabs(Data[jj][1]-(float)XMAX)),2)+
		      pow(MIN(fabs(Data[jj][2]-(float)YMIN),fabs(Data[jj][2]-(float)YMAX)),2);
		      if (distijj2/diagjj2<.5){*/
		    /*if (
		      ( // jj overlaps thisobj
		      ((DataInt[jj][4]>DataInt[thisobj][4] &&
		      DataInt[jj][2]<DataInt[thisobj][4]-MIN(20,(int)(0.25*(DataInt[jj][4]-DataInt[jj][2]+1)))) ||
		      (DataInt[jj][2]<DataInt[thisobj][2] &&
		      DataInt[jj][4]>DataInt[thisobj][2]+MIN(20,(int)(0.25*(DataInt[jj][4]-DataInt[jj][2]+1))))) &&
		      ((DataInt[jj][5]>DataInt[thisobj][5] &&
		      DataInt[jj][3]<DataInt[thisobj][5]-MIN(20,(int)(0.25*(DataInt[jj][5]-DataInt[jj][3]+1)))) ||
		      (DataInt[jj][3]<DataInt[thisobj][3] &&
		      DataInt[jj][5]>DataInt[thisobj][3]+MIN(20,(int)(0.25*(DataInt[jj][5]-DataInt[jj][3]+1)))))
		      ) || 
		      ( // this obj includes jj
		      DataInt[jj][4]<=DataInt[thisobj][4] && 
		      DataInt[jj][2]>=DataInt[thisobj][2] &&
		      DataInt[jj][5]<=DataInt[thisobj][5] &&
		      DataInt[jj][3]>=DataInt[thisobj][3]
		      )
		      ){*/
		    if ( // Overlap is more than frac of jj area
			(float)(MAX(0,MAX(DataInt[thisobj][4],DataInt[jj][4])-MIN(DataInt[thisobj][2],DataInt[jj][2]))*
				MAX(0,MAX(DataInt[thisobj][5],DataInt[jj][5])-MIN(DataInt[thisobj][3],DataInt[jj][3])))>
			0.25*(float)((DataInt[jj][4]-DataInt[jj][2])*(DataInt[jj][5]-DataInt[jj][3]))
			){
#ifdef DEBUG
		      cout<<"Found contaminant "<<jj<<" ID: "<<DataInt[jj][1]<<endl;
#endif
		      objincell[jj]=1;
		      listincell[index]=jj;
		      if (DataInt[jj][2]<XMIN){XMIN=DataInt[jj][2];}
		      if (DataInt[jj][3]<YMIN){YMIN=DataInt[jj][3];}
		      if (DataInt[jj][4]>XMAX){XMAX=DataInt[jj][4];}
		      if (DataInt[jj][5]>YMAX){YMAX=DataInt[jj][5];}
		      index+=1; 
		      crowdsize+=1;
		    }
		  }
		}
	      }
	    }
	    else{ // First order overlappers: include them anyway
	      jj=overlap_kl[thisobj][kk];
	      if (objincell[jj]!=1){
		if (!Fitted[jj]){
#ifdef DEBUG
		  cout<<"Found contaminant 1st order "<<jj<<" ID: "<<DataInt[jj][1]<<endl;
#endif
		  objincell[jj]=1;
		  listincell[index]=jj;
		  //if (Data[jj][4]>maxflux){maxflux=Data[jj][4];}
		  if (DataInt[jj][2]<XMIN){XMIN=DataInt[jj][2];}
		  if (DataInt[jj][3]<YMIN){YMIN=DataInt[jj][3];}
		  if (DataInt[jj][4]>XMAX){XMAX=DataInt[jj][4];}
		  if (DataInt[jj][5]>YMAX){YMAX=DataInt[jj][5];}
		  index+=1;
		  crowdsize+=1;
		}
	      }
	    }
	    kk+=1;
	  }
	}
	beginindex=endindex+1;
	endindex=index-1;
#ifdef VERBOSE
	cout<<"beginindex, endindex "<<beginindex<<" " <<endindex<<endl;
#endif
      }while(endindex>beginindex);

      if (XMIN==XMAX)
	{
	  if (XMIN>0)
	    {XMIN-=1;cout<<"xmin"<<endl;}
	  else
	    {XMAX+=1;}
	}
      if (YMIN==YMAX)
	{
	  if (YMIN>0)
	    {YMIN-=1;cout<<"ymin"<<endl;}
	  else
	    {YMAX+=1;}
	}      
      cout<<"Cell boundaries (ID"<< DataInt[k][1] << " ): X "<<XMIN+1<<" "<<XMAX+1<<", Y  "<<YMIN+1<<" "<<YMAX+1<<endl;
      coo_fix=0;
      float fac_cell=0.5;
      //if (((XMAX-XMIN)>fac_cell*(XMAXP-XMINP)) && ((YMAX-YMIN)>fac_cell*(YMAXP-YMINP)))
      if (crowdsize>nsourcesopt)
	{
	  // Make a cell centered on the object, using the optimized size of cells
	  XMIN=MAX(XMINP,(int)(Data[i][1])-0.5*dxopt);
	  YMIN=MAX(YMINP,(int)(Data[i][2])-0.5*dyopt);
	  XMAX=MIN((int)(Data[i][1])+0.5*dxopt,XMAXP-1);
	  YMAX=MIN((int)(Data[i][2])+0.5*dyopt,YMAXP-1);
	  XMINo=XMIN; YMINo=YMIN; XMAXo=XMAX; YMAXo=YMAX;
	  cout << "crowdsize, nsourcesopt: " << crowdsize << " " << nsourcesopt << endl;
	  cout << "Cell is very large: fitting on X"<<XMIN<<"-"<<XMAX<<" Y"<<YMIN<<"-"<<YMAX<<endl;
	  coo_fix=1;
	  crowdsize=1;
	  for (jj=2;jj<=TOTCELLS;jj++) listincell[jj]=0;
	  for (jj=2;jj<=TOTCELLS;jj++) 
	    {
	      if ((Data[jj][1]>=XMIN)&&(Data[jj][2]>=YMIN)&&(Data[jj][1]<=XMAX)&&(Data[jj][2]<=YMAX))
		{
		  if (!Fitted[jj])
		    {
		      XMINo=MIN(XMINo,DataInt[jj][2]);
		      YMINo=MIN(YMINo,DataInt[jj][3]);
		      XMAXo=MAX(XMAXo,DataInt[jj][4]);
		      YMAXo=MAX(YMAXo,DataInt[jj][5]);
		      crowdsize+=1;
		      listincell[crowdsize]=jj;
		    }
		}
	    }
	  XMIN=XMINo; YMIN=YMINo; XMAX=XMAXo; YMAX=YMAXo;
	  endindex=crowdsize;

	  /*
	  coo_to_single=1;
	  // Just make a single fit on the whole image.
	  cout << "Cell is very large: enlarging to single cell fitting on the whole image."<<endl;
	  XMAX=XMAXP;
	  XMIN=XMINP;
	  YMAX=YMAXP;
	  YMIN=YMINP;
	  drfrac=10.;
	  int notfound;
	  for (j=1;j<=TOTCELLS;j++){
	    notfound=1;
	      for (jj=1;jj<=endindex;jj++){
		if (j==listincell[jj])
		  {notfound=0;
		    break;}
	      }
	      if (notfound){
		endindex+=1;
		listincell[endindex]=j;
#ifdef VERBOSE
		cout << "Adding object "<< j << " ID: "<< DataInt[j][1]<<endl;
#endif
	      }
	      Fitted[j]=1;
	  }
	  */
	}
      /*else
      	{
#ifdef VERBOSE      
	  cout<<"Checking which guys are happy with the fit in this cell"<<endl;
#endif
	  // Check overlappers: are they happy with the fit they are going to receive in this cell?
	  fitcount=1;
	  cellX=XMIN+0.5*(XMAX-XMIN); // cell X center
	  cellY=YMIN+0.5*(YMAX-YMIN); // cell Y center
	  
	  for (k=2;k<=endindex;k++){  
	    j=listincell[k];
	    id=DataInt[j][1];
	    txi=Data[j][1];tyi=Data[j][2];
	    
	    // Distance from the center of the cell
	    drX=fabs(txi-cellX);
	    drY=fabs(tyi-cellY);
	    
	    if ((drX<drfrac*(0.5*(XMAX-XMIN))) && (drY<drfrac*(0.5*(YMAX-YMIN)))){
	      
		// Brightness: j is happy if it is not too faint
		if (Data[j][4]>0.9*Data[i][4]) {
#ifdef VERBOSE
		  cout<<"Obj "<<k<<" ->ID "<<id<<" is happy"<<endl;
#endif
		  fitcount+=1;
		  Fitted[j]=1;
		 }
	    }
	  }
	      
	  cout<<"Keeping "<<fitcount << " fits of "<<endindex<<endl;
	  //}*/

      // ---- Matrix stuff begins here ------ //
      int clippedtot=0;
      clipped=0;
      cliter=0;
      cl = ivector(1, (long)endindex);
      for (k=1;k<=endindex;k++) cl[k]=0;
      crowdsize=endindex;
      cout << "Number of objects: " << crowdsize << endl;
      
      do {
	cliter++;
	crowdsize-=clipped;
	clippedtot+=clipped;
	contaminant = ivector(1, (long)crowdsize+fit_to_bg);
	
	/*
	  MATRIX CREATION PROCEDURE FROM HERE ON
	*/
#ifdef VERBOSE
	cout << "Cell: " << GRIDCOUNT << ", Sources: " << crowdsize << ", fit background: " << fit_to_bg <<endl;
	if (CLIP){
	  cout << "cliter, clipped: " << cliter << ", " << clipped << endl;
	}
#endif
	QCALLOC(Catalog, catalogstruct, crowdsize); 
	pCatalog = Catalog;  
	for (picker=1;picker<=crowdsize+clippedtot;picker++){
	  if (!cl[picker]){
	    k=listincell[picker];
	    pCatalog->id=DataInt[k][1];
	    pCatalog->x_image = Data[k][1];
	    pCatalog->y_image = Data[k][2];
	    pCatalog->xmin_image = DataInt[k][2];
	    pCatalog->ymin_image = DataInt[k][3];
	    pCatalog->xmax_image = DataInt[k][4];
	    pCatalog->ymax_image = DataInt[k][5];
	    pCatalog->detbckg = 0.0;
#ifdef FLUXPRIOR
	    pCatalog->fpf = Data[k][5];
	    pCatalog->pf = Data[k][6];
	    pCatalog->epf = Data[k][7];
#endif
	    pCatalog++;
	  }
	}

#ifdef VERBOSE
	cout << "Entering buildlinearsystem " << crowdsize+fit_to_bg << endl;
#endif

	A = dmatrix(1, (long)(crowdsize+fit_to_bg), 1, (long)(crowdsize+fit_to_bg));
	B = dvector(1, (long)(crowdsize+fit_to_bg));
	
	drX=XMAX-XMIN+1;
	drY=YMAX-YMIN+1;

	buildlinearsystem(A,B,pixels,pixels_rms,
			  LoResFits,WeightFits,TemplateDir,fit_to_bg,
			  Width, Height, ThresholdFactor,
			  drX,drY,XMIN,YMIN,
			  Catalog,crowdsize,use_cellmask,GRIDCOUNT);

	/*for (k=1;k<=crowdsize;k++) {
	  for (j=1;j<=crowdsize;j++) {
	    fprintf(stderr,"%f ", A[k][j]);}
	  fprintf(stderr,"= %f\n",B[k]);}*/	

	dof = new Par(crowdsize+fit_to_bg);
	for (j=1;j<=crowdsize;j++){
	  dof->data[j] = B[j];
	}
	QFREE(Catalog);

	//Acopy = dmatrix(1, (long)(crowdsize+fit_to_bg), 1, (long)(crowdsize+fit_to_bg));
	//for (k=1;k<=crowdsize;k++) {
	//  for (j=1;j<=crowdsize;j++) {
	//    Acopy[k][j]=A[k][j];}}

#ifdef VERBOSE
	cout<<"Linear system built. "<<endl;
#endif
	//-------------------------------------------------------------------------------//
	
	
	/*
	  LINEAR SYSTEM SOLUTION
	*/
	// Allocate covariance matrix
	covar = dmatrix(1,dof->size,1,dof->size);  // covariance matrix
	
	switch (linsyssolver)
	  {
	    
	    // ------------------------------------------------------------------------------ //
	    // THIS BLOCK FOR LU DECOMPOSITION
	    // ------------------------------------------------------------------------------ //
	    
	  case(1):

	    indx = ivector(1,dof->size);  // for LU decomp.
	    for (k=1;k<=dof->size;k++) indx[k] = 1;  // initialize indx
	    col = dvector(1,dof->size);  // for LU decomp; columns of identity matrix
	    ludcmp(A, dof->size, indx, &d);  // perform LU decomposition (see Numerical Recipe Section 2.3)
	    lubksb(A, dof->size, indx, dof->data); // perform LU back-substitution
	    //for(k=1;k<=crowdsize;k++){cout<<dof->data[k]<<" ";}
	    //  cout<<endl;
	    
#ifdef VERBOSE
	    cout << "Done LU decomposition. "<< endl;
#endif

	    /* dof->data now contains all the best-fit fluxes 
	       while A has been destroyed. 
	       To find the inverse of the original A follow NumRec:
	       the matrix is already decomposed so now just 
	       find inverse by columns by back sostitution.
	       calculate the inverse of A to get Fisher matrix, by back-substitution
	       for (j=1;j<=dof->size;j++)
	       cout << dof->data[j] << " " << endl; //exit(0);
	       use the columns of identity matrix for back-substitution*/
	    for (j=1; j<=dof->size; j++) {
	      for (k=1; k<=dof->size; k++) col[k] = 0.0;
	      col[j] = 1.0;
	      lubksb(A, dof->size, indx, col);
	      for (k=1; k<=dof->size; k++) covar[k][j] = col[k];
	    }

	    break;
	    
	    // ------------------------------------------------------------------------------ //
	    // THIS BLOCK FOR CHOLESKY DECOMPOSITION
	    // ------------------------------------------------------------------------------ //
	    
	  case(2):
	    p = dvector(1,dof->size);
	    for (k=1; k<=dof->size; k++) p[k] = 0.0;  // initialize p
	    
	    choldc(A, dof->size, p);  // Cholesky decomp.
	    cholsl(A, dof->size, p, B, dof->data); // Cholesky back-substitution
	    /* dof->data now contains all the best-fit fluxes (and background if fit_to_bg==1)
	       now calculate the inverse of A to get Fisher matrix, by back-substitution
	       cout << "done Cholesky decomposition." << endl;
	       calculate the inverse of L first, stored in the lower triangle of A*/
	    for (k=1;k<=dof->size;k++) {
	      A[k][k] = 1.0/p[k];
	      for (j=i+1;j<=dof->size;j++) {
		sum=0.0;
		for (jj=i;jj<j;jj++) sum -= A[j][jj]*A[jj][k];
		A[j][k]=sum/p[j];
	      }
	      for (j=1;j<i;j++) {
		A[j][k] = 0.0;	
	      }  	
	    }
	    // then use L**-1 to calculate A**-1 and store in covar
	    // A**-1 = (L**-1)^T * L**-1
	    for (k=1;k<=dof->size;k++) {
	      for (j=1;j<=dof->size;j++) {
		sum = 0.0;
		for (jj=1;jj<=dof->size;jj++) {
		  sum += A[jj][k] * A[jj][k];
		}
		covar[k][j] = sum;
	      }
	    }
	    break;	
	    
	  case (3):
	    // ------------------------------------------------------------------------------ //
	    // THIS BLOCK FOR IBG - ITERATIVE BICONJUGATE GRADIENT METHOD DECOMPOSITION
	    // ------------------------------------------------------------------------------ //
	    nmax = dof->size + 10;
	    for (k=1;k<=dof->size;k++) {
	      nmax=nmax+2*contaminant[k];
	      //cout << "..." << contaminant[i]<< " " ;
	    }
	    nmax = MIN (nmax, 16777216); //, 2 * dof->size * dof -> size); 	
	    //cout << ">>> " << nmax <<endl;
	    
	    /* Allocate memory for storing sparse matrix */
	    sa = dvector(1, nmax);
	    /* Allocate memory for storing row indexed array */
	    ija = ulvector(1, nmax);	    
	    sprsin(A, dof->size, thresh, (unsigned long)nmax, sa, ija); 
	    /// converts matrix a into vector sa indexed by vector ija (NumRec)
	    /// NB: still have to solve the system.
	    
	    n = (unsigned long)dof->size;
	    linbcg(sa, ija, n, B, dof->data, itol, tol, itmax, &iter, &erro); /// SOLVE IT.
	    /// itol = 4. tol = 1.e-128. x not initialized?!
	    //printf("\n");	  
	    free_dvector(sa, 1, nmax);
	    free_ulvector(ija, 1, nmax); 
	    //cout << "done IBG decomposition." << endl;
	    
	    // Find covariance matrix:
	    indx = ivector(1,dof->size);  
	    for (k=1;k<=dof->size;k++) indx[k] = 1;  // initialize indx
	    col = dvector(1,dof->size);  //  columns of identity matrix
	    
	    ludcmp(A, dof->size, indx, &d);
	    for (j=1; j<=dof->size; j++) {
	      for (k=1; k<=dof->size; k++) col[k] = 0.0;
	      col[j] = 1.0;
	      lubksb(A, dof->size, indx, col);
	      for (k=1; k<=dof->size; k++) covar[k][j] = col[k];}
	    break;
	    
	  }
	
	if (CLIP) {
	  clipped=0;
	  j=1;
	  int jj;
	  for (jj=1;jj<=crowdsize+clippedtot+fit_to_bg-1;jj++){
	    if (!cl[jj]){
	      j+=1;
	      if (dof->data[j]<0 && fabs(dof->data[j])>nsigma*sqrt(covar[j][j])){		  
#ifdef VERBOSE
		cout << "clipped out: " << j << " " << dof->data[j] << " "  << dof->size << endl;
#endif
		cl[jj]=1;
		clipped++;
	      }
	    }
	  }
	  
#ifdef VERBOSE
	  if (clipped) {
	    cout << "CLIPPED: " << clipped << " " << cliter << endl;}
	  else{
	    cout << "NO CLIPS " << cliter << endl;}
#endif
	}
	
	if (clipped) {
	  // Free memory for next iteration
	  free_dmatrix(A,1,dof->size,1,dof->size);      
	  free_dvector(B,1,dof->size);
	  free_dmatrix(covar,1,dof->size,1,dof->size);
	  free_ivector(contaminant,1,crowdsize+fit_to_bg);
	  
	  if (linsyssolver!=2) {
	    free_ivector(indx, 1, dof->size);
	    free_dvector(col, 1, dof->size);}
	  
	}
	
      } while (clipped);

#ifdef VERBOSE
      if (cliter>1) cout << "Clipping Iterations: " << cliter << endl;
#endif	  

      if (!clipped) {

	if (naper>0) {

	  /************** Aperture photometry ***********************/
       
	  // Find extremes of the region of interest
	
	  picker=1;
	  k=listincell[picker];
	  int XXXX,YYYY;
	  XXXX=floor(Data[k][1]-0.5e0); // central pixel
	  YYYY=floor(Data[k][2]-0.5e0); // central pixel

	  // Create small matrix with data
	  // NB - MAKE IT SMALLER IF AT BORDER!
	  double *pixels_aper, *pixels_aper_rms;
	  QMALLOC(pixels_aper, double, (2*MAXAPER+1)*(2*MAXAPER+1));
	  QMALLOC(pixels_aper_rms, double, (2*MAXAPER+1)*(2*MAXAPER+1));
	  xmini=XXXX-MAXAPER; ymini=YYYY-MAXAPER;
	  xmaxi=XXXX+MAXAPER; ymaxi=YYYY+MAXAPER;
	  for (jj=0;jj<2*MAXAPER+1;jj++){
	    for (kk=0;kk<2*MAXAPER+1;kk++){
	      if (xmini+kk<0 || ymini+jj<0 || xmini+kk>=Width || ymaxi+jj>=Height) // Borders
		{
		  pixels_aper[(MAXAPER*2+1)*jj+kk]=0.0;
		  pixels_aper_rms[(MAXAPER*2+1)*jj+kk]=0.0; //1.e+5;
		}
	      else
		{
		  pixels_aper[(MAXAPER*2+1)*jj+kk]=pixels[Width*(ymini+jj)+(xmini+kk)];
		  pixels_aper_rms[(MAXAPER*2+1)*jj+kk]=pixels_rms[Width*(ymini+jj)+(xmini+kk)];
		}
	    }
	  }

	  // remove contaminations
	  for (picker=2;picker<=crowdsize+clipped;picker++){
	    k=listincell[picker];

	    sprintf(modname, "%s/%s-%d.fits", TemplateDir, "mod", DataInt[k][1]);
	    
	    if(!fits_open_file(&fptr0, modname, READONLY, status)) {
	      
	      fits_get_img_param(fptr0, 2, &bitpix, &naxis, naxes, status);
	      QMALLOC(pixels_th0, double, naxes[0]*naxes[1]);
	      
	      fits_read_pix(fptr0, TDOUBLE, fpixel, naxes[0]*naxes[1], NULL,
			    pixels_th0, NULL, status);
	      fits_close_file(fptr0, status);
	      fptr0=NULL;

	      // Check for overlapping
	      int dx1, dx2, dy1, dy2;
	      int x_in1, x_in2, y_in1, y_in2;
	      int xomin, xomax, yomin, yomax;
	      int owidth,oheight;
	      xomin = MAX(xmini, DataInt[k][2]);
	      xomax = MIN(xmaxi, DataInt[k][4]);
	      yomin = MAX(ymini, DataInt[k][3]);
	      yomax = MIN(ymaxi, DataInt[k][5]);
	      owidth = xomax-xomin+1;
	      oheight = yomax-yomin+1;
	      dx1 = DataInt[k][4] - xmini;
	      dx2 = xmaxi - DataInt[k][2];
	      dy1 = DataInt[k][5] - ymini;
	      dy2 = ymaxi - DataInt[k][3];
	      if ((I_SIGN(dx1) == I_SIGN(dx2)) && (I_SIGN(dy1) == I_SIGN(dy2))) {
		// Set overlap box in thumbnails ref
		x_in1=MAX(0,DataInt[k][2]-xmini); 
		x_in1=MAX(x_in1,(MAXAPER*2+1)-xmaxi-1);
		y_in1=MAX(0,DataInt[k][3]-ymini);
		y_in1=MAX(y_in1,(MAXAPER*2+1)-ymaxi-1);
		x_in2=MAX(0,xmini-DataInt[k][2]);
		x_in2=MAX(x_in2,naxes[0]-DataInt[k][4]-1);
		y_in2=MAX(0,ymini-DataInt[k][3]);
		y_in2=MAX(y_in2,naxes[1]-DataInt[k][5]-1);
#ifdef VERBOSE
		cout << "Removing contaminant" << DataInt[k][1] << endl;
#endif
		for (jj=yomin;jj<oheight;jj++) {
		  for (kk=xomax;kk<owidth;kk++) {
		    pixels_aper[(y_in1+jj)*(MAXAPER*2+1)+(x_in1+kk)]-=pixels_th0[(y_in2+jj)*naxes[0]+(x_in2+kk)]*dof->data[picker];
		  }
		}
	      }
	      QFREE(pixels_th0);
	      
	    }
	  }
#ifdef VERBOSE
	  cout << "Removed all contaminants" << endl;
#endif
	  // Do aperture photometry
	  faper=dvector(1,naper);errfaper=dvector(1,naper);
	  for(jj=1;jj<=naper;jj++){
	    faper[jj]=0.0;errfaper[jj]=0.0;
	  }

	  picker=1;
	  k=listincell[picker];
	  apertures(naper, faper, errfaper, MAXAPER,
		    10, APER, pixels_aper, pixels_aper_rms,
		    2*MAXAPER+1, 2*MAXAPER+1, 
		    Data[k][1]-1.0, Data[k][2]-1.0);

	  QFREE(pixels_aper);
	  QFREE(pixels_aper_rms);

	}

	
	/************** Subtract fitted objects ******************/
	
	picker=1;
	//for (picker=1;picker<=crowdsize+clipped;picker++){
	  k=listincell[picker];
	  if (Fitted[k]) {
	    cout << "Subtracting object "<< k << " ID: " << DataInt[k][1] <<" "<<dof->data[picker] <<endl;
	    sprintf(modname, "%s/%s-%d.fits", TemplateDir, "mod", DataInt[k][1]);
	    
	    if(!fits_open_file(&fptr0, modname, READONLY, status)) {
	      fits_get_img_param(fptr0, 2, &bitpix, &naxis, naxes, status);
	      QMALLOC(pixels_th0, double, naxes[0]*naxes[1]);
	      
	      fits_read_pix(fptr0, TDOUBLE, fpixel, naxes[0]*naxes[1], NULL,
			    pixels_th0, NULL, status);
	      fits_close_file(fptr0, status); 
	      fptr0=NULL;
	      xmini=DataInt[k][2];
	      ymini=DataInt[k][3];
	      
	      for (jj=0;jj<naxes[1];jj++) {
		for (kk=0;kk<naxes[0];kk++) {
		  pixels[Width*(jj+ymini)+(kk+xmini)]-=pixels_th0[naxes[0]*jj+kk]*dof->data[picker];
		}
	      }
	      
	      QFREE(pixels_th0);
	    }
	  }

	/************** Output stuff starts here***********************/

	  printf("Cpu %d: Completed fitting on cell %ld/%ld \n", MEPROC, GRIDCOUNT, TOTCELLS);

	/* Write results to output */
	datout << setprecision(3); datout.setf( ios::fixed );

	if (exportCovariance) {
	  covout << endl << "#Cell id: " << GRIDCOUNT << endl;
	  //covout << "#Arraydim: " << crowdsize+1 << " " << dof->size << endl;
	  if (coo_to_single){
covout << "#Arraydim: " << dof->size+fit_to_bg+1 << " " << dof->size+fit_to_bg << endl;}
	  else{
	    covout << "#Arraydim: " << dof->size+fit_to_bg+1 << " 1" << endl;}
	  //covout << "#Arraydim: 2 1" << endl;

	  covout << "#Fitted flux follows .. " << endl;
	  for (j=1;j<=dof->size;j++){
	    k=listincell[j];
	    //if (Fitted[k]){
	      covout << dof->data[j] << " ";
	      //}
	  }
	  covout << endl;
	  covout << " " << endl;

	  //Starts with the background row.
	  covout << "#Covar matrix follows... " << endl;
	  if (fit_to_bg) {
	    covout << "0 ";
	    for (j=1;j<=dof->size;j++)
	      covout << covar[1][j] << " ";
	    covout << endl;
	  }	  
	}
	
	tcount = fit_to_bg;  //There's an offset in dof->data & covar, because of sky.
	
	for (picker=1; picker<=dof->size; picker++) {
	  /*if (cl[picker+1]){
	    k=listincell[picker];
	    if (Fitted[k]){datout << setw(6) << DataInt[k][1]<<endl;}
	  }
	  else{*/
	    k=listincell[picker];
	    if (Fitted[k]){
	      tcount++;
	      datout << setw(6) << DataInt[k][1]<<" "; 
	      datout.setf(ios::fixed,ios::floatfield | ios::showpoint);
	      xx=Data[k][1];yy=Data[k][2];
	      datout << setprecision(2); 
	      datout << setw(8) <<xx << " " << setw(8) <<yy << " ";
	      
	      //Floating cell number & distance from cell center.	      
	      datout << setw(4) << GRIDCOUNT << " " ;
	      
	      xc = Data[i][1]-XMIN;
	      yc = Data[i][2]-YMIN;
	      datout << setprecision(2);
	      datout << setw(8) << xc << " " << setw(8) << yc << " ";
	      
	      rcell = sqrt(pow((Data[i][1]-xx),2.)+pow((Data[i][2]-yy),2.));
	      datout << setprecision(4); 
	      datout << setw(8) << rcell << " ";
	      datout << scientific;
	      datout << setprecision(6); 
	      if (use_cellmask){
		oberr=sqrt(covar[tcount][tcount]); //* sqrt(chi2/(new_npix - dof->size));
	      } else {
		oberr=sqrt(covar[tcount][tcount]); // * sqrt(chi2/(newcell.Npix() - dof->size));
	      }
	      datout << setw(12) << dof->data[picker] << " "; // flux / scale factor	      
	      datout << setw(12) << oberr << " "; //error
	      //datout.setf(ios::floatfield | ios::showpoint);
	      //datout << setw(10) << facZP*Data[k][4]<<" " << facZP*fabs(Data[k][3]); 

	      datout << setw(10) << Data[k][4]<<" " << Data[k][3]; 
	      //datout << setw(10) << Data[k][4]<<" " << faper; 
	   	      
	      if (naper>0) {
		// Apertures
		apout << DataInt[k][1]; 
		for(nap=0;nap<naper;nap++)
		  apout << " " << setw(8) << faper[nap];
		for(nap=0;nap<naper;nap++)
		  apout << " " << setw(8) << sqrt(errfaper[nap]);
		apout << endl;
	      }
	      
	      int flaggy = 0;
	      if (Data[k][3]<0) {flaggy += 1;} // Saturation
	      if (blendedpriors[0]>=0) // Blended priors
		{
		  int ii,jj;
		  id=DataInt[k][1];
		  for (ii=0; ii<nblended; ii++){
		    if (blendedpriors[ii]==id){
		      flaggy+=2;break;}
		  }
		}

	      if ((DataInt[i][2]<1) || (DataInt[i][3]<1) || (DataInt[i][4]>Width-1) || (DataInt[i][5]>Height-1))
		{flaggy += 4;} // Borders

	      datout << setw(4) << flaggy << " ";
	      
	      //Bcopy[k]=dof->data[picker]; // This is for errors...

	      if (exportCovariance) { 
		covout << DataInt[k][1]<< " " ;
		for (j=1;j<=dof->size;j++){
		  covout << covar[tcount][j] << " ";}
		covout << endl;
	      }

	      //if (exportCovariance) { 
	      //	for (k=1;k<=dof->size;k++){
	      //	  covout << DataInt[k][1]<< " " ;
	      //	  for (j=1;j<=dof->size;j++){
	      //	    covout << covar[k][j] << " ";}
	      //	  covout << endl;
	      //	}
	      //}
	    
	      datout << endl;
	    }
	    
	} // end looping over crowd

	if (fit_to_bg) {
	  picker=dof->size+1;
	  cout << "Background for cell" << GRIDCOUNT << ": " << dof->data[picker] << " (per pixel:" << dof->data[picker]/(float)(drX*drY)<< ")" << endl;
	}

	// Write cell stats for this cell
	cellout << setw(4) << GRIDCOUNT << setw(4) << crowdsize << " " ;
	cellout << setw(6) << drX << " " <<  setw(6) << drY << " ";
	cellout << setw(6) << XMIN << " " << setw(6) << YMIN << " ";
	cellout << setw(6) << DataInt[i][2] << " " << setw(6) << DataInt[i][3] << " ";
	cellout << setprecision(4); 
	cellout.setf(ios::floatfield | ios::showpoint);
	if (fit_to_bg == 1) {
	  cellout << setw(8) << dof->data[1] << " "; // background
	  cellout << setw(8) << sqrt(covar[1][1]) << " "; //error
	} else {
	  cellout << "0.0" << " ";
	}
	
	cellout << setw(10) << (drX*drY - dof->size) << " ";
	cellout << setw(7) << drX*drY;
	cellout << endl;
      }
#ifdef VERBOSE
      cout << "Freeing memory" << endl;
#endif
      // Free memory //
      free_dmatrix(A,1,dof->size,1,dof->size);
      free_dvector(B,1,dof->size);
      free_dmatrix(covar,1,dof->size,1,dof->size);
      free_ivector(contaminant,1,crowdsize+fit_to_bg);
      
      if (linsyssolver!=2) {
	free_ivector(indx, 1, dof->size);
	free_dvector(col, 1, dof->size);}
      
      free_ivector(objincell,1,TOTCELLS);
      free_ivector(listincell,1,TOTCELLS);	 
      free_ivector(cl,1,endindex);
      if (naper>0) {
	free_dvector(faper,1,naper);
	free_dvector(errfaper,1,naper);
      }
    }

  }

  /*
  // Now do another loop for the errors...
  newerr.open("newerr.temp");
  float errfac;
  for (i=1;i<=TOTCELLS;i++){
    j=1; errfac=0.0;
    while(overlap_kl[i][j]>0){
      //if (blendedpriors[(DataInt[i][1]-1)*totnumsources + (DataInt[overlap_kl[i][j]][1]-1)]){
#ifdef VERBOSE
      cout << "Blended priors: " <<  DataInt[i][1] << " " << DataInt[overlap_kl[i][j]][1] << " " << Acopy[i][overlap_kl[i][j]] << " " << Acopy[i][i] << endl;
#endif
      errfac+=pow(fabs(Acopy[i][overlap_kl[i][j]]/Acopy[i][i])*Bcopy[overlap_kl[i][j]],2);
	//}
      j++;
    }
    
    newerr << setw(12) << sqrt(errfac) << endl;
  }
  newerr.close();
  */

  // Begin final cleanup here
  //free_dmatrix(blends,1,TOTCELLS,1,TOTCELLS);    
  //free_dmatrix(Acopy,1,totnumsources+fit_to_bg,1,totnumsources+fit_to_bg);
  //free_dvector(Bcopy,1,totnumsources+fit_to_bg);
  free_matrix(Data, 1, TOTCELLS, 1, 4);
  free_imatrix(DataInt, 1, TOTCELLS, 1, 5);
  free_imatrix(overlap_kl,1, TOTCELLS, 1, jmax+10);
  if (exportCovariance) covout.close();
  datout.close();
  if (naper>0) apout.close();
  cellout.close();
    
  /*
  // write residual LRI matrix on the fly
  for (jj=0;jj<Height;jj++) {
    for (kk=0;kk<Width;kk++) {
      cout<<pixels[Width*jj+kk]<<" ";
    }
    cout<<endl;
  }
  */
  
  QFREE(pixels); QFREE(pixels_rms);
  
}


/* ------------------------------------------------------------------------------
   FUNCTION FOR STANDARD TFIT-CELL FITTING OR SINGLE CELL FITTING
------------------------------------------------------------------------------ */

void Funcs::walkGrid(char *SexFile, char *TemplateDir, 
		     char *LoResFits, char *WeightFits, 
		     double *pixels, double *pixels_rms, 
		     long *pixels_flag, long *pixels_seg,
		     int Width, int Height, int USESIG, 
		     char outfile[], char cellfile[], Grid *BigGrid,
		     float RMS_CONSTANT, float BG_CONSTANT, double facZP,
		     int use_cellmask, float mask_floor, int dithercell,
		     int max_flag, int fit_to_bg,
		     int exportCovariance, char CovarFile[], 
		     int XBUFFERSIZE, int YBUFFERSIZE, int FLAGDIST, 
		     int nc, int nr, int xxx, int yyy, int xrange, int yrange, 
		     int MEPROC, float ThresholdFactor, int linsyssolver, 
		     int CLIP, char *overlaps, 
		     int *blendedpriors, int nblended, int *status) {

  /*********************************************************************
   *
   *  The Fits images are designated as:
   *    I: The full LoRes image (x,y) values over the whole image
   *    E: The full weight map on the lores image
   *    F: The full flag map on the lores image
   *
   *********************************************************************/

  register int a,b,i,j,k,l,m,n,mm,nn;
  static long GRIDCOUNT = 0;
  static long TOTCELLS = 0;
  static int XX=1, YY=2, NRHS=1;
  long 
    crowdsize, growcount, *imflag,
    XMIN, XMAX,
    YMIN, YMAX,
    NXGRIDS,NYGRIDS;
  int 
    XMINo,YMINo,XMAXo,YMAXo,
    XMINP, XMAXP, YMINP, YMAXP,
    flag=0, kk, ll, jj,
    flaggedCount=0,
    groupedCount=0,
    myi, myj, dither, xditherwidth, yditherwidth,
    x0, y0, oxd, oyd, oxu, oyu,
    xlo, xhi, ylo, yhi, cx, cy,
    bxlo=0, bxhi=0, bylo=0, byhi=0,
    xcorner, ycorner,
    *lowcell, *highcell,
    *lowcellsmall, *highcellsmall,
    npix, cellindex, pcounter,
    totnumsources,
    *dofflag, *mask, new_npix=0, **overlap_kl;
  char pme[STRING_LENGTH];
  float 
    xx, yy, obx, oby, 
    xc, yc,
    xpos=0.0, ypos=0.0, 
    background=0.0, 
    fwhm=0.0,
    flux=0.0,
    isoflux,
    XCEN, YCEN, rcell;
  double 
    **covar, **temp, **newtemp,
    totflux=0.0, oberr, 
    **A,**Acopy,
    *image,*newimage, 
    *err, *newerr, sumkl=0.0, sumb=0.0, *tempkl, *tempb, sum=0.0;
  ofstream datout, covout, cellout;
  Fits *thisone, *thatone;
  Par *dof; 
  List *theList;
  gNode *step, *pnt, *item0, *item, *itemj;
  // for Cholesky decomposition
  double *B, *p;
  // for LU decomposition
  int *indx;
  double *col, d=1.0;
  int id, NX, NY, lcx, lcy, hcx, hcy;
  float xmin,xmax,ymin,ymax;
  int stat;
  
  Box cell, newcell;
  vector <gNode *>  crowd;
  long picker, pickerj, tcount;

  catalogstruct *pCatalog;
 
  long fpixel[2] = {1,1};

#ifdef FLUXPRIOR
  float flagpriorflux, priorflux, errpriorflux;
#endif

  /*********************************************************************
   *
   * funcs returns the m basis functions in a[] evaluated at x 
   * degree of freedom array set with one degree of freedom
   */
  
  void funcs(double *, double [], int, int );
  //*********************************************************************

  // Open the files
  char outfile1[STRING_LENGTH];
  char cellfile1[STRING_LENGTH];
  char CovarFile1[STRING_LENGTH];
  
  sprintf(pme,"_%d",MEPROC);
  snprintf(outfile1,STRING_LENGTH,"%s%s",outfile,pme);
  datout.open(outfile1);
  snprintf(cellfile1,STRING_LENGTH,"%s%s",cellfile,pme);
  cellout.open(cellfile1);
  if (exportCovariance) 
    {
      snprintf(CovarFile1,STRING_LENGTH,"%s%s",CovarFile,pme);
      covout.open(CovarFile1);
    }


  
  // Compute total number of sources
  totnumsources=BigGrid->Count(); 

  // Define the set of arbitrary grids that we will use
  NXGRIDS = 1; NYGRIDS = 1;
  if (Width > XBUFFERSIZE && Height > YBUFFERSIZE) {
    NXGRIDS = (Width - 2*(FLAGDIST)) / (XBUFFERSIZE - 2*(FLAGDIST)) + 1;
    NYGRIDS = (Height - 2*(FLAGDIST)) / (YBUFFERSIZE - 2*(FLAGDIST)) + 1;
  }

  //blends = dmatrix(1, (long)totnumsources, 1, (long)totnumsources);
  //for (i=1;i<=totnumsources;i++)
  //  for (j=1;j<=totnumsources;j++){
  //    blends[i][j]=0.0;}
  
#ifdef VERBOSE
  cout    << "NXGRIDS, NYGRIDS = " << NXGRIDS << "," << NYGRIDS << endl;
  cellout << "#NXGRIDS, NYGRIDS = " << NXGRIDS << "," << NYGRIDS << endl;
#endif
  
  // Find the region this CPU is interested in
  XMINP=0;
  YMINP=0;
  XMAXP=Width;
  YMAXP=Height;
  if (nc>1 || nr>1) {
    XMINP=MAX(0,(xxx-1)*xrange-100);
    YMINP=MAX(0,(yyy-1)*yrange-100);
    XMAXP=MIN(xxx*xrange+100,Width);
    YMAXP=MIN(yyy*yrange+100,Height);
  }
  
  cout << "CPU xmin, ymin, xmax, ymax: " << XMINP <<" "<< YMINP<<" "<< XMAXP<<" "<< YMAXP << endl;

  datout << "#  1   ObjectID" << endl;
  datout << "#  2   X" << endl;
  datout << "#  3   Y" << endl;
  datout << "#  4   Cell "  << endl;
  datout << "#  5   cx          X pos in cell "  << endl;
  datout << "#  6   cy          Y pos in cell "  << endl;
  datout << "#  7   Rcell "  << endl;
  datout << "#  8   FitQty      Fit Quantity (Flux or Scale Factor)"  << endl;
  datout << "#  9   FitQuErr    Variance of Fit Quantity"  << endl;
  //if (fabs(facZP-1.0)>TOL){
  //  datout << "# 10   SExF        Scaled SExtractor flux"  << endl;}
  //else{
    datout << "# 10   SExF        SExtractor flux_best in hires catalog"  << endl;
    //}
  datout << "# 11   Totflux     Total flux in hires cutout" << endl;
  //datout << "# 12   NewErr" << endl;
  datout << "# 12   Flag       Flag for saturation and/or prior blending" << endl;
  
  cellout << "#  1   CellID " << endl;
  cellout << "#  2   Nobj        Num objects in cell " << endl;
  cellout << "#  3   NX          XDim of cell " << endl;
  cellout << "#  4   NY          YDim of cell " << endl;
  cellout << "#  5   LX          X pos of lower left corner of cell " << endl;
  cellout << "#  6   LY          Y pos of lower left corner of cell " << endl;
  cellout << "#  7   lx0         X pos of original (ungrown) llc " << endl;
  cellout << "#  8   ly0         Y pos of original (ungrown) llc " << endl;
  cellout << "#  9   Background  Background value for this cell " << endl;
  cellout << "# 10   BGErr       Error in background value " << endl;
  cellout << "# 11   DoF         Degrees of freedom for this cell " << endl;
  cellout << "# 12   new_npix    Npix used in fit (after cellmask applied) " << endl;

  // Walk over each grid spacing

  xditherwidth = (XBUFFERSIZE - 2*FLAGDIST)/2;
  yditherwidth = (YBUFFERSIZE - 2*FLAGDIST)/2;
#ifdef VERBOSE
  cout << "x/y ditherwidth = " << xditherwidth << "," << yditherwidth << endl;
#endif

	TOTCELLS=(dithercell+1)*NXGRIDS*NYGRIDS;
	GRIDCOUNT = (MEPROC-1) * (TOTCELLS/(nc*nr) + 10000);

	for (dither=0;dither<=dithercell;dither++)   
    for (myi=1;myi<=NXGRIDS;myi++) 
      for (myj=1;myj<=NYGRIDS;myj++) {

	if (xrange==XBUFFERSIZE && yrange==YBUFFERSIZE){ // Single cell fitting
	  XMIN=XMINP;XMAX=XMAXP;
	  YMIN=YMINP;YMAX=YMAXP;
	}
	else{
	  // Compute the bounds of this LRI cell...
	  XMIN = XBUFFERSIZE*(myi-1) - 2*(myi-1)*FLAGDIST + dither*xditherwidth;
	  XMAX = XBUFFERSIZE*(myi)   - 2*(myi-1)*FLAGDIST + dither*xditherwidth;
	  YMIN = YBUFFERSIZE*(myj-1) - 2*(myj-1)*FLAGDIST + dither*yditherwidth;
	  YMAX = YBUFFERSIZE*(myj)   - 2*(myj-1)*FLAGDIST + dither*yditherwidth;
	  //.. ensuring that we don't go outside the image.
	  XMAX = (XMAX > Width) ? Width : XMAX;
	  YMAX = (YMAX > Height) ? Height : YMAX;
	}

	if (XMIN>XMAXP || XMAX<XMINP || YMIN>YMAXP || YMAX<YMINP)
	  {
#ifdef VERBOSE
	    cout << "Cell outside CPU region" << endl;
#endif
	  }
	else
	  {
#ifdef VERBOSE
	    cout << ":::" << XMIN << " " << XMAX << " " << YMIN << " " << YMAX << endl;
#endif
	    GRIDCOUNT++;
	    
#ifdef VERBOSE
	    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	    cout << "Beginning buffer "<< GRIDCOUNT << ": " << endl;
	    cout << "Dither, dithercell = " << dither <<","<<dithercell<<endl;
#endif
	    XCEN = (XMAX - XMIN)/2.0;
	    YCEN = (YMAX - YMIN)/2.0;
		   
	    cell.Set(XMIN, YMIN, XMAX, YMAX);
	    newcell = cell; /// call this "newcell"...
		   
#ifdef VERBOSE
	    cout << "X-Y Ranges: " << XMIN << "," << YMIN << "," << XMAX << "," << YMAX << endl;
#endif
		   
	    // Find the range of cells in the BigGrid that we care about.
	    /// That is, only list objects from cells of BigGrid overlapping this LRI cell
	    lowcell  = BigGrid->Contains(XMIN,YMIN);
	    highcell = BigGrid->Contains(XMAX,YMAX);
		   
	    //Compute the number of objects there will be.
	    //crowdsize = 0; replaced by crowd.size()
	    crowd.clear();
	    growcount=0;
	    /// Loop on BigGrid cells and list objects
	    for(m=lowcell[XX];m<=highcell[XX];m++)      //xcoords
	      for(n=lowcell[YY];n<=highcell[YY];n++) {  //ycoords
		item = BigGrid->grid[n][m]->Top();
		   
		// Loop through the list for this BigGrid cell
		while (item != NULL) {
		  // Make sure the object center is in the cell
		  item->getCoords(&obx,&oby);
			 
		  if (cell.Contains(obx,oby)){
#ifdef VERBOSE
		    cout << "Cell contains " << item->Num() << " "<<obx <<" "<< oby << endl;
#endif
			   
		      crowd.push_back(item);
		      item->getLimits(&oxd,&oyd,&oxu,&oyu);
		      // Check its template bounds
		      if (!cell.Contains(oxd,oyd) || !cell.Contains(oxu,oyu))
			{         
#ifdef VERBOSE
			  cout << "...Growing to hold object #" <<item->Num()<<endl;
#endif
			  newcell.GrowsToHold(oxd,oyd,oxu,oyu); 
			  growcount++;
			}
			   
		    //end if object is in cell
		  }
			 
		  else { // BigGrid cell contains many guys, only few belong to present HRI cell
#ifdef VERBOSE
		    cout << "object "<< item->Num() <<" discarded from square " << m<< "," << n << obx << ","<< oby << endl;
#endif
		  } // end else object is not in cell 
			 
		  item = BigGrid->grid[n][m]->Next(item);
		}
	      }

	    if (crowd.empty() ) {
#ifdef VERBOSE
	      cout << "Skipping buffer "<< GRIDCOUNT <<": Zero objects " << endl;
#endif
	    }
		   
	    else 
		     
	      {
		       
#ifdef VERBOSE
		cout <<  "... "<<crowd.size()<<" objects."<<endl; 
		cout << growcount <<" objects or "<<100.0*float(growcount)/crowd.size() << "% extended past original cell boundaries." << endl;
#endif
		       
		// ---- Matrix stuff begins here ------ //
		int clippedtot=0;
		clipped=0;
		cliter=0;
		cl = ivector(1, (long)crowd.size());
		for (i=1;i<=crowd.size();i++) cl[i]=0;
		crowdsize=crowd.size();
		       
		do { // clipping loop
		  cliter++;
		  crowdsize-=clipped;
		  clippedtot+=clipped;
		  contaminant = ivector(1, (long)crowdsize+fit_to_bg);
			 
			   
		    /*
		      MATRIX CREATION PROCEDURE FROM HERE ON
		      Use the list of objects in crowd
		    */
#ifdef VERBOSE
		    cout << "Cell: " << GRIDCOUNT << ", Sources: " << crowdsize << ", fit background: " << fit_to_bg <<endl; 
		    cout << "cliter, crowdsize, clipped: " << cliter << ", " << crowdsize << ", " << clipped << endl;
#endif
		    QCALLOC(Catalog, catalogstruct, crowdsize); 
		    pCatalog = Catalog;  
		    for (picker=0;picker<crowdsize+clippedtot;picker++){
		      if (!cl[picker+1]){
			item=crowd[picker];
			
			item->getCoords(&obx,&oby);
			pCatalog->id=item->Num();
			pCatalog->x_image = obx; 
			pCatalog->y_image = oby; 
			item->getLimits(&oxd,&oyd,&oxu,&oyu);
			pCatalog->xmin_image = oxd; 
			pCatalog->xmax_image = oxu; 
			pCatalog->ymin_image = oyd; 
			pCatalog->ymax_image = oyu; 
			pCatalog->detbckg = 0.0;
#ifdef FLUXPRIOR
			item->getfluxprior(&flagpriorflux,&priorflux,&errpriorflux);
			pCatalog->fpf = flagpriorflux;
			pCatalog->pf = priorflux;
			pCatalog->epf = errpriorflux;
#endif
			pCatalog++;
		      }
		    }

#ifdef VERBOSE
		    cout << "Entering buildlinearsystem " << crowdsize+fit_to_bg << endl;
#endif 		    

		    A = dmatrix(1, (long)crowdsize+fit_to_bg, 1, (long)crowdsize+fit_to_bg);
		    B = dvector(1, (long)crowdsize+fit_to_bg);

		    buildlinearsystem(A,B,pixels,pixels_rms,
				      LoResFits,WeightFits,TemplateDir,fit_to_bg,
				      Width, Height, ThresholdFactor,
				      newcell.Xdim(),newcell.Ydim(),
				      newcell.Lx(),newcell.Ly(),
				      Catalog,crowdsize,use_cellmask,GRIDCOUNT);
		    cout<<"crowdsize "<<crowdsize<<endl;
		    dof = new Par(crowdsize+fit_to_bg); 
		    for (j=1;j<=crowdsize+fit_to_bg;j++){
		      dof->data[j] = B[j];
		    }

		    QFREE(Catalog);

		    /*Acopy = dmatrix(1, (long)(crowdsize+fit_to_bg), 1, (long)(crowdsize+fit_to_bg));
		      for (k=1;k<=crowdsize;k++) {
		      for (j=1;j<=crowdsize;j++) {
		      Acopy[k][j]=A[k][j];}}*/

		    /*for (i=1;i<=crowdsize+fit_to_bg;i++) {
		      for (j=1;j<=crowdsize+fit_to_bg;j++) {
			printf("%f ", A[i][j]);}
			printf("= %f\n",B[i]);}*/
			 
#ifdef VERBOSE
		  cout<<"Linear system built. "<<endl;
#endif
			 
		  //-------------------------------------------------------------------------------//
			 
			 
		  /*
		    LINEAR SYSTEM SOLUTION
		  */
		  // Allocate covariance matrix
		  covar = dmatrix(1,dof->size,1,dof->size);  // covariance matrix
		  switch (linsyssolver)
		    {
			     
			     
		      // Three choices for matrix decomposition of A
			     
		      // ------------------------------------------------------------------------------ //
		      // THIS BLOCK FOR LU DECOMPOSITION, TO CALCULATE COVARIANCE MATRIX                //
		      // ------------------------------------------------------------------------------ //
			     
		    case(1):
			     
		      // === Use Numerical Recipe === //
		      /// funcs_math.cc
		      indx = ivector(1,dof->size);  // for LU decomp.
		      for (i=1;i<=dof->size;i++) indx[i] = 1;  // initialize indx
		      col = dvector(1,dof->size);  // for LU decomp; columns of identity matrix
		      ludcmp(A, dof->size, indx, &d);  // perform LU decomposition (see Numerical Recipe Section 2.3)
		      lubksb(A, dof->size, indx, dof->data); // perform LU back-substitution
#ifdef VERBOSE
		      cout << "Done LU decomposition. "<< endl;
#endif
		      /*for (j=1;j<=crowdsize;j++){
			printf("%f\n",dof->data[j]);
		      }*/

		      // dof->data now contains all the best-fit fluxes (and background if fit_to_bg==1)
		      // while A has been destroyed. 
		      // To find the inverse of the original A follow NumRec:
		      // the matrix is already decomposed so now just find inverse by columns by back sostitution.
		      // calculate the inverse of A to get Fisher matrix, by back-substitution
		      //for (j=1;j<=dof->size;j++)
		      //  cout << dof->data[j] << " " << endl; //exit(0);
		      // use the columns of identity matrix for back-substitution
		      for (j=1; j<=dof->size; j++) {
			for (i=1; i<=dof->size; i++) col[i] = 0.0;
			col[j] = 1.0;
			lubksb(A, dof->size, indx, col);
			for (i=1; i<=dof->size; i++) covar[i][j] = col[i];			
		      }
		      break;

	  
		      // ------------------------------------------------------------------------------ //
		      // THIS BLOCK FOR CHOLESKY DECOMPOSITION, TO CALCULATE COVARIANCE MATRIX          //
		      // ------------------------------------------------------------------------------ //

		    case(2):
		      p = dvector(1,dof->size);
		      for (i=1; i<=dof->size; i++) p[i] = 0.0;  // initialize p
	      
		      choldc(A, dof->size, p);  // Cholesky decomp.
		      cholsl(A, dof->size, p, B, dof->data); // Cholesky back-substitution
		      // dof->data now contains all the best-fit fluxes (and background if fit_to_bg==1)
		      // now calculate the inverse of A to get Fisher matrix, by back-substitution
		      //cout << "done Cholesky decomposition." << endl;
		      // calculate the inverse of L first, stored in the lower triangle of A
		      for (i=1;i<=dof->size;i++) {
			A[i][i] = 1.0/p[i];
			for (j=i+1;j<=dof->size;j++) {
			  sum=0.0;
			  for (k=i;k<j;k++) sum -= A[j][k]*A[k][i];
			  A[j][i]=sum/p[j];
			}
			for (j=1;j<i;j++) {
			  A[j][i] = 0.0;	
			}  	
		      }
		      // then use L**-1 to calculate A**-1 and store in covar
		      // A**-1 = (L**-1)^T * L**-1
		      for (i=1;i<=dof->size;i++) {
			for (j=1;j<=dof->size;j++) {
			  sum = 0.0;
			  for (k=1; k<=dof->size; k++) {
			    sum += A[k][i] * A[k][j];
			  }
			  covar[i][j] = sum;
			}
		      }
		      break;	

	  
		    case (3):
		      // ------------------------------------------------------------------------------ //
		      // THIS BLOCK FOR IBG - ITERATIVE BICONJUGATE GRADIENT METHOD
		      // ------------------------------------------------------------------------------ //

		      nmax = dof->size + 10;
		      for (i=1;i<=dof->size;i++) {
			nmax=nmax+2*contaminant[i];
			//cout << "..." << contaminant[i]<< " " ;
		      }
		      nmax = MIN (nmax, 16777216); //, 2 * dof->size * dof -> size); 	
		      //cout << ">>> " << nmax <<endl;
		
		      /* Allocate memory for storing sparse matrix */
		      sa = dvector(1, nmax);
		      /* Allocate memory for storing row indexed array */
		      ija = ulvector(1, nmax);	    
		      sprsin(A, dof->size, thresh, (unsigned long)nmax, sa, ija); 
		      /// converts matrix a into vector sa indexed by vector ija (NumRec)
		      /// NB: still have to solve the system.
	      
		      n = (unsigned long)dof->size;
		      linbcg(sa, ija, n, B, dof->data, itol, tol, itmax, &iter, &erro); /// SOLVE IT.
		      /// itol = 4. tol = 1.e-128. x not initialized?!
		      //printf("\n");	  
		      free_dvector(sa, 1, nmax);
		      free_ulvector(ija, 1, nmax); 
		      //cout << "done IBG decomposition." << endl;

		      // Find covariance matrix:
		      indx = ivector(1,dof->size);  
		      for (i=1;i<=dof->size;i++) indx[i] = 1;  // initialize indx
		      col = dvector(1,dof->size);  //  columns of identity matrix

		      ludcmp(A, dof->size, indx, &d);
		      for (j=1; j<=dof->size; j++) {
			for (i=1; i<=dof->size; i++) col[i] = 0.0;
			col[j] = 1.0;
			lubksb(A, dof->size, indx, col);
			for (i=1; i<=dof->size; i++) covar[i][j] = col[i];}
		      break;

		    }


		  if (CLIP) { // Clip out negative sources and fit again
		    clipped=0;
		    j=0;
		    int jj;
		    for (jj=1;jj<=crowdsize+clippedtot+fit_to_bg;jj++){
		      //cout << "jj " << jj << endl;
		      if (!cl[jj]){
			j+=1;
			if (dof->data[j]<0 && fabs(dof->data[j])>nsigma*sqrt(covar[j][j])){		  
#ifdef VERBOSE
			  cout << "clipped: tot " << jj << " now " << j << " "<< dof->data[j] << " " << sqrt(covar[j][j]) << endl;
#endif
			  cl[jj]=1;
			  clipped++;
			}
		      }
		    }
		     
		    if (clipped) {
		      cout << "CLIPPED: " << clipped << " " << cliter << endl;}
		    else{
		      cout << "NO CLIPS " << cliter << endl;}
		  }
		   
		  if (clipped) {
		    // Free memory for next iteration
		    free_dmatrix(A,1,dof->size,1,dof->size);      
		    free_dvector(B,1,dof->size);
		    free_dmatrix(covar,1,dof->size,1,dof->size);
		    free_ivector(contaminant,1,crowdsize+fit_to_bg);
		     
		    if (linsyssolver!=2) {
		      free_ivector(indx, 1, dof->size);
		      free_dvector(col, 1, dof->size);}
		     
		  }

		} while (clipped);
			  
#ifdef VERBOSE
		if (cliter>1) cout << "Clipping Iterations: " << cliter << endl;
#endif	  
	  
		if (!clipped) {
		  /************** Output stuff starts here***********************/ 
#ifdef CELLS_ON_OBJ
#ifdef VERBOSE
		  cout << "Starting output" << endl;
		  if (item0 != NULL) 
		    {cout << "Next item: " << item0->Num() <<endl;}
		  else
		    {cout << "Next item is null, changing cell" << endl;}
#endif
#endif
		  printf("Cpu %d: Completed fitting on cell %ld/%ld \n", MEPROC, GRIDCOUNT, TOTCELLS);
		  if (MEPROC==1) fprintf(stderr,"\33[1M %d%s completed\n \33[1A",min(100,(int)(100.*GRIDCOUNT/(TOTCELLS/(nc*nr)))),"%");

		  /* Write results to output */
		  datout << setprecision(3); datout.setf( ios::fixed );
	    
		  if (exportCovariance) {
		    covout << endl << "#Cell id: " << GRIDCOUNT << endl;
		    covout << "#Arraydim: " << crowdsize+1 << " " << dof->size-fit_to_bg << endl;
	      
		    covout << "#Fitted fluxes follows .. " << endl;
		    for (j=1;j<=dof->size-fit_to_bg;j++)
		      covout << dof->data[j] << " ";
		    covout << endl;
		    covout << " " << endl;
	      
		    //Starts with the background row.
		    covout << "#Covar matrix follows... " << endl;
		    /*if (fit_to_bg) {
		      covout << "0 ";
		      for (j=1;j<=dof->size-fit_to_bg;j++)
			covout << covar[1][j] << " ";
		      covout << endl;
		    }*/
	      
		  }
		  tcount = 0 ; //fit_to_bg;  //There's an offset in dof->data & covar, because of sky.
		  
		  for (picker=0; picker<crowdsize+clippedtot; picker++) {
		    
		    if (cl[picker+1]){
		      pnt = crowd[picker];
		    }
		    else{
		      pnt = crowd[picker]; 
		      tcount++;
		      datout << setw(6) << pnt->Num() << " " ; //which object		      
		      datout.setf(ios::fixed,ios::floatfield | ios::showpoint);
		      pnt->getCoords(&xx,&yy); //x,y
		      
		      datout << setprecision(2); 
		      datout << setw(8) << xx << " " << setw(8) << yy << " ";
		      
		      //Floating cell number & distance from cell center.
		      
		      datout << setw(4) << GRIDCOUNT << " " ;
		      
		      xc = newcell.Xpos(xx);
		      yc = newcell.Ypos(yy);
		      datout << setprecision(2);
		      datout << setw(8) << xc << " " << setw(8) << yc << " ";
		      
		      rcell = newcell.RadialDistance(xx,yy);
		      datout << setprecision(4); 		      
		      datout << setw(8) << rcell << " ";
		      datout << scientific;
		      datout << setprecision(6); 
		      if (use_cellmask){
			oberr=sqrt(covar[tcount][tcount]); //* sqrt(chi2/(new_npix - dof->size));
		      } else {
			oberr=sqrt(covar[tcount][tcount]); // * sqrt(chi2/(newcell.Npix() - dof->size));
		      }
		      datout << setw(12) << dof->data[tcount] << " "; //measured flux (scale factor)
		      datout << setw(12) << oberr << " "; //error
		      //datout.setf(ios::floatfield | ios::showpoint);
		      //datout << setw(10) << facZP * pnt->getSExFlux() << " " << setw(10) << facZP * fabs(pnt->getBG()); //getISOFlux();
		      datout << pnt->getSExFlux() << " " << pnt->getBG(); //getISOFlux();
		         
		      float errfac=0.0;  
		      
		      if (exportCovariance) { 
			covout << pnt->Num()<< " " ;
			for (j=1;j<=dof->size-fit_to_bg;j++)
			  {covout << covar[tcount][j] << " ";
			    if (j!=tcount) {

			      /*crowd[j-1]->getLimits(&oxd,&oyd,&oxu,&oyu);
			      errfac+=pow(fabs(Acopy[tcount][j]/Acopy[tcount][tcount])*0.5*dof->data[j],2.)*blends[picker+1][j]/((double)((oxu-oxd)*(oyu-oyd)));*/
			      //errfac+=pow(fabs(Acopy[tcount][j]/Acopy[tcount][tcount])*blends[picker+1][j]/(dof->data[tcount]+dof->data[j])*0.5*dof->data[j],2.);
			      /*double ATi=0.0, ATj=0.0;
			      crowd[j-1]->getLimits(&oxd,&oyd,&oxu,&oyu);
			      ATj=(double)((oxu-oxd)*(oyu-oyd));
			      pnt->getLimits(&oxd,&oyd,&oxu,&oyu);
			      ATi=(double)((oxu-oxd)*(oyu-oyd));
			      errfac+=pow(dof->data[j]*blends[picker+1][j]*dof->data[j]/((ATi*dof->data[tcount]+ATj*dof->data[j])),2.);*/
			      //if (blendedpriors[(crowd[j-1]->Num()-1)*totnumsources + (pnt->Num()-1)]){
#ifdef VERBOSE
			      //cout << "Blended priors: " << pnt->Num() << " " << crowd[j-1]->Num() << " " << Acopy[tcount][j] << " " << Acopy[tcount][tcount] << endl;
#endif
				/*crowd[j-1]->getLimits(&oxd,&oyd,&oxu,&oyu);
				float L; float dr,xxxx,yyyy;
				L=0.5*sqrt((float)((oxu-oxd)*(oxu-oxd)+(oyu-oyd)*(oyu-oyd)));
				crowd[j-1]->getCoords(&xxxx,&yyyy);
				dr=sqrt((xxxx-xx)*(xxxx-xx)+(yyyy-yy)*(yyyy-yy));
				//errfac+=pow(MAX(0.0,(1.-dr/L))*fabs(Acopy[tcount][j]/Acopy[tcount][tcount])*dof->data[j],2.);
				errfac+=pow(0.1*pow(2.7,(-dr/L))*fabs(Acopy[tcount][j]/Acopy[tcount][tcount])*dof->data[j],2.);*/
				//errfac+=pow(fabs(covar[tcount][j]/covar[tcount][tcount])*dof->data[j],2.);
				errfac+=dof->data[j];
				//errfac+=pow(fabs(Acopy[tcount][j]/Acopy[tcount][tcount])*dof->data[j],2.);
				//}
			      //errfac+=fabs(covar[tcount][j]);
			    }
			  }
			covout << endl;

		      }
		      //errfac=MIN(errfac,pow(fabs(dof->data[tcount]),2));
		      //datout << setw(12) << 3.0*oberr; //+MAX(0.0,(errfac-dof->data[tcount])); //newerror
		      int flaggy = 0;
		      pnt->getLimits(&oxd,&oyd,&oxu,&oyu);

		      if (pnt->getBG() < 0.) {flaggy = 1;} // Saturation
		      if (blendedpriors[0]>=0) // Blended priors
			{
			  int ii,jj;
			  id=pnt->Num();
			  for (ii=0; ii<nblended; ii++){
			    if (blendedpriors[ii]==id){
			      flaggy+=2;break;}
			  }
			}

		      if ((oxd<1) || (oyd<1) || (oxu>Width-1) || (oyu>Height-1))
			{flaggy += 4;} // Borders

		      datout << setw(4) << flaggy << " ";
		      datout << endl;

		      // Collect up total flux: only once per object.
		      if (pnt->isFlagged() == 1) {
			totflux += pnt->getISOFlux() * dof->data[tcount];
		      }
		    
		    } // end this obj
		  		  		    
		  } // end looping over crowd
	    
		  if (fit_to_bg) {
		    picker=crowdsize+clippedtot+1;
		    cout << "Background for cell" << GRIDCOUNT << ": " << dof->data[picker] << " (per pixel:" << dof->data[picker]/(float)(newcell.Xdim()*newcell.Ydim())<< ")" << endl;
		  }

		  // Write cell stats for this cell
		  cellout << setw(4) << GRIDCOUNT << setw(4) << crowdsize << " " ;
		  cellout << setw(6) << newcell.Xdim() << " " <<  setw(6) << newcell.Ydim() << " ";
		  cellout << setw(6) << newcell.Lx() << " " << setw(6) << newcell.Ly() << " ";
		  cellout << setw(6) << XMIN << " " << setw(6) << YMIN << " ";
		  cellout << setprecision(4); 

		  cellout.setf(ios::floatfield | ios::showpoint);
		  /*if (fit_to_bg == 1) {
		    cellout << setw(8) << dof->data[1] << " "; // background
		    cellout << setw(8) << sqrt(covar[1][1]) << " "; //error
		  } else {
		    cellout << "...Not Fitted..." << " ";
		  }*/

		  if (use_cellmask) {		
		    cellout << setw(10) << (new_npix - dof->size) << " ";	      
		  } else {
		    cellout << setw(10) << (newcell.Npix() - dof->size) << " ";
	      
		  }
		  cellout << setw(7) << new_npix;
		  cellout << endl;
		}
	  
		// Free memory //
		free_dmatrix(A,1,dof->size,1,dof->size);      
		//free_dmatrix(Acopy,1,dof->size,1,dof->size);      
		free_dvector(B,1,dof->size);
		free_dmatrix(covar,1,dof->size,1,dof->size);
		free_ivector(contaminant,1,crowdsize+fit_to_bg);
		free_ivector(cl,1,crowd.size());
	  
		if (linsyssolver!=2) {
		  free_ivector(indx, 1, dof->size);
		  free_dvector(col, 1, dof->size);}

	      } // Finish check that floating cell has objects
	         
	  }	  
      
      } // Finish looping over grids and dither pattern
	
	//free_dmatrix(blends,1,totnumsources,1,totnumsources); 
	if (exportCovariance) covout.close();
	datout.close();
	cellout.close();
	
	if (MEPROC==1) fprintf(stderr,"\33[1M %d%s completed\n \33[1A",100,"%");
	
	QFREE(pixels); 
	QFREE(pixels_rms);
	
}


/* ------------------------------------------------------------------------------ */

int buildlinearsystem(double **a, double *b, 
		      double *pixels, double *pixels_rms,
		      char *meas_im, char *rms_im, char *indir, 
		      int fitbg, long Width, long Height,
		      float ThresholdFactor, 
		      long CellXdim, long CellYdim,
		      long CellXl, long CellYl,
		      catalogstruct* Catalog, 
		      int NumSources, int usemask, int gridcount) 
{

  int i, j, m, n, ii, jj, iii, jjj, jini, jjjini;
  int xmin1, xmax1, ymin1, ymax1, xmin2, xmax2, ymin2, ymax2;
  int x_in1, x_in2, y_in1, y_in2;
  int xomin, xomax, yomin, yomax;
  int owidth,oheight;
  int dx1, dx2, dy1, dy2;
  int status = 0;   /*  CFITSIO status value MUST be initialized to zero!  */
  long firstpix1[2] = {1,1};
  long firstpix2[2] = {1,1};
  double *th1, *th2, *th3;
  fitsfile *fptr3, *fptr4;
  double *pixels_th1, *pixels_th2;
  int *Mask;
  long fpixel[2] = {1,1};
  catalogstruct *pCatalog1, *pCatalog2;
  char modname[BUFFERSIZE];
  long naxes[2] = {1,1};
  int bitpix, naxis, rows1, columns1, rows2, columns2, area, cccc=0;
  double sigma2,temp1,temp2,temp3;
  double check1, check2, check3;
  double Threshold1, Threshold2, maxv;
  double TOL=1.e-12;

  QMALLOC(Mask, int, CellXdim*CellYdim);
  if (usemask) {
    // Mask out bad pixels
    for (i=0;i<CellXdim*CellYdim;i++) Mask[i]=0;
    pCatalog1 = Catalog;
    for (i = 1; i <= NumSources; i++) {
      // Read thumbnail limits 
      xmin1 = pCatalog1->xmin_image; 
      ymin1 = pCatalog1->ymin_image; 
      sprintf(modname, "%s/%s-%d.fits", indir, "mod", pCatalog1->id);
      if(!fits_open_file(&fptr3, modname, READONLY, &status)) {
	fits_get_img_param(fptr3, 2, &bitpix, &naxis, naxes, &status);
	columns1=naxes[0];
	rows1=naxes[1];
	area=rows1*columns1;
	QMALLOC(pixels_th1, double, area);
	fits_read_pix(fptr3, TDOUBLE, fpixel, area, NULL,
		      pixels_th1, NULL, &status);   
 	fits_close_file(fptr3, &status);
	fptr3=NULL; 
	for (jj=0;jj<rows1;jj++){
	  for (ii=0;ii<columns1;ii++){
	    if (pixels_th1[jj*columns1+ii]>TOL){
	      //Find corresponding pixel in cell
	      Mask[(ymin1-CellYl+jj)*CellXdim+(xmin1-CellXl+ii)]+=1;	      
	      cccc+=1;
	    }}}
	QFREE(pixels_th1);
      }
      if (i<NumSources) pCatalog1++;
    }
  }
  else {
    for (ii=0;ii<CellXdim;i++)
      for (jj=0;jj<CellYdim;jj++) {
	if (pixels[(jj+CellYl)*Width+(ii+CellXl)]>TOL){
	  Mask[jj*CellXdim+ii]=1;
	  cccc+=1;
	}
      }
  }

  /*
  printf("%d %d\n",CellXdim,CellYdim);
    for (j=0;j<CellYdim;j++){
    for (i=0;i<CellXdim;i++){
    printf("%d ",Mask[j*CellXdim+i]);}
    printf("\n");}
  */

  if (!cccc) {
#ifdef DEBUG
    cout << "No good pixel found, returning" << endl;
#endif
    return(1);
  }  

  // Begin real Matrix construction  
  pCatalog1 = Catalog;

  for (i = 1; i <= NumSources+fitbg; i++) {

#ifdef VERBOSE
    printf("\33[1M> Building system row: %7d / %-7d (ID: %d)\n\33[1A", i, NumSources, pCatalog1->id);
#endif

    /* Initialize right-hand side vector */
    b[i] = 0.0;
    contaminant[i]=0;

    if (i>NumSources)
      {
	columns1=CellXdim;
	rows1=CellYdim;
	area=rows1*columns1;
	QMALLOC(pixels_th1, double, area);
	for (ii=0;ii<area;ii++) pixels_th1[ii]=1.0/(float)area;
	xmin1=CellXl+1;
	xmax1=CellXl+CellXdim-1;
	ymin1=CellYl+1;
	ymax1=CellYl+CellYdim-1;
#ifdef DEBUG
	printf(">°°° th 1 background allocated\n");
#endif
      }
    else
      {
	sprintf(modname, "%s/%s-%d.fits", indir, "mod", pCatalog1->id);
	if(!fits_open_file(&fptr3, modname, READONLY, &status))
	  {
	    // Read thumbnail limits 
	    xmin1 = pCatalog1->xmin_image; 
	    xmax1 = pCatalog1->xmax_image; 
	    ymin1 = pCatalog1->ymin_image; 
	    ymax1 = pCatalog1->ymax_image; 
	    
	    /* Open object "i" template thumbnail. 
	       Simply use fits_open to avoid repeating long checks */
	    fits_get_img_param(fptr3, 2, &bitpix, &naxis, naxes, &status);
	    columns1=naxes[0];
	    rows1=naxes[1];
	    area=rows1*columns1;
	    QMALLOC(pixels_th1, double, area);
	    fits_read_pix(fptr3, TDOUBLE, fpixel, area, NULL,
			  pixels_th1, NULL, &status);
	    fits_close_file(fptr3, &status);
	    fptr3=NULL;
	    /*if (usemask) {
	      for (ii=0; ii<area; ii++) 
	      {if (fabs(pixels_th1[ii])<TOL) pixels_th1[ii]=0.e0;}}*/
	    
	  }
	else
	  {
	    return(status);
	  }
      }
     
    if (ThresholdFactor>TOL) 
      {jini=1;
	pCatalog2=Catalog;
	maxv=0.e0;
	for (ii=0;ii<area;ii++) maxv=MAX(maxv,pixels_th1[ii]);
	Threshold1=ThresholdFactor*maxv;	  
      }
    else
      {jini=i;
	pCatalog2=pCatalog1;}
      
    for (j = jini; j <= NumSources+fitbg; j++) { 
	
#ifdef VERBOSE
      printf("> building matrix A[%d][%d]\n",i,j);
#endif
	
      /* Initialize coefficient matrix */
      a[i][j] = 0.0;

      // Read thumbnail limits 
      if (j<=NumSources) 
	{
	  xmin2 = pCatalog2->xmin_image; 
	  xmax2 = pCatalog2->xmax_image; 
	  ymin2 = pCatalog2->ymin_image; 
	  ymax2 = pCatalog2->ymax_image; 
	}
      else
	{
	  xmin2=CellXl+1;
	  xmax2=CellXl+CellXdim-1;
	  ymin2=CellYl+1;
	  ymax2=CellYl+CellYdim-1;
	}

      dx1 = xmax2 - xmin1;
      dx2 = xmax1 - xmin2;
      dy1 = ymax2 - ymin1;
      dy2 = ymax1 - ymin2;
	  
      /* Check for thumbnail overlap */
      if ((I_SIGN(dx1) == I_SIGN(dx2)) && (I_SIGN(dy1) == I_SIGN(dy2))) {

	if (j>NumSources) 
	  {
	    columns2=CellXdim;
	    rows2=CellYdim;
	    area=rows2*columns2;
	    QMALLOC(pixels_th2, double, area);
	    for (ii=0;ii<area;ii++) pixels_th2[ii]=1.0/(float)area;
#ifdef DEBUG
	    printf(">** th 2 background allocated\n");
#endif
	  }
	else
	  {				
	    sprintf(modname, "%s/%s-%d.fits", indir, "mod", pCatalog2->id);
	    if (!fits_open_file(&fptr4, modname, READONLY, &status)) {
#ifdef DEBUG
	      printf(">** th 2 ID %d opened with status %d\n",pCatalog2->id,status);
	      if (status) return(status);
#endif
	      fits_get_img_param(fptr4, 2, &bitpix, &naxis, naxes, &status);
	      columns2=naxes[0];
	      rows2=naxes[1];
	      area=rows2*columns2;
	      QMALLOC(pixels_th2, double, area);
	      fits_read_pix(fptr4, TDOUBLE, fpixel, area, NULL,
			    pixels_th2, NULL, &status);
	      fits_close_file(fptr4, &status);
	      fptr4=NULL;
	      /*if (usemask) {
		for (ii=0; ii<area; ii++)
		{if (fabs(pixels_th2[ii])<TOL) pixels_th2[ii]=0.e0;}}*/
	    }
	    else
	      {
		return(status);
	      }
	  }	  
	      
	/* Set overlap box in LRI ref */
	xomin = MAX(xmin1, xmin2);
	xomax = MIN(xmax1, xmax2);
	yomin = MAX(ymin1, ymin2);
	yomax = MIN(ymax1, ymax2);
	owidth = xomax-xomin+1;
	oheight = yomax-yomin+1;
#ifdef VERBOSE
	if (i<=NumSources && j<=NumSources)
	  printf("Overlap: %d %d: %d %d\n",pCatalog1->id,pCatalog2->id,owidth,oheight);
#endif	    
	      
	//Compute flux in the overlapping region (for systematic error)
	//double ovfl=0.0;
	//ovfl=(double)(owidth*oheight);
	//for (jj=xomin;jj<=xomax;jj++){
	//  for (ii=yomin;ii<=yomax;ii++){
	//    ovfl+=pixels[ii*Width+jj];
	//    }}
	//blends[i][j]=ovfl;
	//blends[j][i]=ovfl;

#ifdef DEBUG
	fprintf(stderr,"> th1 = %d %d %d %d\n",xmin1,xmax1,ymin1,ymax1);
	fprintf(stderr,"> th2 = %d %d %d %d\n",xmin2,xmax2,ymin2,ymax2);
	fprintf(stderr,"> overlap = %d %d %d %d -> %d x %d\n",
		xomin,xomax,yomin,yomax,owidth,oheight);
#endif
	      
	/*if (ThresholdFactor>TOL) {
	  maxv=0.e0;
	  for (ii=0;ii<area;ii++) maxv=MAX(maxv,pixels_th2[ii]);
	  Threshold2=ThresholdFactor*maxv;}*/
	      
	/* Set overlap box in thumbnails ref*/
	x_in1=MAX(0,xmin2-xmin1); 
	x_in1=MAX(x_in1,columns1-xmax1-1);
	y_in1=MAX(0,ymin2-ymin1);
	y_in1=MAX(y_in1,rows1-ymax1-1);
	x_in2=MAX(0,xmin1-xmin2);
	x_in2=MAX(x_in2,columns2-xmax2-1);
	y_in2=MAX(0,ymin1-ymin2);
	y_in2=MAX(y_in2,rows2-ymax2-1);

	if (ThresholdFactor<TOL)
	  {
	    ii=0;
	    for (n = yomin; n <= yomax; n++) {
	      jj=0;
	      for (m = xomin; m <= xomax; m++) {
		if(Mask[(n-CellYl)*CellXdim+(m-CellXl)]>0){
		  sigma2=pixels_rms[n*Width+m];
		  sigma2=sigma2*sigma2;
#ifdef DEBUG		    
		  if (sigma2<1.e-10)
		    {printf("TERROR: sigma - %d %d %d %d %ld %ld, %f\n",m,n,ii,jj,Width,n*Width+m,sigma2);}
#endif
		  temp1=pixels_th1[(y_in1+ii)*columns1+(x_in1+jj)];
		  temp2=pixels_th2[(y_in2+ii)*columns2+(x_in2+jj)];
		  //if (usemask) {if (temp1<TOL && temp2<TOL) {temp1=0.0; temp2=0;}}
		  a[i][j] += (temp1*temp2) / sigma2 ;
		}
		jj++;
	      }
	      ii++;
	    }
	  }
	else 
	  {	    
	    ii=0;
	    for (n = yomin; n <= yomax; n++) {
	      jj=0;
	      for (m = xomin; m <= xomax; m++) {
		if(Mask[(n-CellYl)*CellXdim+(m-CellXl)]>0){
		  sigma2=pixels_rms[n*Width+m];
		  sigma2=sigma2*sigma2;
#ifdef DEBUG
		  if (sigma2<1.e-10) 
		    {printf("TERROR: sigma - %d %d %d %d %ld %ld, %f\n",m,n,ii,jj,Width,n*Width+m,sigma2);}
#endif
		  temp1=pixels_th1[(y_in1+ii)*columns1+(x_in1+jj)];
		  temp2=pixels_th2[(y_in2+ii)*columns2+(x_in2+jj)];
		  //if (usemask) {if (temp1<TOL && temp2<TOL) {temp1=0.0; temp2=0;}}
		  //if (temp1>Threshold1 && temp2>Threshold2) a[i][j] += (temp1*temp2) / sigma2;
		  if (temp1>Threshold1) a[i][j] += (temp1*temp2) / sigma2;
		}
		jj++;
	      }
	      ii++;
	    }
	  }

#ifdef DEBUG
	if (a[i][j]<-1.e10) {printf("________%d %d: %g\n",i,j,a[i][j]);}
#endif

#ifdef FLUXPRIOR
	if (i==j)
	  {
	    a[i][j] += pCatalog1->fpf/pow(pCatalog1->epf,2);
	  }
#endif
	
	QFREE(pixels_th2);

	if (j<=NumSources)
	  {
#ifdef DEBUG
	    printf(">** th 2 ID %d closed with status %d\n",pCatalog2->id,status);
	    if (status) return(status);
#endif
	  }
	else
	  {
#ifdef DEBUG
	    printf(">** th 2 background closed with status %d\n",status);
	    if (status) return(status);
#endif
	  }

	if((i != j) && (a[i][j] != 0))
	  {
	    contaminant[i]++;
#ifdef DEBUG
	    printf("contaminant[%d]: %d\n",i,contaminant[i]);
#endif
	  }

#ifdef DEBUG
	printf("\33[1M\n\33[1A");
#endif
      }

      if (j<NumSources) pCatalog2++;
    }

#ifdef VERBOSE
    printf("> building right-hand side vector b[%d]\n",i);
#endif

    x_in1=MAX(0,columns1-xmax1-1);
    y_in1=MAX(0,rows1-ymax1-1);
    if (ThresholdFactor<TOL)
      {
	ii=0;
	for (n = ymin1; n <= ymax1; n++) {
	  jj=0;
	  for (m = xmin1; m <= xmax1; m++) {
	    if(Mask[(n-CellYl)*CellXdim+(m-CellXl)]>0){
	      sigma2=pixels_rms[n*Width+m];
	      sigma2=sigma2*sigma2;
#ifdef DEBUG		    
	      if (sigma2<1.e-10)
		{printf("TERROR: sigma - %d %d %d %d %ld %ld, %f\n",m,n,ii,jj,Width,n*Width+m,sigma2);}
#endif
	      temp1 = pixels_th1[(y_in1+ii)*columns1+(x_in1+jj)];
	      b[i] += (temp1 * pixels[n*Width+m] ) / sigma2 ;
	    }
	    jj++;
	  }
	  ii++;
	}}
    else
      {
	ii=0;
	for (n = ymin1; n <= ymax1; n++) {
	  jj=0;
	  for (m = xmin1; m <= xmax1; m++) {
	    if(Mask[(n-CellYl)*CellXdim+(m-CellXl)]>0){
	      sigma2=pixels_rms[n*Width+m];
	      sigma2=sigma2*sigma2;
	      temp1 = pixels_th1[(y_in1+ii)*columns1+(x_in1+jj)];
	      if (sigma2<=0.0){
		printf("sigma2: %f %d %d %d\n",pixels_rms[n*Width+m],n,m,Width);exit(1);}
	      if (temp1>Threshold1) b[i] += (temp1 * pixels[n*Width+m] ) / sigma2 ;
	    }
	    jj++;
	  }
	  ii++;
	}}

#ifdef FLUXPRIOR
    b[i] += pCatalog1->fpf*pCatalog1->pf/pow(pCatalog1->epf,2);
#endif
   
    QFREE(pixels_th1);

    if (i<=NumSources)
      {
#ifdef DEBUG
	printf(">°°° th 1 ID %d closed with status %d\n",pCatalog1->id,status);
	if (status) return(status);
#endif
      }
    else
      {
#ifdef DEBUG
	printf(">°°° th 1 background closed with status %d\n",status);
	if (status) return(status);
#endif
      }

    if (i<NumSources) pCatalog1++;

  }

  /* Fill symmetric matrix elements */
  if(ThresholdFactor<TOL) {
#ifdef VERBOSE
    cout << "Filling symmetric matrix elements" << endl;
#endif
    for (i = 1; i <= NumSources+fitbg; i++) {
      for (j = 1; j < i; j++) {
	a[i][j] = a[j][i];
      }}}

  /*pCatalog1=Catalog;
    for (ii=1;ii<=NumSources;ii++){
    printf("%d %d: %f \n",ii, pCatalog1->id,a[ii][ii]);
    pCatalog1++;}*/
  
  /*pCatalog1=Catalog;
  for (i=1;i<=NumSources;i++){
    printf("_%d_____%d:  ",i,pCatalog1->id);
    for (j=1;j<=NumSources;j++){
      printf("%g ",a[i][j]);}printf("\t");
    printf("   %g\n",b[i]);pCatalog1++;}
  printf("Finished constructing matrices\n");*/
  
  QFREE(Mask);

  if (status) fits_report_error(stderr, status); /* print any error message */

  return(status);
}


//------------------------------------------------------------------------------------------------//

void apertures(int naper, double *f, double *errf, 
	      float Rmax, int binsubpix, float *aper,
	      double *pixels_aper, double *pixels_aper_rms, 
	      int W, int H, float x0, float y0)
{
  double facareasub, dsubpix, R, R2, fbkgd;
  double *facarea;
  float x, y, xp, yp, xm, ym, 
    xa, ya, xb, yb, dx, dy, dsx, dsy;
  int ap, xc, yc, xmin, xmax, ymin, ymax, 
    i, j, iii, jjj, keep_pix;

  dsubpix=1.0/(float)binsubpix;
  facareasub=dsubpix*dsubpix;

  /*for (j=0;j<H;j++){
    for (i=0;i<W;i++){
      cout << pixels_aper[W*j+i]<< " ";
    }
    cout << endl;
    }*/

  x0=(x0-floor(x0))+0.5*(W-1); // Position in thumbnail
  y0=(y0-floor(y0))+0.5*(H-1);

  xmin=0;
  xmax=W-1;
  ymin=0;
  ymax=H-1;

  facarea = (double *) malloc(naper * sizeof(double));

  for(ap=naper-1;ap>-1;ap--) // Loop on apertures, starting from larger
    {
      facarea[ap]=0.0;
      f[ap]=0.0 ; 
      errf[ap]=0.0; 
    }

  xc=floor(x0-0.5e0);
  yc=floor(y0-0.5e0);
  
  for (j=ymin;j<=ymax;j++)
    for(i=xmin;i<=xmax;i++) // Loop on pixels of this object
      {
	x=(float)i;
	y=(float)j;
	xp=x+0.5;
	yp=y+0.5;
	xm=x-0.5; // lower x bound of i pixel
	ym=y-0.5; // lower y bound of i pixel
	
	// "a"=farthest point; "b"=closest point
	if (ym>(float)yc+0.5) // Upper region
	  {
	    if (xm>(float)xc+0.5){xa=xp; ya=yp; xb=xm; yb=ym;} // 1st quad
	    else if (xp<(float)xc-0.5){xa=xm; ya=yp; xb=xp; yb=ym;} // 2nd quad
	    else { // Same column, above
	      if (x0>=xc) {xa=xm; ya=yp; xb=x0; yb=ym;}
	      else {xa=xp; ya=yp; xb=x0; yb=ym;}
	    }
	  }
	else if (ym<(float)yc-0.5) // Lower region
	  {
	    if (xm<(float)xc-0.5){xa=xm; ya=ym; xb=xp; yb=yp;} // 3rd quad
	    else if (xp>(float)xc+0.5){xa=xp; ya=ym; xb=xm; yb=yp;} // 4th quad
	    else{ // Same column, below
	      if (x0>=xc) {xa=xm; ya=ym; xb=x0; yb=yp;}
	      else {xa=xp; ya=ym; xb=x0; yb=yp;}
	    }
	  }
	else // Same row
	  {
	    if (i>xc){ // Right
	      if (y0>=yc) {xa=xp; ya=ym; xb=xm; yb=y0;}
	      else {xa=xp; ya=yp; xb=xm; yb=y0;}
	    }
	    else if (i<xc){ // Left
	      if (y0>=yc) {xa=xm; ya=ym; xb=xp; yb=y0;}
	      else{xa=xm; ya=yp; xb=xp; yb=y0;}
	    }
	  }
	
	keep_pix=1;
	
	for(ap=naper-1;ap>-1;ap--) // Loop on apertures, starting from larger
	  {	
	    
	    if (keep_pix) // If the pixel has been excluded before, no need to check it again
	      {

		R=0.5*aper[ap]; // First loop has R=Rmax
		R2=R*R;
		facarea[ap]=1.0;
		fbkgd=0.0;
		
		// Check if the pixel is totally outside R
		if ((xb-x0)*(xb-x0)+(yb-y0)*(yb-y0)>R2){
		  facarea[ap]=0.0;
		  // Also, don't need to check this pixel anymore
		  keep_pix=0;
		}
		// Now check if it's totally inside R
		else if ((xa-x0)*(xa-x0)+(ya-y0)*(ya-y0)>=R2){
		  // Must do subgrid
		  facarea[ap]=0.0;
		  dx=xm-x0;
		  dy=ym-y0;
		  for (jjj=0;jjj<binsubpix;jjj++)
		    for (iii=0;iii<binsubpix;iii++)
		      {
			dsx=dx+((float)iii+0.5)*dsubpix;
			dsy=dy+((float)jjj+0.5)*dsubpix;
			if (dsx*dsx+dsy*dsy<R2) facarea[ap]+=facareasub;
		      }
		  //printf("---> %d %d (%d) %f\n",i,j,ap,facareasub);
		}
	      }
	    
	    f[ap]+=facarea[ap]*pixels_aper[j*W+i];
	    errf[ap]+=(facarea[ap]*pixels_aper_rms[j*W+i])*(facarea[ap]*pixels_aper_rms[j*W+i]);

	    // Subtract background?
	    f[ap]-=facarea[ap]*fbkgd;
	    
	  } // end loop on aper
	
      } // End loop on pixels; f[ap] is the flux within aperture aper[ap]
  
  for (ap=naper-1;ap>-1;ap--) errf[ap]=sqrt(errf[ap]); //+MAX(0.0,f[ap])/GAIN);    
  free(facarea);   
}
