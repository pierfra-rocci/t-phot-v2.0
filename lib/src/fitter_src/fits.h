// this is the header file for the object used to store a fits
// file as a matrix - it implements the numerical recipe routines
//
// last updated 26 Jan 1999

#ifndef FITS_H
#define FITS_H
#include "box.h"

using namespace std;

class Fits {
  char *title;
  friend ostream &operator<<(ostream &stream, Fits &fits);
  friend class Box;
public :
  int xsize,ysize;
  int xmin,ymin;
  int xmax,ymax;
  float crpix1, crpix2;
  //float xcorner,ycorner;
  Box xyrange;
  int PixType;
  float totflux;
  int **idata;
  float **data;
  long **ldata;

  Fits();
  Fits(int, int);
  Fits(int,int,int,int);
  Fits(int,int,int,int,int);
  Fits(int,int,int,int,int,float);
  Fits(int, int, char *, int);
  ~Fits();
  int rnd(float);
  char *Title();
  void newTitle(char *);
  void scale(float,float,Fits *);
  void resetPix(int,int);
  void setPix(float);
  void convolve(Fits *, Fits *);
  void subtract(float);
  void Flatten(Box, double* holder);
  void Flatten(Box, long* holder);
  void Flatten(Box, double** holder, long row);
  // bool isInside(float, float, float, float); // deprecated
};

#endif
