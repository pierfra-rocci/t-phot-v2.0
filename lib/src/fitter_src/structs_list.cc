#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "fitsio.h"
#include "nrutil.h"
#include "define.h"
#include "fits.h"
#define NR_END_ME 1
#define FREE_ARG_ME char*
#include "structs.h"

#define FLUXPRIOR

List::List() { 
  top = NULL; 
  size=0; 
}

List::List(gNode *t) { 
  if (t==NULL) {
    top = t; 
    size = 1;
  }
  else {
    top = NULL;
    size = 0;
  }
}

List::~List() {
   gNode *current = top, *temp;
   while (current != NULL) {
     temp = current;
     current = current->next;
     delete temp;
   }
}

long List::Size() { return size; }

void List::push(long l,float x, float y, float bg, float fx, float fwhm, 
		int flag) {
  gNode *temp;  
  temp = new gNode(l,x,y,bg,fx,fwhm,flag,top);
  //  temp->setEllipse(a,b,theta);
  top = temp;
  size++;
}

#ifdef FLUXPRIOR
void List::push(long l,float x, float y, 
		int xmin, int ymin, int xmax, int ymax,
		float bg, float fx, float fpf, float pf, float epf,
		int flag) {
  gNode *temp;  
  temp = new gNode(l,x,y,xmin,ymin,xmax,ymax,bg,fx,fpf,pf,epf,flag,top);
  top = temp;
  size++;
}
#else
void List::push(long l,float x, float y, 
		int xmin, int ymin, int xmax, int ymax, 
		float bg, float fx, int flag) {
  gNode *temp;  
  temp = new gNode(l,x,y,xmin,ymin,xmax,ymax,bg,fx,flag,top);
  top = temp;
  size++;
}
#endif

void List::build(long l,long g, int num, float x, float y, float bg, float tbg,
		 float flx, float scale) {
  gNode *temp = Top();
  temp = new gNode(l,g,num,x,y,bg,flx,scale,top);
  temp->setTBG(tbg);
  top = temp;
  size++;
}

void List::pushToEnd(long l,float x,float y, float bg, float fx, 
		     float fwhm, int flag, long g) {
  gNode *temp = Top();
  gNode *newGuy = new gNode(l,x,y,bg,fx,fwhm,flag,NULL);
  newGuy->changeGroup(g);
  while (Next(temp) != NULL)
    temp = Next(temp);
  temp->next = newGuy;
  size++;
}

gNode* List::Find(long num) {
  int i,j,done=0;
  gNode *current;
  current = Top();

  while (current != NULL) {
    // cout << "% " << current->Num() << " " << num << endl;
    if (current->Num() == num) return current;
    current = Next(current);
  }

  //  cout << "Warning: I could not locate object number " << num << endl;
  //  cout << "Returning values for object number " << num-1 << endl;
  //  cout << endl;
  return this->Find(num-1);
}

gNode* List::pop() {
  gNode *temp;
  if (top != NULL) {
    top = top->next;
    size--;
    return temp;
  }
  return NULL;
}

gNode* List::Top() {
  return top;
}

gNode* List::Next(gNode *n) {
  return n->next;
}
