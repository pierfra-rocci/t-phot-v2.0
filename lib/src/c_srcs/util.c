#include <string.h>
#include <stdio.h>
#include "fitsio.h"
#include <tphot.h>
#include "globals.h"

#define	MAXCATALOG 465536 // Max number of sources
#define MAXMASK 1048576
//#define INTERACTIVE

extern float CRPIX1,CRPIX2;
extern double CRVAL1, CRVAL2;
extern double CD1_1, CD2_2;
extern double *pixels_kern;
extern char *CTYPE1, *CTYPE2;

uchar Offset;
long Width, Height;
long MeasXoff, MeasYoff;
long MeasXmin, MeasXmax, MeasYmin, MeasYmax;
long MeasWidth, MeasHeight;
long OverlapWidth, OverlapHeight;
catalogstruct *pCatalog;

void WriteThumbnail (char *dstdir, char *prefix, int id, int ThWidth, int ThHeight, double totflux, double *image, char *kern) {

  void TestFitsExistence(char *FitsName);

  fitsfile *thptr;
  char thname[BUFFERSIZE];
  int status = 0; /*  CFITSIO status value MUST be initialized to zero! */
  int i;
  long firstpix[2] = {1,1};
  double *Row, *ptr;
  void WriteHeader(fitsfile *fptr, long naxis1, long naxis2, double totflux, char *kern);

  /* Allocate memory for storing double image row */
  QMALLOC(Row, double, ThWidth);
  
  sprintf(thname, "%s/%s-%d.fits", dstdir, prefix, id);
#ifdef INTERACTIVE
  TestFitsExistence(thname);
#endif
  remove(thname);
  if (fits_create_file(&thptr, thname, &status)) {
    fits_report_error(stderr, status);
    exit(EXIT_FAILURE);
  }
  
  WriteHeader(thptr, ThWidth, ThHeight, totflux, kern);
  
  ptr = image;
  for (firstpix[1] = 1; firstpix[1] <= ThHeight; firstpix[1]++) {
    for (i = 0; i < ThWidth; i++) {
      Row[i] = *ptr;
      ++ptr;
    }
    if(fits_write_pix(thptr, TDOUBLE, firstpix, ThWidth, Row, &status)) {
      fits_report_error(stderr, status);
      exit(EXIT_FAILURE);
    }          
  }
  
  QFREE(Row);
  
  if(fits_close_file(thptr, &status)) {
    fits_report_error(stderr, status);
    exit(EXIT_FAILURE);
  }    
}

void WriteHeader(fitsfile *fptr, long naxis1, long naxis2, double totflux, char *kern) {
  char keyword[FLEN_KEYWORD];
  char comment[FLEN_COMMENT];
  double dbl_value;
  int status = 0; /*  CFITSIO status value MUST be initialized to zero!  */
  int int_value, decim=6;
  long lng_value;

  //SIMPLE (int)
  sprintf(keyword, "SIMPLE");
  sprintf(comment, "File conforms to FITS standard");
  int_value = 1;
  fits_write_key_log(fptr, keyword, int_value, comment, &status);
  //BITPIX (long)
  sprintf(keyword, "BITPIX");
  sprintf(comment, "Bits per pixel"); 
  lng_value = -32;
  fits_write_key_lng(fptr, keyword, lng_value, comment, &status);
  //NAXIS (long)
  sprintf(keyword, "NAXIS");
  sprintf(comment, "Number of axes"); 
  lng_value = 2;
  fits_write_key_lng(fptr, keyword, lng_value, comment, &status);
  //NAXIS1 (long)
  sprintf(keyword, "NAXIS1");
  sprintf(comment, "Axis length"); 
  lng_value = naxis1;
  fits_write_key_lng(fptr, keyword, lng_value, comment, &status);
  //NAXIS2 (long)
  sprintf(keyword, "NAXIS2");
  sprintf(comment, "Axis length"); 
  lng_value = naxis2;
  fits_write_key_lng(fptr, keyword, lng_value, comment, &status);
  //CRPIX1 (float)
  sprintf(keyword, "CRPIX1");
  sprintf(comment, ""); 
  dbl_value = CRPIX1;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status);
  //CRPIX2 (float)
  sprintf(keyword, "CRPIX2");
  sprintf(comment, ""); 
  dbl_value = CRPIX2;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status);
  //CRVAL1 (float)
  sprintf(keyword, "CRVAL1");
  sprintf(comment, ""); 
  dbl_value = CRVAL1;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status);
  //CRVAL2 (float)
  sprintf(keyword, "CRVAL2");
  sprintf(comment, ""); 
  dbl_value = CRVAL2;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status);
  //TOTFLUX (float)
  sprintf(keyword, "TOTFLUX");
  sprintf(comment, ""); 
  dbl_value = totflux;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status);
  //CD1_1 (float)
  sprintf(keyword, "CD1_1");
  sprintf(comment, ""); 
  dbl_value = CD1_1;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status);
  //CD2_2 (float)
  sprintf(keyword, "CD2_2");
  sprintf(comment, ""); 
  dbl_value = CD2_2;
  fits_write_key_flt(fptr, keyword, dbl_value, decim, comment, &status); 
  //CTYPE1 (str)
  sprintf(keyword, "CTYPE1");
  sprintf(comment, ""); 
  fits_write_key_str(fptr, keyword, &CTYPE1, comment, &status); 
  //CTYPE2 (str)
  sprintf(keyword, "CTYPE2");
  sprintf(comment, ""); 
  fits_write_key_str(fptr, keyword, &CTYPE2, comment, &status); 
  //CONVKERN (str) - Convolution Kernel (Only used for templates)
  sprintf(keyword, "CONVKERN");
  sprintf(comment, ""); 
  fits_write_key_str(fptr, keyword, kern, comment, &status); 
  //FLUXNORM (str) - Normalization
  sprintf(keyword, "FLUXNORM");
  sprintf(comment, "Normalized flux: new total = 1.0");
  int_value = 1;
  fits_write_key_log(fptr, keyword, int_value, comment, &status);

  if(status) {
    fits_report_error(stderr, status);
    fprintf(stderr, "%s:%d:ERROR: can't correctly write FITS header\n",
	    progname, __LINE__);
    exit(EXIT_FAILURE);
  }
}

/* ------------------------------------------------------------------------------ */

fitsfile *chkfitsopen (char *filename, char type) {
  fitsfile *ptr;
  char keyword[FLEN_KEYWORD];
  char comment[FLEN_COMMENT];
  int status = 0;   /*  CFITSIO status value MUST be initialized to zero!  */
  int naxis;
  long naxes[2] = {1,1};
  double dblvalue;
  uchar CheckSize = 0;

  if(fits_open_file(&ptr, filename, READONLY, &status)) {
    fits_report_error(stderr, status);
    return(NULL);
  }
  /* Read dimensions */
  if(fits_get_img_dim(ptr, &naxis, &status)) {
    fits_report_error(stderr, status);
    return(NULL);
  }
  if (naxis > 2) {
    fprintf(stderr, "ERROR: %s has > 2 dimensions\n", filename);
    return(NULL);
  }
  /* Read size */
  if(fits_get_img_size(ptr, 2, naxes, &status)) {
    fits_report_error(stderr, status);
    return(NULL);  
  }

  switch (type) {
  case 'i': // Measure image
    // Set offset default values
    MeasXoff = 0.0;
    MeasYoff = 0.0;
    
    // Set measure image size
    MeasWidth = naxes[0];
    MeasHeight = naxes[1];
    
    // Search for offset keywords
    sprintf(keyword, "CPHXOFF");
    fits_read_key_dbl(ptr, keyword, &dblvalue, comment, &status);
    if (!status)
      MeasXoff = (long)round(dblvalue);
    
    sprintf(keyword, "CPHYOFF");
    fits_read_key_dbl(ptr, keyword, &dblvalue, comment, &status);
    if (!status)
      MeasYoff = (long)round(dblvalue);

    if ((MeasXoff != 0) || (MeasYoff != 0)) {
      // Set offset flag
      Offset++;
      // Compute the coordinates of the overlap box between the detection and the measure image
      MeasXmin = MAX(1, (1 + MeasXoff));
      MeasXmax = MIN(Width, (MeasWidth + MeasXoff));
      MeasYmin = MAX(1, (1 + MeasYoff));
      MeasYmax = MIN(Height, (MeasHeight + MeasYoff));
    }
    else {
      MeasXmin = 1;
      MeasXmax = MIN(Width, MeasWidth);
      MeasYmin = 1;
      MeasYmax = MIN(Height, MeasHeight);
    }
    // Compute the size of the overlap box between the detection and the measure image
    OverlapWidth = MeasXmax - MeasXmin + 1;
    OverlapHeight = MeasYmax - MeasYmin + 1;
    break;
    
  case 'r': // Detection image
    // Set image size
    Width = naxes[0];
    Height = naxes[1];
    break;
    
  case 'R': // RMS image
    // Check if RMS image has the same size of the measure image
    if (naxes[0] != MeasWidth) {
      fprintf(stderr, "ERROR: %s has width different from measure image\n", filename);
      return(NULL);
    }
    if (naxes[1] != MeasHeight) {
      fprintf(stderr, "ERROR: %s has height different from measure image\n", filename);
      return(NULL);
    }
    break;
    
  case 's': //Segmentation image
    CheckSize++;
    break;
    
  default:
    break;
  }
  
  if (CheckSize && (naxes[0] != Width)) {
    fprintf(stderr, "ERROR: %s has width different from detection image\n", filename);
    return(NULL);
  }
  if (CheckSize && (naxes[1] != Height)) {
    fprintf(stderr, "ERROR: %s has height different from detection image\n", filename);
    return(NULL);
  }
  
  return(ptr);
}

/* -------------------------------------------------------------------------------*/

void TestFitsExistence(char *FitsName) {
  FILE *ptr;
  FILE *stream = stdin;
  char buffer[BUFFERSIZE];
  int attempts = 0;

  if((ptr = fopen(FitsName, "r")) != NULL) {
    printf("WARNING: file \'%s\' already exists!\n", FitsName); 
    while(++attempts <= 3) {
      printf("\33[1MDo you want to overwrite it? [y|n] ");
      if(fgets(buffer, BUFFERSIZE, stream) == NULL) {
	fprintf(stderr, "%s:%d:ERROR: empty buffer in fgets() call\n",
		progname, __LINE__);
	exit(EXIT_FAILURE);
      }
      if(strlen(buffer) != 2 || (*buffer != 'y' && *buffer != 'n')) { 
	printf("\33[1A");
	continue;
      }
      else if (*buffer == 'y') {
	printf("\33[1M\33[1A");
	printf("\33[1M\33[1A");
	break;
      }
      else
	exit(EXIT_SUCCESS);
    }
    if(attempts > 3) {
      printf("\n");
      printf("I'll take it as no!\n");
      exit(EXIT_SUCCESS);
    }
    fclose(ptr);
  }
}

/* -------------------------------------------------------------------------------*/

FILE *chkfopen(const char * filename, const char * input_mode) {
  FILE *ptr;

  if ((ptr = fopen(filename, input_mode)) == NULL) {
    fprintf(stderr, "Error: can't open file \"%s\" in mode : %s\n", 
	    filename, input_mode);
    return(NULL);
  }
  else 
    return (ptr);
}

/* ------------------------------------------------------------------------------ */

int ReadCatalog (char *CatalogName) {

  /*
    Reads catalog in SExtractor format and stores values
  */

  FILE *CatalogFile; // C standard type. The associated variable must be a pointer.
  char buffer[BUFFERSIZE];
  int i = 0; int j;
  long id;
  double x_image;
  double y_image;
  int xmin_image;
  int xmax_image;
  int ymin_image;
  int ymax_image;
  double detbckg;
  double measbckg;
  catalogstruct *pCatalog;

  // opens the file CatalogName and assign it to the variable CatalogFile
  if ((CatalogFile = chkfopen(CatalogName, "r")) == NULL) {  // chkfopen defined below
    return(RETURN_FAILURE);
  }

  /* Allocate memory for storing catalog sources */
  QCALLOC(Catalog, catalogstruct, MAXCATALOG); 
  pCatalog = Catalog;  

  while (fgets(buffer, BUFFERSIZE, CatalogFile) != NULL  // reads the catalog
	 && !feof(CatalogFile)) 
    {
      // Skip line if it begins with "#" or the input columns are not 9 or id is < 0 
      if ((buffer - '#') == 0 || 
	  (sscanf(buffer, "%ld\t%lf\t%lf\t%d\t%d\t%d\t%d\t%lf\t%lf",  // Input Sextr File
		  &id, &x_image, &y_image, &xmin_image, &ymin_image, 
		  &xmax_image, &ymax_image, &detbckg, &measbckg)) != 9 || // Order changed! It is correct now
	  (id < 1)) {
	continue;}
    
      if(++i > MAXCATALOG) 
	{ // i is accreted by one (BEFORE evaluating). if there are too many sources: error
	  fprintf(stderr, "WARNING: reached maximum catalog sources number %d\n", MAXCATALOG);
	  i = MAXCATALOG;
	  break;
	}

      // now assign read values to pointer pCatalog which points to current value of Catalog object
      pCatalog->id = id;
      pCatalog->x_image = x_image;
      pCatalog->y_image = y_image;
      pCatalog->xmin_image = xmin_image;
      pCatalog->xmax_image = xmax_image;
      pCatalog->ymin_image = ymin_image;
      pCatalog->ymax_image = ymax_image;
      pCatalog->detbckg = detbckg;
      pCatalog->measbckg = measbckg;

      pCatalog++;
    }
  
  if((NumInSources = i) == 0) { // NumInSources = i
    fprintf(stderr, "ERROR: catalog %s empty or in invalid format\n", CatalogName);
    return(RETURN_FAILURE);
  }
  NumSources = NumInSources;

  QREALLOC(Catalog, catalogstruct, NumSources);
  fclose (CatalogFile);
  return (RETURN_SUCCESS);
}

/* ------------------------------------------------------------------------------ */

int ReadFilter(char *FilterName) {
  FILE *FilterFile = NULL;
  char str[BIG_BUFFERSIZE];
  char *sstr;
  int i, j, n;
  double var, sum, pix;
  char* dstdiro;

  dstdiro="./";
  if ((FilterFile = chkfopen(FilterName, "r")) == NULL) {
    return(RETURN_FAILURE);
  }

  /* Allocate memory for storing mask elements */

  QMALLOC(pixels_kern, double, MAXMASK);

  for (i = 0, n = 0; fgets(str,BIG_BUFFERSIZE,FilterFile);) {
    j = 0;
    sstr = strtok(str, " \t\n");
    if (sstr && sstr[0]!=(char)'#') {
      do {
	j++;
	pixels_kern[i++] = strtod(sstr, NULL);
	if (i > MAXMASK) {
	  fprintf(stderr, "ERROR: Convolution mask too large in %s\n", FilterName);
	  return(RETURN_FAILURE);
	}
      }	while (sstr = strtok(NULL, " \t\n"));
    }
    if (j > n) {
      n = j;
    }
  }
  
  fclose(FilterFile);

  if ((Width = n) < 1) {
    fprintf(stderr, "ERROR: Unappropriate convolution mask width in %s\n", FilterName);
    return(RETURN_FAILURE);
  }
  if (i%n) {
    fprintf(stderr, "ERROR: Unappropriate convolution mask line in %s\n", FilterName);
    return(RETURN_FAILURE);
  }

  QREALLOC(pixels_kern, double, i);
  
  Height = i/n;
  var = 0.0;
  for (j = 0, sum = 0.0; j < i; j++) {
    sum += fabs(pix = pixels_kern[j]);
    var += pix*pix;
  }

  return(RETURN_SUCCESS);
}

/* ------------------------------------------------------------------------------ */

float CutOne (int id, int lx, int ly, int xr, int yr, double bckg, double *pixels, int *pixels_seg, int wid, double *cuto) {

  /*
    Produces the hires cutout for the object "Source". Also checks for saturation
  */

  int i,j,q,t,satur=0;
  float s=0.0,maxcuto=0.0;
  for (i=0;i<yr;i++)
    for (j=0;j<xr;j++)
      {
	t=(ly+i)*wid+(lx+j);
	if (pixels_seg[t]==id) maxcuto=MAX(maxcuto,pixels[t]-bckg);
      }

  for (i=0;i<yr;i++)
    for (j=0;j<xr;j++)
      {
	q=i*xr+j;
	t=(ly+i)*wid+(lx+j);
	if (pixels_seg[t]==id)
	  {
	    cuto[q]=pixels[t]-bckg;
	    s+=cuto[q]; // total flux within segmentation
	    if (cuto[q]>0.99*maxcuto) satur+=1;
	  }
	  else
	{cuto[q]=0.e0;}
      }

  // normalize to 1
  for (i=0;i<yr*xr;i++) cuto[i]=cuto[i]/s;

  if (satur>5) {
    s=-s;
  }
  return(s);
}


/* =============================================================================== */
