
/*
Copyright © 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdio.h>
#include "fitsio.h"
#include <tphot.h>
#include "globals.h"
#include <fftw3.h>
#include "unistd.h"

/*
  Produces templates from xy catalog and PSF 
*/

#define MAX_BCKG_STEP 16
#define DILATION_STEP_FACTOR 1.0
//#define VERBOSE
#define SAVE_TH
//#define SAVE_UNCUT_TH
//#define SAVE_HR_TH

double *pixels_kern, *pixels, *pixels_kern_0;
long fpixel[2] = {1,1};
char *dstdir, *indir, *kernfile;
fitsfile *fptr1, *fptr2;
int hW,hH,nc,nr,TOTFLUX, SubBckg; 
long Xoff, Yoff;
double XoffLR, YoffLR;
long xmin,xmax,ymin,ymax;
int count=0,id,xc,yc,Area;
long LRW=0,LRH=0;
int multi, FFT=1, txt=0, pixratio;
float CRPIX1ref=0.0,CRPIX2ref=0.0;
float CRPIX1=0.0,CRPIX2=0.0;
double CRVAL1=0.e0, CRVAL2=0.e0;
double CD1_1=0.0; double CD2_2=0.0;
char *CTYPE1, *CTYPE2;
double TOL = 1.e-9;
long Width, Height, WidthHiRes;
float measfl=0.0;

static int MakeOne (catalogstruct *);
static double* Normalize (double *, int, int);
extern void WriteThumbnail (char *dstdir, char *prefix, int id, int ThWidth, 
			    int ThHeight, double totflux, double *image, char *kern);
extern void WriteHeader(fitsfile *fptr, long naxis1, long naxis2);
extern FILE *chkfopen(const char * filename, const char * input_mode);
extern fitsfile *chkfitsopen (char *filename, char type);

int main(int argc, char *argv[])
{
  int status = 0;   /* CFITSIO status value MUST be initialized to zero! */
  int bitpix, naxis, i, ii, jj, s, l, j;
  double x,y,detbckg,measbckg,absx,absy,sumi,val1,val2,val,one=1.0;
  double *mod, *mod0, *mod1;
  char buffer[BUFFERSIZE];
  int NumProcessedSources=0, Kept=0, eliminate, startcount,xint,yint,temp;
  int nc0,nr0,x_in,y_in,Area1,flag;
  catalogstruct *pCatalog;
  char *cutcat, *multicat; //, *kfileind;
  char *logbasename; 
  char logname1[BUFFERSIZE],logname2[BUFFERSIZE], kfileind[BUFFERSIZE];
  FILE *log,*CatalogFile; 
  char *xcorner, *ycorner, *xref, *yref, *wid, 
    *hei, *cd11, *cd22, *ctyp1, *ctyp2;
  char comments[BUFFERSIZE];
  int mkext;
  
  if (argc != 7 || argv[1]=="h") {
    printf("Usage: tphot_psf TemplateDir TemplateCat KernelFile LRI PosCat mkext\n");
    exit(1);  
  }
  
  // Header keywords
  wid="NAXIS1"; hei="NAXIS2";
  xcorner="CRPIX1"; ycorner="CRPIX2";
  xref="CRVAL1"; yref="CRVAL2";
  cd11="CD1_1"; cd22="CD2_2";
  ctyp1="CTYPE1"; ctyp2="CTYPE2";

  dstdir=argv[1]; // models output directory
  logbasename=argv[2]; // name of output catalog
  kernfile=argv[3];
  mkext=atoi(argv[6]);

  /* Open and check LRI to read in some header values and pixel values */
  if ((fptr1=chkfitsopen(argv[4],'r'))==NULL) return(RETURN_FAILURE);
  fits_read_key(fptr1,TINT,wid,&LRW,comments,&status);
  fits_read_key(fptr1,TINT,hei,&LRH,comments,&status);
  fits_read_key(fptr1,TFLOAT,xcorner,&CRPIX1ref,comments,&status);
  fits_read_key(fptr1,TFLOAT,ycorner,&CRPIX2ref,comments,&status);
  fits_read_key(fptr1,TDOUBLE,xref,&CRVAL1,comments,&status);
  fits_read_key(fptr1,TDOUBLE,yref,&CRVAL2,comments,&status);
  fits_read_key(fptr1,TDOUBLE,cd11,&CD1_1,comments,&status);
  fits_read_key(fptr1,TDOUBLE,cd22,&CD2_2,comments,&status);
  fits_read_key(fptr1,TSTRING,ctyp1,&CTYPE1,comments,&status);
  fits_read_key(fptr1,TSTRING,ctyp2,&CTYPE2,comments,&status);
  pixels = (double *) malloc(LRW*LRH * sizeof(double));
  fits_read_pix(fptr1, TDOUBLE, fpixel, LRW*LRH, NULL,
		pixels, NULL, &status); 
  fits_close_file(fptr1, &status);

  /* Open and check PSF images */
  // Reads FITS file PSF
  if ((fptr2=chkfitsopen(kernfile,'r'))==NULL) return(RETURN_FAILURE);
  /* Reads PSF matrix. */
  pixels_kern_0 = (double *) malloc(Width*Height * sizeof(double));
  fits_read_pix(fptr2, TDOUBLE, fpixel, Width*Height, NULL,
		pixels_kern_0, NULL, &status); 
  fits_close_file(fptr2, &status);
#ifdef VERBOSE
  printf("PSF read\n");
#endif
  Area = Width*Height;
  pixels_kern = (double *) malloc(Area * sizeof(double));
  for (i=0;i<Area;i++) pixels_kern[i]=pixels_kern_0[i];

  if (Width%2==0 || Height%2==0){
    printf("Psf image should have odd axis dimensions. Aborting"); exit(1);}
  else{
    hW=(Width-1)/2;
    hH=(Height-1)/2;
  }

  printf("Producing templates...\n");

  NumProcessedSources = 0;
  int ccc=0;

  snprintf(logname1,BUFFERSIZE,"%s",logbasename);
  if((log = chkfopen(logname1, "w")) == NULL)
    return(RETURN_FAILURE);
  
  fprintf(log, "#id x y xmin ymin xmax ymax cutout_meas_flux LRI_centr_flux\n");
  
  /* Read positions catalog */
  cutcat=argv[5];
  CatalogFile = chkfopen(cutcat, "r");
  while (fgets(buffer, BUFFERSIZE, CatalogFile) != NULL && !feof(CatalogFile))  // reads the catalog
    {
      // Skip line if it begins with "#" or the input columns are not 3 or id is < 0 
      if ((buffer - '#') == 0 || 
	  (sscanf(buffer, "%d\t%lf\t%lf", &id, &x, &y)) != 3 ||
	  (id < 1)) {
	continue;}
      
      ccc+=1;

#ifdef VERBOSE
      printf("> Writing thumbnail: %7d (ID: %d)\n", ccc, id);
#else
      printf("\33[1M> Writing thumbnail: %7d (ID: %d)\n\33[1A", ccc, id);
#endif

      if (mkext)
	{
	  sprintf(kfileind, "PSFS/%d_PSF.fits", id);
	  if( access(kfileind, F_OK) != -1) {
	    fptr2=chkfitsopen(kfileind,'r');
	    fits_read_pix(fptr2, TDOUBLE, fpixel, Area, NULL,
			  pixels_kern, NULL, &status); 
	    fits_close_file(fptr2, &status);
	  }
	}

      NumProcessedSources++;
      
      xc=floor(x+0.5e0);
      yc=floor(y+0.5e0);
      xmin=xc-hW;
      xmax=xmin+Width-1;
      ymin=yc-hH;
      ymax=ymin+Height-1;
      detbckg=1.0;
      measbckg=1.0;

      int AreaEnl;
      nc=Width+2; nr=Height+2; 
      AreaEnl=nc*nr; // Because of shifting, a larger template might be necessary    
      xmin-=1; ymin-=1;
      xmax+=1; ymax+=1;

      // Check if it is completely outside LRI
	  
      if (xmax<1 || xmin>LRW || ymax<1 || ymin>LRH)
	{
#ifdef VERBOSE
	  printf("Model %d is outside LRI frame!\n", id);
#endif
	}
      else
	{
	  mod = (double *) malloc(AreaEnl * sizeof(double));
	  for (ii=0;ii<AreaEnl;ii++) mod[ii]=0.0;

	  // Shift it
	  double Xx,Yy;
	  Xx = x-(double)((int)(x+0.5));
	  Yy = y-(double)((int)(y+0.5));
	  absx = fabs(Xx);
	  absy = fabs(Yy);
	  xint=0; yint=0;
	  if(absx!=0.0) xint = -(round)(Xx/absx);
	  if(absy!=0.0) yint = -(round)(Yy/absy);
	  sumi=0.0;
	  for (i=MAX(-yint,0);i<Height-MAX(0,yint);i++){
	    for (j=MAX(-xint,0);j<Width-MAX(0,xint);j++){
		  
	      // Bilinear interpolation
	      val1 = (1.0-absx)*pixels_kern[i*Width+j]+absx*pixels_kern[i*Width+(j+xint)];
	      val2 = (1.0-absx)*pixels_kern[(i+yint)*Width+j]+absx*pixels_kern[(i+yint)*Width+(j+xint)];
	      val = (1.0-absy)*val1+absy*val2;
	      
	      temp=i*Width+j;
	      temp=(i+1)*(Width+2)+(j+1);
	      if (temp>=0 && temp<AreaEnl) mod[temp]=val;
	      sumi+=val;

	    }
	  }

	  for (i=0;i<AreaEnl;i++) mod[i]=mod[i]/sumi;	    

	  /*If the model overlaps LRI limits, cut it */
	  if (xmax>LRW || xmin<1 || ymax>LRH || ymin<1)
	    {
	      xmin = MAX( xmin , 1 );
	      xmax = MIN( xmax , LRW );
	      ymin = MAX( ymin , 1 );
	      ymax = MIN( ymax , LRH );
	      count+=1;
	      CRPIX1=CRPIX1ref-xmin+1;
	      CRPIX2=CRPIX2ref-ymin+1;
	      nc0=nc;nr0=nr;
	      x_in=0;y_in=0;
	      if(xmin==1) x_in=nc-(xmax-xmin+1);
	      if(ymin==1) y_in=nr-(ymax-ymin+1);
	      nc=xmax-xmin+1;
	      nr=ymax-ymin+1;
	      Area1=nc*nr;
	      
	      mod1 = (double *) malloc(Area1 * sizeof(double));
	      for (ii=0;ii<Area1;ii++) mod1[ii]=0.0;
	      
	      for (ii=0;ii<nr;ii++){ // Y
		for (jj=0;jj<nc;jj++){ // X
		  mod1[ii*nc+jj]=mod[(y_in+ii)*nc0+(x_in+jj)];}}	
	      
	      mod1=Normalize(mod1,Area1,id);
	      if (mod1[0]>-1.e11) { 
		flag=0;
#ifdef SAVE_TH
		WriteThumbnail(dstdir, "mod", id, nc, nr, TOTFLUX, mod1, kernfile);
#endif
	      }
	      free(mod1);
	      mod1=NULL;
	      
	    }
	  else
	    {	  
	      if (nc!=xmax-xmin+1 || nr!=ymax-ymin+1) {
		printf("HORROR>>> %d: %ld %ld %ld %ld %ld %ld %ld %ld %d %d\n",id,xmin,xmax,ymin,ymax,Xoff,Yoff,LRW,LRH,nc,nr);
		exit(1);
	      }
	      Kept+=1;
	      CRPIX1=CRPIX1ref-xmin+1;
	      CRPIX2=CRPIX2ref-ymin+1;
	      mod=Normalize(mod,AreaEnl,id);
	      if (mod[0]>-1.e11) { 
		flag=0;
#ifdef SAVE_TH
		WriteThumbnail(dstdir, "mod", id, nc, nr, TOTFLUX, mod, kernfile); 
#endif
	      }
	    }
	  
	  free(mod);
	  mod=NULL;
	  
#ifdef VERBOSE
	  printf("Source %d produced at XY %f %f\n",NumProcessedSources,x,y);
#endif
	  val=0.0;
	  if ((x+0.5<=LRW)&&(y+0.5<=LRH)&&(x+0.5>0)&&(y+0.5>0))
	    val = pixels[LRW*(int)(y+0.5)+(int)(x+0.5)];
	  fprintf(log, "%d %f %f %ld %ld %ld %ld %e %e\n",
		  id,x,y,
		  MAX(xmin,1),MAX(ymin,1),MIN(xmax,LRW),MIN(ymax,LRH),
		  one, val);
#ifdef VERBOSE
	  printf("Source %d written on catalog\n",NumProcessedSources);
#endif
	  
	}
    }
  
  free(pixels_kern);
  free(pixels);
  
  fclose(log); 
  
  printf("\n");
  printf("Cut thumbnails: %d / %d\n",count,NumProcessedSources);
  printf("Kept thumbnails: %d / %d\n",Kept,NumProcessedSources);  
  
  if (status) fits_report_error(stderr, status); /* print any error message */
  return(status);
  
}


/* ------------------------------------------------------------------------------ */

double* Normalize (double *in, int dim, int idu)
/* Masks out bad pixels and normalize to 1 */
{
  int ii;
  double sum=0.e0;

  for (ii=0;ii<dim;ii++){
    //if ((in[ii])<TOL) in[ii]=0.e0; // masking here?
    sum+=in[ii];
  }
  if (sum>TOL) 
    {
      for (ii=0;ii<dim;ii++){
	in[ii]=in[ii]/(sum);
	//if ((in[ii])<TOL) in[ii]=0.e0;
      }
    }
  else
    {
      in[0]=-1.e12;
#ifdef VERBOSE
      printf("Bad normalization! ID= %d SUM= %f\n",idu, sum);
#endif
    }
  return(in);
}
