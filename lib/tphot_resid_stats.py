#!/usr/bin/python

#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import scipy.stats as st
import os,sys
from astropy.io import fits,ascii


def do_resid_stats(catf,pref,catt,resid):    

    thres=0.5

    resf=fits.open(resid)
    res=resf[0].data

    o=open("residual_stats.cat",'w')
    o.write("#id x y xmin ymin xmax ymax f errf mean median std kurt mean_in median_in std_in kurt_in mean_out median_out std_out kurt_out\n")

    cf=ascii.read(catf)
    ct=ascii.read(catt)

    ids=ct['id'].tolist()

    print "Working..."

    for obj in cf:

        id=obj['ObjectID']
        #print "Working on obj ",id
        x=obj['X']
        y=obj['Y']
        f=obj['FitQty']
        errf=obj['FitQuErr']
        idx=ids.index(id)
        xmin=ct['xmin'][idx]
        ymin=ct['ymin'][idx]
        xmax=ct['xmax'][idx]
        ymax=ct['ymax'][idx]

        b=res[ymin:ymax+1,xmin:xmax+1]

        M1=np.median(b)
        m1=np.mean(b)
        s1=np.std(b)
        k1=st.kurtosis(b,axis=None)

        tf=fits.open(pref+str(id)+'.fits')
        t=tf[0].data
        tthres=thres*t.max()
        bthres=tthres*f
        tf.close()

        M2=np.median(b[b>bthres])
        m2=np.mean(b[b>bthres])
        s2=np.std(b[b>bthres])
        k2=st.kurtosis(b[b>bthres],axis=None)

        M3=np.median(b[b<=bthres])
        m3=np.mean(b[b<=bthres])
        s3=np.std(b[b<=bthres])
        k3=st.kurtosis(b[b<=bthres],axis=None)

        o.write("%d %f %f %d %d %d %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n"%(id,x,y,xmin,ymin,xmax,ymax,f,errf,m1,M1,s1,k1,m2,M2,s2,k2,m3,M3,s3,k3))

    resf.close()
    o.close()

if __name__ == '__main__':
    
    """
    Computes statistics on residual image
    For each source, computes mean, std and kurtosis
    Statistics are computed on the whole template area, on a central
    region defined by the parameter thres, and on the outer region
    ON KURTOSIS:
    A gaussian *with correct normalization* (i.e., not normalized to 1)
    has kurtosis = 0 if fisher=False. Positive values are peaked,
    negative values are shallow. 
    """

    if np.size(sys.argv)!=5:
        print "USAGE: python tphot_resid_stats.py fluxcat templdir templcat residimg"
        sys.exit()

        catf=sys.argv[1]
        pref=sys.argv[2]+'/mod-'
        catt=sys.argv[3]
        resid=sys.argv[4]

        do_resid_stats(catf,pref,catt,resid)
