from __future__ import division
import box

class WCS:
    """ A simple class to facilitate manipulation of the WCS as
        an object distinct from the entire FITS header. 
    """

    def __init__(self,h):
	#assert h['naxis'] == 2, "Sorry, only 2D supported at this time"
	#Reference point
	self.crpix1 = h.get('crpix1',0.0)
	self.crpix2 = h.get('crpix2',0.0)
	self.crval1 = h.get('crval1',0.0)
	self.crval2 = h.get('crval2',0.0)
	#Units
	self.cunit1 = h.get('cunit1')
	self.cunit2 = h.get('cunit2')
	self.ctype1 = h.get('ctype1')
	self.ctype2 = h.get('ctype2')

	#scale
	self.cdelt1 = h.get('cdelt1',0.0)
	self.cdelt2 = h.get('cdelt2',0.0)
	self.crota1 = h.get('crota1',0.0)
	self.crota2 = h.get('crota2',0.0)

	#cd matrix
	self.cd1_1 = h.get('cd1_1',0.0)
	self.cd1_2 = h.get('cd1_2',0.0)
	self.cd2_1 = h.get('cd2_1',0.0)
	self.cd2_2 = h.get('cd2_2',0.0)
	#To be added: other valid wcs keywords such as pc matrix

##     def __repr__(self):
## 	import util
## 	return util.show(self,noprint=1)

    def __str__(self):
	longstring="\n "
	for k in self.__dict__:
	    if not k.startswith('_'): #Hide the hidden attributes
		longstring+= k +" : "+ str(self.__dict__[k])+ "\n"
	return longstring


    def set_crpix(self,x,y):
	self.crpix1=x
	self.crpix2=y

    def xy2xy_box(self,thatbox,other,nocorner=None):
	""" Transforms the xy coords of the box associated with the "other"
	wcs into the frame specified by this wcs. """
	if nocorner is not None:
	    halfpix = 0.0
	else:
	    #Add & subtract half a pixel in order to get the pixel coordsof the
	    #corner of the box.
	    halfpix = 0.499
	lx,ly = self.xy2xy(thatbox.lx-halfpix,thatbox.ly-halfpix,other)
	ux,uy = self.xy2xy(thatbox.ux+halfpix,thatbox.uy+halfpix,other)
	return box.Box(x1=lx,y1=ly,x2=ux,y2=uy)

    def xy2xy(self,x,y,other):
	""" Transform the xy coordinates associated with the "other"
	wcs into the frame specified by this wcs """
	assert (self.ctype1, self.ctype2 == other.ctype1, other.ctype2), \
	       "Sorry: Type of transformations must be identical"
	assert (self.crval1, self.crval2 == other.crval1, other.crval2), \
	       "Sorry: Sky reference point must be identical"
	#assert (self.cd1_2 == self.cd2_1 == other.cd1_2 == other.cd2_1 == 0), \
	#       "Sorry, support for rotation is not yet available"
        if not (self.cd1_2 == self.cd2_1 == other.cd1_2 == other.cd2_1 == 0):
            print "\rWARNING - support for rotation is not yet available",

	sx = other.cd1_1/self.cd1_1 #other.cdelt1/self.cdelt1  #
	sy = other.cd2_2/self.cd2_2 #other.cdelt2/self.cdelt2  #
	x1 = sx*(x - other.crpix1) + self.crpix1
	y1 = sy*(y - other.crpix2) + self.crpix2
	return x1,y1

    def wcspos(self,localbox):
	""" Applies this wcs to the xy positions defined by the box to
	produce a box in the wcs reference frame."""
	import copy
	newbox = copy.deepcopy(localbox)
	newbox.Move(-self.crpix1, -self.crpix2)
	newbox.lx=box.pixel_index(newbox.lx)
	newbox.ly=box.pixel_index(newbox.ly)
	newbox.ux=box.pixel_index(newbox.ux)
	newbox.uy=box.pixel_index(newbox.uy)
	return newbox

    def localpos(self, wcsbox):
	""" Applies this wcs in reverse, to the xy positions defined
	by the box in the wcs reference frame, to produce a box in the
	"local" reference frame."""
	import copy
	newbox = copy.deepcopy(wcsbox)
	newbox.Move(self.crpix1, self.crpix2)
	newbox.lx=box.pixel_index(newbox.lx,'upper')
	newbox.ly=box.pixel_index(newbox.ly,'upper')
	newbox.ux=box.pixel_index(newbox.ux,'upper')
	newbox.uy=box.pixel_index(newbox.uy,'upper')

	return newbox
	


    ## Issues:
    ##    How distinguish between "missing value" and legal value?
    ## Maybe 0.0 is not a good default value.
    ##    Am I just reinventing the wheel here? What about the 
    ## wcstools library, or etc?
    ##    Should these things be data members? or should self be
    ## a dictionary? Pros/cons?


    def xyshift(self,x,y):
	""" Shift the CRPIX1/2 values as specified. """
	self.crpix1 = self.crpix1 + x
	self.crpix2 = self.crpix2 + y

    def skyshift(self,ra,dec,form=None):
	""" Shift the CRVAL1/2 values as specified. If form is
	not specified, arcseconds are assumed."""
	assert self.ctype1 == 'RA---TAN',"Sorry, only RA/DEC WCS is supported"
	pass


    def rescale(self, factor):
	""" Change all the scale-related keywords by the specified factor. 
	The sign of the factor determines whether to multiply (positive)
	or divide (negative). (is this a good idea?)"""
	if (factor < 0.0): factor = 1/float(abs(factor))
	#cdelts. Test for presence/nonzero/non-None first? for all these?
	self.cdelt1 = self.cdelt1*factor
	self.cdelt2 = self.cdelt2*factor
	# CD matrix
	self.cd1_1 *= factor
	self.cd1_2 *= factor
	self.cd2_1 *= factor
	self.cd2_2 *= factor
	# ref points
	self.crpix1 /= factor
	self.crpix2 /= factor
	
    def consistify(self):
	#This needs a better name
	""" Following the priority rules in the WCS papers, make
	all the WCS keywords consistent with each other. """
	pass

    def fillcd(self):
	#This isn't very robust.
	assert(self.cd1_1 == self.cd1_2 == self.cd2_1 == self.cd2_2 == 0.0, \
	       "CD matrix already filled: used consistify instead.")
	assert(self.crota1 == self.crota2 == 0.0, "Rotation not supported")
        if self.cd1_1 != 0.0 or self.cd2_2 != 0.0:
            #print "CD matrix already filled."
            pass
        else:
            self.cd1_1 = self.cdelt1
            self.cd2_2 = self.cdelt2

    def upgrade(self):
	""" Following the standards in the WCS papers, remove
	all keywords that have been superceded by the new standard.
	should this take a header? """
	pass

	
    def insert(self,h,addkeys=False):
	""" Insert the WCS described by this instance into the
	FITS header h. Should be more or less the inverse of __init__."""
	all=dir(self)
	for t in all:
	    if t.startswith('_') or callable(getattr(self,t)):
		pass
	    else:
		if h.has_key(t):
                    try:
                        h[t] = getattr(self,t)
                    except ValueError, e:
                        pass
                        #print e
                        #print "omitting h[%s] for this object"%t
                else:
                    if addkeys:
                        h.update(t,getattr(self,t))

	

    def rd2xy(self,ra,dec):
	pass

    def xy2rd(self,x,y):
	pass

    def aligned(self,other):
	""" Boolean function to determine whether the pixel grids of
	two WCSs are aligned with each other. This requires that the
	pixels be the same size (equal scales), and also that the pixel
	corners line up with each other properly (fractional parts of
	crpix's must be the same). """
	scale = self.cd1_1/other.cd1_1 * self.cd2_2/other.cd2_2
	xalign = self.crpix1%1 == other.crpix1%1
	yalign = self.crpix2%1 == other.crpix2%1
	return (scale and xalign and yalign)

    def set(self,scale,crval,crpix,
            ctype=('RA---TAN','DEC--TAN'),
            cunit=('deg     ','deg     '),
             ):
        """ For use when creating one from scratch """

        self.ctype1,self.ctype2=ctype
        self.cunit1,self.cunit2=cunit
        self.crval1,self.crval2=crval
        self.crpix1,self.crpix2=crpix

        self.cd2_2 = scale/3600.0
        self.cd1_1 = -1*self.cd2_2
        self.cd1_2 = self.cd2_1 = 0

        self.cdelt1=self.cd1_1
        self.cdelt2=self.cd2_2
        
