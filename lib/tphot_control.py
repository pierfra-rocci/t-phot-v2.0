
"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

import os,sys

#---------------------------------------------------

def do_cutouts(hirescat,
               hiresfile,
               hiresseg,
               cutoutdir,
               cutoutcat,
               kernelfile,
               scale_factor,
               loresfile,
               kerntxt,
               subbckg,
               normalize=True,
               culling=True,
               nproc=1):
    import cutout

    subbckg=int(subbckg)

    info=cutout.cut_them(hirescat,
                         hiresfile,
                         hiresseg,
                         cutoutdir,
                         cutoutcat,
                         culling,
                         kernelfile,
                         scale_factor,
                         loresfile,
                         kerntxt,
                         subbckg,
                         normalize=normalize,
                         nproc=nproc)
    
    return info

#---------------------------------------------------

def do_convolve(d,
                flagfile,
                maxflag,
                shiftfile,
                nproc):
    import convolve

    if os.path.exists(d['cutoutcat']):
        if os.path.exists(d['cutoutcat']+"_CULLED"):
            cutcat = d['cutoutcat']+"_CULLED"
        else:
            cutcat = d['cutoutcat']
    else:
        if os.path.exists(d['hirescat']+"_CULLED"):
            cutcat = d['hirescat']+"_CULLED"
        else:
            cutcat = d['hirescat']
        print "WARNING: No cutouts found; will convolve sources on the fly from Hi Res image"

    if d.has_key('hiresseg'):
        if os.path.exists(d['hiresseg']):
            segim = d['hiresseg']
        else:
            segim = None
            print "WARNING: Input segmentation image does not exist; will abort if needed"
    else:
        segim = None
        print "WARNING: No segmentation image given; will abort if needed"

    mkext=0
    if d.has_key('mkext'):
        if d['mkext']:
            mkext=1

    dzonesize=0
    if d.has_key('dzonesize'):
        dzonesize=d['dzonesize'] 

    if d['multikernels']:
        
        convolve.smooth_multikern(gridfile=d['kernellookup'],
                                  indir=d['cutoutdir'],
                                  outdir=d['templatedir'],
                                  templcat=d['templatecat'],
                                  cutcat=cutcat,
                                  loresfile=d['loresfile'],
                                  HiRes=d['hiresfile'],
                                  seg=segim,
                                  scale_factor=d['relscale'],
                                  flagfile=None,
                                  maxflag=d['maxflag'],
                                  FFT=d['fftconv'],
                                  kerntxt=d['kerntxt'],
                                  subbckg=int(d['subbckg']),
                                  mkext=mkext,
                                  kernel=d['kernelfile'],
                                  shiftfile=shiftfile,
                                  dzonesize=dzonesize)
    else:
        
        convolve.smooth(d['cutoutdir'],
                        cutcat,
                        d['loresfile'],
                        d['hiresfile'],
                        segim,
                        d['kernelfile'],
                        d['relscale'],
                        d['templatedir'],
                        d['templatecat'],
                        d['culling'],
                        d['fftconv'],
                        d['kerntxt'],
                        int(d['subbckg']),
                        mkext,
                        dzonesize,
                        shiftfile,
                        flagfile=flagfile,
                        maxflag=d['maxflag'])
        
#---------------------------------------------------
        
# Cull is now inside convolve. Transform is no more used.
# All fitting stuff is done in fit.py

def do_fit(d):
    import fit
    
    fit.minimize(d)

#---------------------------------------------------
def do_diags(tphotcat,
             modelfile,
             loresfile,
             templatedir,
             templatecat,
             tphotcovar,
             exclfile,
             residstats,
             writecovar,
             inputcats):
    
    import diags

    diags.main(tphotcat,
               modelfile,
               loresfile,
               templatedir,
               templatecat,
               tphotcovar,
               exclfile,
               residstats,
               writecovar,
               inputcats)

#..............................................................

def do_dance(d):

    import dance

    mkext=0
    if d.has_key('mkext'):
        if d['mkext']:
            mkext=1
            kern=None
        else:
            if d.has_key('kernelfile'):
                kern=d['kernelfile']
            else:
                print "No kernel selected. Aborting"
                sys.exit()
    else:
        if d.has_key('kernelfile'):
            kern=d['kernelfile']
        else:
            print "No kernel selected. Aborting"
            sys.exit()

    if d.has_key('fit_loc_bkgd'):
        if d['fit_loc_bkgd']:
            tcat=d['templatedir']+'/_templates_only.cat'
        else:
            tcat=d['templatecat']

    if d.has_key('nneighinterp'):
        nneighinterp=d['nneighinterp']
    else:
        nneighinterp=100

    fitlog,junk=d['fitpars'].split('.',1)
    fitlog=fitlog+'_1.log'

    dance.main(
        tcat,
        d['loresfile'],
        d['modelfile'],
        kern,
        d['dzonesize'],
        d['maxshift'],
        d['relscale'],
        d['kernellookup'],
        d['ddiagfile'],
        d['dlogfile'],
        d['kerntxt'],
        d['dancefft'],
        mkext,
        nneighinterp,
        fitlog)


#..............................................................

def do_plotdance(diagfile):

    import plotdance

    plotdance.main(diagfile)

#..............................................................

def do_archive(d):
    
    import archive

    archive.arch(d)

#..............................................................

def do_positions(d):

    import positions

    outcat=d['templatecat']
    merge=False
    if d.has_key('usereal'):
        if d['usereal']:
            if d['hirescat'] != "None":
                merge = True
    if d.has_key('usemodels'):
        if d['usemodels']:
            if d['modelscat'] != "None":
                merge = True
    if merge:
        outcat=d['templatecat']+'_psf'
    mkext=0
    if d.has_key('mkext'):
        if (d['mkext']):
            mkext=1

    shiftfile=None
    if d['multikernels']:
        shiftfile=d['ddiagfile']

    positions.create_cat(
        d['poscat'],
        d['psffile'],
        d['templatedir'],
        outcat,
        d['loresfile'],
        merge,
        shiftfile,
        d['relscale'],
        mkext
    )

#..............................................................

def do_models(d):

    import models

    merge=False
    if d.has_key('usereal'):
        if d['usereal']:
            if d['hirescat'] != "None":
                if d['savecut']:
                    merge = True

    models.do_models(
        d['hiresfile'],
        d['loresfile'],
        d['modelscat'],
        d['modelsdir'],
        d['cutoutcat'],
        d['cutoutdir'],
        d['culling'],
        d['relscale'],
        d['kernelfile'],
        d['kerntxt'],
        merge
    )
