
#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import futil, shutil
import cull
from subprocess import call
from astropy.io import fits
import numpy as np


class cut_them_worker(object):
	def __init__(self,truecatfile,imgfile,segfile,outdir,outcat,subbckg,noclean=None,
		normalize=None,colkeys=None,mycols=None): 
		self.counter = 0
		self.truecatfile = truecatfile
		self.imgfile = imgfile
		self.segfile = segfile
		self.outdir = outdir
		self.outcat = outcat
		self.noclean = noclean
		self.normalize = normalize
		self.colkeys = colkeys
		self.mycols = mycols
                self.subbckg = subbckg
			
	def __call__(self): #,line):
		
		#######
		# Call C code to create cutouts.
		# To parallelize, it will be sufficient to split the input catalog.
		cmd=' '.join(['tphot_cutout',self.imgfile, self.segfile, self.truecatfile, self.outdir, self.outcat, str(self.subbckg)])
		print cmd
		stat=call(cmd,shell=True)
		if not (stat==0):
			print "ERROR during execution of tphot_cutout routine."
			print "Aborting."
			sys.exit()
				

def cut_them(catfile,imgfile,segfile,outdir,outcat,culling,kernel,scale_factor,
	     loresfile,txt,subbckg,nproc=1,noclean=None,normalize=None):
	assert os.path.isfile(catfile), "Catalog file does not exist: "+catfile
	assert os.path.isfile(imgfile), "Detection file does not exist: "+imgfile
	assert os.path.isfile(segfile), "Segmentation file does not exist: "+segfile

	# If necessary, create outdir
	if not outdir.endswith('/'): outdir+='/'
	if not os.path.isdir(outdir):
                os.mkdir(outdir)
        if os.path.exists(outcat+'*'):
                os.remove(outcat+'*')
	
	# If required, do some culling BEFORE cutting 
	truecatfile=catfile
	if culling:
		"""select sources and print new culled catfile"""
		newcatfile=catfile+"_CULLED"
		if os.path.exists(newcatfile):
			os.remove(newcatfile)
		newcat = open(newcatfile,'w')
                cull.cull(loresfile,catfile,imgfile,newcat,scale_factor,kernel,txt)
		truecatfile=newcatfile
                newcat.close()
        else:
                if os.path.exists(outcat+"_CULLED"):
                        os.remove(outcat+"_CULLED")
                
	worker = cut_them_worker(truecatfile,imgfile,segfile,outdir,outcat,subbckg,noclean=noclean,normalize=normalize)
	worker.__call__()

    

