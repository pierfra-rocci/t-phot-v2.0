
#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.


import os, glob, errno
from shutil import copyfile
import cull

def do_models(HRI,LRI,modelscat,modelsdir,cutoutcat,cutoutdir,culling,scale_factor,kernel,txt,merge):

    culs=glob.glob(modelsdir+'*CULLED*')
    if culs:
        for obj in culs:
            os.remove(obj)

    # If required, do some culling BEFORE cutting 
    truecat=modelscat
    if culling:
            """select sources and print new culled catfile"""
            newcat=modelscat+'_CULLED'
            newc = open(newcat,'w')
            cull.cull(LRI,modelscat,HRI,newc,scale_factor,kernel,txt)
            newc.close()
            truecat=newcat

    # if necessary, merge catalogs
    if merge:
        cat1=truecat
        cat2=cutoutcat
        o=open("merged_temp.cat",'w')
        c1=open(cat1,'r')
        c2=open(cat2,'r')
        t=c1.readlines()
        for line in t:
            o.write(line)
        t=c2.readlines()
        for line in t:
            if not line.startswith("#"):
                o.write(line)
        o.close()
        os.rename("merged_temp.cat",cat2)

        wd=os.getcwd()+'/'
        mdir=os.path.abspath(modelsdir)+'/'
        os.chdir(cutoutdir)
        mods=glob.glob(mdir+'cut-*.fits')
        for obj in mods:
            junk,nam=obj.split('cut',1)
            nam='cut'+nam
            try:
                os.symlink(obj,nam)
            except OSError as e:
                if e.errno==errno.EEXIST:
                    os.remove(nam)
                    os.symlink(obj,nam)
        os.chdir(wd)
