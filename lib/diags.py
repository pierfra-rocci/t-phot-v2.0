import os, glob, shutil
import numpy as N
import fitsimage
import sextutils
import set_covar_info

def collage(shape,dir,exclist,bigwcs=None,mult=None,getflags=None,finder=False,
            skydim=None):
    """ Build a collage from a set of postage stamps. 
    shape = tuple defining dimensions of collage
    dir = directory where all postage stamps are to be found
    bigwcs = defines the wcs of the output collage: take it from the image
             that you're trying to replicate, generally loresimage
    mult = a dictionary of (id, scale factor) pairs. If present,
           multiply each cutout by this scale factor before inserting into collage.
    getflags : if set, also return a dictionary of the MAXFLAG keyword value
    If finder is set to True, then a "finder chart" version of the collage
    will also be created, with the object ID placed in the pixel corresponding
    to the central pixel of the template.
    """

    print "Excluding IDs:",exclist

    if getflags:
        flagdict={}
    collage=N.zeros(shape,N.float32)
    if finder:
        idmap=N.zeros(shape,N.int32)
    if skydim:
        skymap=N.zeros(shape,N.float32)
        dimthresh=skydim
    
    if mult is None: # drive it from the directory
        all=glob.glob(os.path.join(dir,'*.fits'))
    else: #drive it from the list of templates
        all=[os.path.join(dir,'mod-'+str(id)+'.fits') for id in mult.keys()]

            
    for thing in all:
        try:
            sub=fitsimage.Image(thing)
            id,ext=os.path.splitext(os.path.basename(thing))
            if "-" in id:
                param,id=id.split("-",1)

            if bigwcs is None:
                thebox=sub.wcs_box
                print "No WCS supplied for collage: assuming crpix1/2=0.5"
            else:
                thebox=bigwcs.localpos(sub.wcs_box)
              #  thebox=bigwcs.xy2xy_box(sub.wcs_box,sub.wcs,nocorner=1)
                thebox.int()
            #	collage[sub.wcs_box.Slice()]+=sub.d

            if finder:
                x,y=bigwcs.xy2xy(sub.wcs_box.Xdim()/2,
                                 sub.wcs_box.Ydim()/2,
                                 sub.wcs)
                idmap[int(y),int(x)]=int(id)

            if skydim:
                xdim,ydim=sub.h['naxis1'],sub.h['naxis2']
                if (xdim+ydim)/2 >= dimthresh:
                    skybox=N.zeros(sub.d.shape,sub.d.dtype)
                    skybox[ydim/4:3*ydim/4,xdim/4:3*xdim/4]=sub.h['obskysub']
                    skymap[thebox.Slice()]+=skybox

            if getflags:
                try:
                    flagdict[id]=sub.h['maxflag']
                except KeyError:
                    flagdict[id]=-99

            if mult is not None:
                sub.d=sub.d*float(mult[id])

            try:
                if not (int(id) in exclist):
                    collage[thebox.Slice()]+=sub.d
            except ValueError,e:
                print "Object %s, %s"%(id,str(e))
                print thebox

        except IOError, e:
            print "Object %s, %s"%(id,str(e))

    #Return the correct set of things
    ans={}
    ans['collage']=collage
    if getflags:
        ans['flag']=flagdict
    if finder:
        ans['id']=idmap
    if skydim:
        ans['sky']=skymap

    if len(ans) > 1:
        return ans
    else:
        return ans['collage']

#-----------------------------------------------------------------------------

def getbest(file,basis='rcell'):
    """ Produce a version of the TPHOT output catalog file that contains
    only one record for every object. 'Best' is defined as 'closest to 
    cell center'. The total number of solutions for each object is appended
    to the end of the line.
    Depends on hardcoded information about the structure of the output files."""

    outfile=file+'_best'
    out=open(outfile,'w')
    d={}
    count={}
    f=open(file)
    cat=sextutils.se_catalog(file)
    out.write(cat._header)    

    #Build a dictionary indexed by object id and cell distance
    for k in cat:
        
        fl=cat.fitqty[k] 
        if basis == 'rcell':
            value=cat.rcell[k]
        elif basis == 'error':
            value=cat.fitquerr[k]
        else:
            raise ValueError, """
            Invalid option for basis keyword:
               option received = %s,
               valid choices are 'rcell','error'"""%basis
        ### this if statement is here to include more than one entry
        ### for each key!
        if d.has_key(cat.objectid[k]): 
            d[cat.objectid[k]][value] = k #cat.line(k)
            count[cat.objectid[k]]+= 1
        else: ### if it is the first entry for this key.
            d[cat.objectid[k]]={value:k} #cat.line(k)}
            count[cat.objectid[k]] = 1
	
    #Pick out the best values
    best={}
    for key in d.keys():
        ### this means: take the minimum of the keys in the dictionary 
        ### which is the object d[key] corresponding to key in d
	best[key]=d[key][min(d[key].keys())]

    #Print them, in object number order, out to a new file
    ordered_list = [int(key) for key in best.keys()]
    ordered_list.sort()    

    #Add a header line
    try:
        ncols=cat._ncolumns+1
        out.write("# %2d   NumFits     Number of fits for this object\n"%ncols)
        
        #Now write out the data          
        sf={}
        for key in ordered_list:
            k=best[key]
            lina=cat.line(k)
            if "nan" in lina:
                lina=lina.replace("nan","1.000000e20")
            out.write("%s  %d\n"%(lina[:-1],count[key]))
            sf[str(key)]=cat.fitqty[k]
        out.close()
        return sf

    except UnboundLocalError, e:
        msg="No catalog entries were found in %s. Task aborting."%file
        raise ValueError(msg)
			 
#-----------------------------------------------------------------------------

def addflags(bestname,flagdict):
    """ Add a column containing the maximum flag value to the best
    catalog """

    f=open(bestname)
    outname=bestname+'_plus'
    out=open(outname,'w')

    hdrdone=False
    
    for line in f:
        if line.startswith('#'):
            out.write(line)
            pound,num,junk=line.split(None,2)
        else:
            if not hdrdone:
                #Add the new header line
                newnum=int(num)+1
                out.write('# %2d   MaxFlag     Max flag value in object template\n'%newnum)
                hdrdone=True
            #Add the flag value
            id,junk=line.split(None,1)
            out.write("%s  %5d\n"%(line[0:-1],flagdict[id]))

    #rename so we only have one best file
    out.close()
    os.remove(bestname)
    os.rename(outname,bestname)

# ---------------------------------------------------------------------------------------------- #

def main(tphotcat, modelfile, loresfile, templatedir, templatecat, tphotcovar, exclfile, residstats, writecovar, inputcats):
    
    from tphot_resid_stats import do_resid_stats
    import fitsimage, futil
    from adapt_cats import adapt

    excludelist=[]
    if exclfile:
        if not os.path.exists(exclfile):
            print "**** WARNING:",exclfile,"not found!"
            print "**** Will produce model and residual images"
            print "**** including all fitted sources"
        elif exclfile=='none':
            pass
        else:
            excl=open(exclfile)
            exclude=excl.readlines()
            for line in exclude:
                excludelist.append(int(line))

    sf=getbest(tphotcat)
    collage_file=modelfile
    if os.path.exists(collage_file):
        os.remove(collage_file)
    lores=fitsimage.Image(loresfile)
    products = collage(lores.d.shape,templatedir,excludelist,
                       bigwcs=lores.wcs,mult=sf,
                       getflags=True,finder=True)
    outcoll=products['collage']
    futil.writeimage(outcoll,collage_file,header=lores.h)
    resid=lores.d - outcoll
    residname='resid_'+os.path.basename(collage_file)
    if len(os.path.dirname(collage_file)) != 0:
        residname=os.path.join(os.path.dirname(collage_file),
                               residname)
    if os.path.exists(residname):
        os.remove(residname)
    futil.writeimage(resid,residname,header=lores.h)
    addflags(tphotcat+'_best', products['flag'])
    idmapname=residname.replace('resid','idmap')
    if os.path.exists(idmapname):
        os.remove(idmapname)    
    futil.writeimage(products['id'],
                     idmapname,
                     header=lores.h)

    tphotcat_best=tphotcat+"_best"
    if writecovar:
        # calculate covariance index and append to tphot cat file
        tphotcovardir=tphotcovar+"_covardir"
        tphotcat_new=tphotcat_best+"_plus_covar"
        set_covar_info.covar(tphotcat_best,tphotcovardir)

        # tphot catalog without covariance index is now called tphotcat+"_OLD"
        os.system("mv %s %s" % (tphotcat_best,tphotcat_best+"_OLD") )
        os.system("mv %s %s" % (tphotcat_new, tphotcat_best) )

        Nhead=16

    else:

        Nhead=14

#    else: # writecovar:
#        os.remove(tphotcovar)
#        shutil.rmtree(tphotcovardir)
    
    # If required, do statistics on residual image
    if residstats==True:
        print "Evaluating statistics on residual image"
        do_resid_stats(tphotcat_best,templatedir+'/mod-',templatecat,residname)
        print "Done, results are in file residual_stats.cat"


    # Check if output catalog has different number of objects 
    # than input catalog(s) and if so make it good
    fullcat=[]
    for catt in inputcats:

        try:
            i=open(catt)
            il=i.readlines()
            for l in il:
                if l[0]!="#":
                    fullcat.append(l)
            i.close()
        except:
            print "WARNING: No catalog",catt,"found. Skipping"

    if N.size(fullcat)==0:
        print "WARNING: no input catalogs given/found. Skipping catalog reshaping"
        return

    fo=tphotcat_best
    o=open(fo)    
    ol=o.readlines()
    fout=tphotcat_best+'_COMPLETE'
    o.close()
    
    nini=0
    for iii in fullcat:
        if iii[0]!='#':
            nini+=1
    if (nini != N.size(ol)-Nhead):
        print ("Finalizing output catalog including previously rejected sources")

        stat=adapt(fullcat,fo,fout,Nhead)
        if stat!=0:
            print "WARNING: could not reshape final catalog. Skipping"
        else:
            print "Final catalog is",fout

    else:
        shutil.copyfile(tphotcat_best,fout)
    

    

