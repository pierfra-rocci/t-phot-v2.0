#!/usr/bin/env/python

"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""
 
import string, sys, os
import time
import tphot_control

#---------------------------------------------------

class Stage:
    """ Parent class for a processing stage """

    def __init__(self,name,exestring,*params,**kwds):
        self.name=name
        self.exestring=exestring
        self.params=params
        self.keywords=kwds
        self.status=None
        self.retvalue=None

    def __str__(self):
        return self.name
    
    def run(self):
        print "Running %s, starting time: %f"%(self.name,time.time())
        self.retvalue = self.exestring(*self.params,**self.keywords)
        self.status=0

#---------------------------------------------------

class Pipeline(list): #list = paramfile

    def __init__(self,name,catch=False):
        self.name=name
        self.catch=catch
        
    def run(self):
        if self.catch:
            self.run_catchall()
        else:
            for stage in self:
                stage.run()

    def run_catchall(self):
        for stage in self:
            try:
                stage.run()
            except Exception, e:
                print '\t',e,': Proceeding to next stage'
                
    def show(self): # Print out stages

        d={True:'continue',False:'stop'}
        header='%s: Processing will %s if a task raises an exception '
        print header%(self.name, d[self.catch])
        for stage in self:
            print "\t",stage

    def status(self):
        for stage in self:
            print "%s: %s"%(stage.name,stage.status)

    def answers(self):
        print "\n %s results:"%self.name
        for stage in self:
            print "\t%s: %s"%(stage.name,stage.retvalue)
        print "\n"

#---------------------------------------------------

def readparam(configfile,paramdict={},preservecase=False):

    """ Routines for reading and interpreting T-PHOT input parameter file.
    __authors__ =  'Henry C. Ferguson, Emiliano Merlin'
    __version__=  '1.0.0'
    """ 

    f=open(configfile)
    for line in f:
        line=line.strip() #trim whitespace
        if not line.startswith('#') and len(line) > 0:
            #strip out trailing comments, if any
            try:
                ll,junk = line.split('#',1)
            except ValueError:
                ll=line
            #replace commas with spaces, then split on spaces
            ll=ll.replace(',',' ')
            try:
                keyword,vals=ll.split(None,1)
            except:
                print "Error in paramfile (bad entry):",ll
                print "Aborting"
                sys.exit()
            if not preservecase:
                keyword=keyword.lower()
            #Do we have multiple values?
            if len(vals.split()) == 1:
                paramdict[keyword] = truval(vals)
            else:
                paramdict[keyword] = []
                for x in vals.split():
                    paramdict[keyword].append(truval(x))

    return paramdict

#---------------------------------------------------

def truval(x):
  """ Return the properly typed value of x """

  boolvals={'true':True, 'false':False, 't': True, 'f': False,
            'yes': True, 'no': False, 'y':True, 'n': False}
  x=x.strip() #strip off leading & trailing whitespace first
  try: #is it an int?
    ans=int(x)
  except ValueError:
    try: #is it a float?
      ans = float(x)
    except ValueError: #is it a literal?
      if x.startswith("'") or x.startswith('"'):
        ans=x[1:-1]
      else:
        try: #is it a boolean?
          ans=boolvals[x.lower()]
        except: #must be a string
          ans=x
  return ans #  returns the new "properly typed" val

#---------------------------------------------------

def StageFactory(StageName,d):
    """ Builds objects of type 'Stage' """

    StageName=StageName.lower() # all in lower case

    # Check kernelfile extension
    if not d.has_key('kerntxt'):
        if ".fits" in d['kernelfile']:
            d['kerntxt']=False
        elif ".txt" in d['kernelfile']:
            d['kerntxt']=True
        else:
            print "Unknown kernel file extension. Aborting"
            sys.exit()

    # Check other things
    if not d.has_key('maxflag'):
        d['maxflag']=64
        print "Setting maxflag = 64"
    if not d.has_key('posframe'):
        d['posframe']='lores'
    if not d.has_key('fitbackground'):
        d['fitbackground']=False
    if not d.has_key('savecut'):
        d['savecut']=True
        print "Storing cutouts"
    if not d.has_key('bgconstant'):
        d['bgconstant']=0.0
    if not d.has_key('fit_loc_bkgd'):
        d['fit_loc_bkgd']=0
    if not d.has_key('mk_ext'):
        d['mk_ext']=None
    if not d.has_key('kernellookup'):
        d['kernellookup']='None'

    if StageName == 'cutout':
        if d.has_key('nproc'):
          nproc = d['nproc']
        else:
          nproc = 1
        if not d.has_key('subbckg'):
            d['subbckg']=True

        args=(d['hirescat'],
              d['hiresfile'],
              d['hiresseg'],
              d['cutoutdir'],
              d['cutoutcat'],
              d['kernelfile'],
              d['relscale'],
              d['loresfile'],
              d['kerntxt'],
              d['subbckg'])
        kwargs={'normalize':d.get('normalize'),
                'culling':d.get('culling',True),
                'nproc':nproc}
               
        return Stage(StageName,
                     tphot_control.do_cutouts,
                     *args,
                     **kwargs)
                 
    if StageName == 'convolve':
        if d.has_key('nproc'):
          nproc = d['nproc']
        else:
          nproc = 1
        if not d.has_key('subbckg'):
            d['subbckg']=True

        if d.has_key('flagfile'):
          flagfile = d['flagfile']
        else:
          flagfile = None

        kwargs={'flagfile':d.get('loresflag'), 
                    'maxflag':d.get('maxflag'),
                    'shiftfile':d.get('ddiagfile'), 
                    'nproc':nproc}

        return Stage(StageName,
                     tphot_control.do_convolve,
                     d,**kwargs)
                         

    if StageName=='fit':

        return Stage(StageName,
                     tphot_control.do_fit,
                     d)

                         
    if StageName=='diags':

        if not d.has_key('exclfile'):
            d['exclfile']=None
        if not d.has_key('residstats'):
            d['residstats']=False
        inputcats=[]
        if d['usereal']:
            if d.has_key('hirescat'):
                if os.path.exists(d['hirescat']):
                    inputcats.append(d['hirescat'])
        if d['useunresolved']:
            if d.has_key('poscat'):
                if os.path.exists(d['poscat']):
                    inputcats.append(d['poscat'])
        if d['usemodels']:
            if d.has_key('modelscat'):
                if os.path.exists(d['modelscat']):
                    inputcats.append(d['modelscat'])

        args=(d['tphotcat'],
              d['modelfile'],
              d['loresfile'],
              d['templatedir'],
              d['templatecat'],
              d['tphotcovar'],
              d['exclfile'],
              d['residstats'],
              d['writecovar'],
              inputcats)

        return Stage(StageName,
                     tphot_control.do_diags,
                     *args)
    

    if StageName=='dance':

        return Stage(StageName,
                     tphot_control.do_dance,
                     d) 


    if StageName=='plotdance':

        args=(d['ddiagfile'],)
        
        return Stage(StageName,
                     tphot_control.do_plotdance,
                     *args)

    if StageName=='archive':

        return Stage(StageName,
                     tphot_control.do_archive,
                     d)


    if StageName=='positions':

        return Stage(StageName,
                     tphot_control.do_positions,
                     d)

    if StageName=='models':

        return Stage(StageName,
                     tphot_control.do_models,
                     d)

    #If we get here, stage is undefined
    print "Sorry, %s stage has not yet been defined"%StageName
    return

#---------------------------------------------------
