
def get_pars(parfile):
    """ Returns a dictionary that maps keywords to values for a parameter
    file written with lines of the form
    #This is a comment
    keyword      keyword_value """

    pars={}
    f=open(parfile,'r')
    text=f.readlines()
    for line in text:
	if not line.startswith("#"):
	    key, val = line.split()
	    pars[key]=val
    f.close()
    return pars

#-----------------------------------------------------------------------

def catkeys(catfile):

     """ Returns a dictionary that maps keywords to column numbers for a
     catalog file with a SExtractor-style header."""
     thecat=open(catfile)
     header={}
     while 1:
	 line=thecat.readline()
	 if line.startswith('#'): 
	     cols=line.split()
	     header[cols[2]] = int(cols[1])
	 else:
	     thecat.close()
	     return header

#-----------------------------------------------------------------------

def parseline(line, lookup, keywords):
    """ Return column values based on keys in a dictionary from catkeys, above"""
    if line.startswith('#'):
	print "Header line: skipped"
	return None
    else:
	cols=line.split()
	vals=[]
	for word in keywords:
	    vals.append(cols[lookup[word]-1]) #Python counts from 1
	return vals

#----------------------------------------------------------
	
def writeimage(image, filename, header=None, comment=None):

    from astropy.io import fits
    #fits.writeto(filename, image, header)
    #if (comment is not None):
    #    f=fits.open(filename,"update")
    #    f[0].header.add_comment(comment)
    #    f.flush()
    #    f.close()

    hdulist=fits.HDUList()
    hdu=fits.PrimaryHDU()
    hdu.data=image
    if (header is not None):
        hdu.header=header
    if (comment is not None):
        hdu.header.add_comment(comment)
    hdulist.append(hdu)
    hdulist.writeto(filename)

#----------------------------------------------------------

def make_tphot_cat(infile, outfile,mycoldefs=None):
    #Parse the catalog file header.
    colkeys=catkeys(infile)
    #Specify the columns we're going to want.
    if mycoldefs:
        lines=open(mycoldefs).read()
        mycols=lines.split()
    else:
        mycols=['NUMBER', 'X_IMAGE', 'Y_IMAGE','FLUX_ISO']
    #Open the output file we're going to create for TPHOT
    tphotcat=open(outfile,"w")
    #Loop through the file
    f=open(infile,"r")
    counter=0
    for line in f:
	if not line.startswith('#'):
	    #process the line
	    counter+=1
	    id,xc,yc,flux = parseline(line.strip(),colkeys,mycols)
	    print >>tphotcat, id,xc,yc,flux

    f.close()
    tphotcat.close()

#----------------------------------------------------------
