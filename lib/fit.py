"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

from subprocess import call
from astropy.io import fits,ascii
import numpy as np
import os,sys,glob
import multiprocessing
import time

class fit_worker(object):

    def __init__(self,fitpars):
        self.fitpars=fitpars

    def __call__(self,me):

        """
        try:
            fitlog,param=self.fitpars.split(".",1)
        except:
            fitlog=self.fitpars
        """
        fitlog='tphot_fitter'
        self.fitlog=fitlog+"_"+str(me)+".log"
        #######
        # Call C++ code to do fit.
        print "Start: ",time.time()
        #cmd=' '.join(['tphot_fitter',self.fitpars,str(me),'>',self.fitlog])
        cmd=' '.join(['tphot_fitter',self.fitpars,'>',self.fitlog])
        print cmd
        fit_stat = call(cmd,shell=True)

        if fit_stat != 0:
            #print fit_msg
            raise ValueError, "TPHOT failed: exit status %d"%os.WSTOPSIG(fit_stat)       
        print "End: ",time.time()
        #######

#-----------------------------------------------------------

class covar_worker(object):
    def __init__(self,fitcovar,fitcovardir):
        self.fitcovar=fitcovar
        self.fitcovardir=fitcovardir
        self.fitcovarverb='python covar_proc.py'

    def __call__(self):

        import covar_proc
        
        print "Working on covariance stuff..."
        covar_proc.gen_covar_fits(self.fitcovar,self.fitcovardir)

        #######
        # Covariance stuff
        #cmd=' '.join([self.fitcovarverb,self.fitcovar,self.fitcovardir])
        #call(cmd,shell=True)
        ####### 

#-----------------------------------------------------------

def excerpt_fit_pars(d):

   # To fit local background with templates    
    import make_bkgd_templates
    if d['fit_loc_bkgd']:
        shap='c'
        make_bkgd_templates.bkgd_templ(shap,int(d['fit_loc_bkgd']),d['templatedir'],d['templatecat'])

    # Order sources 
    if os.path.exists(d['templatecat']+'_culled'):
        catal=d['templatecat']+'_culled'
    else:
        catal=d['templatecat']
    cat=ascii.read(catal)
    cat.sort(cat.colnames[8])
    cat.reverse()          
    testfile=catal+'_temp'
    ascii.write(cat,testfile)
    orderedfile=catal+'_FluxSorted'
    os.rename(testfile,orderedfile)

    if d.has_key('rmscheck'):
        if d['rmscheck']:
            o=open(testfile,'w')
            l=open(orderedfile)
            lines=l.readlines()
            print "Checking RMS map to exclude unreliable sources from fit"
            rmsf=fits.open(d['loreserr'])
            rms=rmsf[0].data
            i=0
            excluded=0
            for line in lines:
                if i<np.size(cat):
                    if line[0].isdigit():
                        if rms[int(cat[i][2]-1),int(cat[i][1])-1] < 1000:
                            o.write(line)
                        else:
                            excluded+=1
                        i+=1
            if excluded:
                print "%d sources have been excluded due to exceedingly high RMS"%excluded
            l.close()
            o.close()
            os.rename(testfile,orderedfile)

    # Add fluxpriors colums
    if d.has_key('fluxpriorscat'):
        if (d['fluxpriorscat']!='none'):
            print "**** INCLUDING FLUX PRIORING  ****"
            if not os.path.exists(d['fluxpriorscat']):
                print "ERROR: file ", d['fluxpriorscat'], "not found."
                goon=raw_input("Continuing without flux prioring? [y]/n")
                if goon!='n':
                    lll=open(d['fluxpriorscat'],'w')
                    lll.write('0 0 0\n')
                    lll.close()
                else:
                    print "Aborting. Bye"
                    sys.exit()
            catflux=ascii.read(d['fluxpriorscat'])
            l=open(orderedfile)
            lines=l.readlines()
            o=open(testfile,'w')
            for line in lines:
                if line[0].isdigit():
                    ids,junk=line.split(' ',1)
                    IDS=catflux[catflux.colnames[0]].tolist()
                    try:
                        idx=IDS.index(int(ids))
                        line=line.rstrip('\n')+' '+str(catflux[catflux.colnames[1]][idx])+' '+str(catflux[catflux.colnames[2]][idx])+' '+str(catflux[catflux.colnames[3]][idx])+'\n'
                    except:
                        line=line.rstrip('\n')+' 0 0.0000 1.0000\n'
                    o.write(line)
            os.remove(orderedfile)
            os.rename(testfile,orderedfile)
        else:
            l=open(orderedfile)
            lines=l.readlines()
            o=open(testfile,'w')
            for line in lines:
                if line[0].isdigit():
                    line=line.rstrip('\n')+' 0 0.0000 1.0000\n'
                    o.write(line)
            os.remove(orderedfile)
            os.rename(testfile,orderedfile)
    else:
        l=open(orderedfile)
        lines=l.readlines()
        o=open(testfile,'w')
        for line in lines:
            if line[0].isdigit():
                line=line.rstrip('\n')+' 0 0.0000 1.0000\n'
                o.write(line)
        os.remove(orderedfile)
        os.rename(testfile,orderedfile)

    # Fitting method
    d['cell_overlap']=0
    d['dithercell']="False"
    if (str(d['fitting'])=='coo'):
        d['cell_xdim']=0
        d['cell_ydim']=0
    elif(str(d['fitting'])=='0'):
        d['cell_xdim']=0
        d['cell_ydim']=0
    elif(str(d['fitting'])=='single'):
        d['cell_xdim']=-1
        d['cell_ydim']=-1
    elif(str(d['fitting'])=='-1'):
        d['cell_xdim']=-1
        d['cell_ydim']=-1
    elif(str(d['fitting'])=='single!'):
        d['cell_xdim']=-10
        d['cell_ydim']=-10
    elif(str(d['fitting'])=='-10'):
        d['cell_xdim']=-10
        d['cell_ydim']=-10
    elif("," in str(d['fitting'])):
        d['cell_xdim']=int(d['fitting'][0])
        d['cell_ydim']=int(d['fitting'][1])
        d['cell_overlap']=int(d['fitting'][2])
        d['dithercell']="True"


    # Write cmd for fitter
    cmd=''
    cmd+=str(d['loresfile'])
    cmd+=' '+str(catal)
    cmd+=' '+str(d['templatedir'])
    cmd+=' '+str(d['tphotcat'])
    cmd+=' '+str(d['tphotcell'])
    cmd+=' '+str(d.get('hiresseg','None'))
    cmd+=' '+str(d['errtype'].upper())
    cmd+=' '+str(d['loreserr'])
    cmd+=' '+str(d['rmsconstant'])
    cmd+=' '+str(d['cellmask'])
    cmd+=' '+str(d['maskfloor'])
    cmd+=' '+str(d['dithercell'])
    cmd+=' '+str(d.get('loresflag','None'))
    cmd+=' '+str(d['maxflag'])
    cmd+=' '+str(d['fitbackground'])
    cmd+=' '+str(d['bgconstant'])
    cmd+=' '+str(d.get('writecovar','True'))
    cmd+=' '+str(d['tphotcovar'])
    cmd+=' '+str(d['cell_xdim'])
    cmd+=' '+str(d['cell_ydim'])
    cmd+=' '+str(d['cell_overlap'])
    cmd+=' '+str(d['threshold'])
    cmd+=' '+str(d['linsyssolver'].upper())
    cmd+=' '+str(d['clip'])
    cmd+=' '+str(d['nproc'])
    cmd+=' '+str(d.get('zphr',0.0))
    cmd+=' '+str(d.get('zplr',0.0))
    cmd+=' '+str(orderedfile)
    cmd+=' '+str(1)
    if d.has_key('apertures'):
        cmd+=' '+str(d['tphotcat'])+'_apertures'
        naper=np.size(d['apertures'])
        cmd+=' '+str(naper)
        for ap in range(naper):
            cmd+=' '+str(d['apertures'][ap])
    return cmd    


#---------------------------------------------------------------------------

def minimize(d):
    """ Regenerate the old-style parfile, and spawn to run the fit.
    Rely on the PATH being properly set to find the executable"""

    if not d.has_key('nproc'):
        nproc = 1
    else:
        nproc=d['nproc']
    if (nproc>1):
        if (nproc>multiprocessing.cpu_count()):
            print "WARNING: Desired number of CPUs is too high, reducing"
            nproc=multiprocessing.cpu_count()
        if not (nproc%2==0):
            print "WARNING: Need an even number of CPUs, reducing"
            nproc=nproc-1
        print "Working with", nproc, "CPUs"
    d['nproc']=nproc

    print "Nproc = ", nproc

    fitpars=excerpt_fit_pars(d)
    
    """
    # Parallel
    worker = fit_worker(fitpars)
    pool=multiprocessing.Pool(processes=nproc)
    p=pool.map_async(worker,range(1,nproc+1))
    results = p.get() 
    pool.close()
    pool.join()
    """
    # Single CPU
    worker = fit_worker(fitpars)   
    worker.__call__(1)
    
    print "Done with the fit."

    outfile=d['tphotcat']
    outcell=d['tphotcell']
    if (d['writecovar']):
        outcovar=d['tphotcovar']
    
    if (nproc>1):
        print "Joining catalogs..."

        # Join output catalogs
        out=open(outfile,'w')
        cell=open(outcell,'w')
        if (d['writecovar']):
            covar=open(outcovar,'w')

        for i in range(1,nproc+1):

            outp=open(outfile+"_"+str(i),'r')
            lines=outp.readlines()
            if (i==1):
                for l in lines: 
                    out.write(l)
            else:
                for l in lines:
                    if not ("#" in l):
                        out.write(l)
            outp.close()
            os.remove(outfile+"_"+str(i))

            outp=open(outcell+"_"+str(i),'r')
            lines=outp.readlines()
            if (i==1):
                for l in lines: 
                    cell.write(l)
            else:
                for l in lines:
                    if not ("#" in l):
                        cell.write(l)
            outp.close()
            os.remove(outcell+"_"+str(i))

            if (d['writecovar']):
                outp=open(outcovar+"_"+str(i),'r')
                lines=outp.readlines()
                for l in lines: 
                    covar.write(l)
                outp.close()
                os.remove(outcovar+"_"+str(i))
            
        out.close()
        cell.close()
        if (d['writecovar']):
            covar.close()
            
            
    else:
        os.rename(outfile+"_1",outfile)
        os.rename(outcell+"_1",outcell)
        if (d['writecovar']):
            os.rename(outcovar+"_1",outcovar)

    # If apertures, order catalog
    aps=glob.glob('*_apertures_*')
    for ap in aps:
        c=ascii.read(ap)
        c.sort(c.colnames[0])
        ascii.write(c,'temp')
        os.rename('temp',ap)

    if (d['writecovar']):
        # produce covariance matrix for each object in fits format
        # files are placed under a subdirectory called tfitcovar+'_covardir'
        # where tfitcovar is the name of the covariance matrix (ascii file)
        # given in the tfit parameter file

        fitcovar=d['tphotcovar']
        fitcovardir=fitcovar+'_covardir'

        worker = covar_worker(fitcovar,fitcovardir)
        worker.__call__()

