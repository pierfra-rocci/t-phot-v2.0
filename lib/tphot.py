#! /usr/bin/env python

#Copyright 2015 Emiliano Merlin
#
#This file is part of T-PHOT.
#
#T-PHOT is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#T-PHOT is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.


import types
import init
import time
import os, glob, shutil

def run(parfile,args,interactive):

    print "Start: ",time.time(),time.clock()
    
    #---------------------------------------------------
    #Read param file and create a dictionary with params:

    d=init.readparam(parfile,{}) 

    n=0
    while(n<np.size(args)):
        arg=args[n]
        if arg.startswith('-'):
            try:
                kw=arg.strip('-')
                kw=kw.strip("'")
                kw=kw.strip('"')
                d[kw]=args[n+1]
                print "Keyword",kw,"has now value",args[n+1]
            except:
                print "Error in parsing command line arguments"
                sys.exit()
            n+=1
        n+=1

    if not d.has_key('order'):
        print "ERROR: No pipeline specified in parameter file."
        print "Maybe keyword 'order' is commented out?"
        print "Aborting."
        sys.exit()

    d['interactive']=interactive

    #---------------------------------------------------
    #Construct the pipeline:

    pipe=init.Pipeline(parfile) 

    # Catch for single-stage pipelines: change string into list
    if type(d['order']) == types.StringType:
        d['order'] = [d['order']]

    # Check which type of hi-res catalogs are used 
    # and consequently modify pipeline
    hires = False
    models = False
    pos = False

    if d.has_key('usereal'): # standard hi-res cutouts
        if d['usereal']:
            hires = True
            if not d.has_key('hirescat'):
                print "Real priors selected but no catalog given. Aborting"
                sys.exit()
    if d.has_key('usemodels'): # Galfit-like model priors
        if d['usemodels']:
            models = True
            if not d.has_key('modelscat'): # Galfit-like model priors
                print "Model priors selected but no catalog given. Aborting"
                sys.exit()  
    if d.has_key('useunresolved'): # point-source priors
        if d['useunresolved']:
            pos = True
            if not d.has_key('poscat'): # Galfit-like model priors
                print "Point-like priors selected but no catalog given. Aborting"
                sys.exit()

    if (("priors" in d['order']) or ("standard" in d['order'])):
        if not (hires or models or pos):
            print "Please select prior types in paramfile. Aborting"
            sys.exit()

    secondpass=False
    if "standard2" in d['order']:
        secondpass=True
        d['order']=[]
        d['order'].insert(0,'archive')
        d['order'].insert(0,'diags')
        d['order'].insert(0,'fit')
        if (pos):
            d['order'].insert(0,'positions')
        if (hires or models):
            d['order'].insert(0,'convolve')
        d['multikernels']=True
    elif "standard" in d['order']:
        d['order']=[]
        d['order'].insert(0,'plotdance')
        d['order'].insert(0,'dance')
        d['order'].insert(0,'diags')
        d['order'].insert(0,'fit')
        if (pos):
            d['order'].insert(0,'positions')
        if (hires or models):
            d['order'].insert(0,'convolve')
            d['order'].insert(0,'priors')
    elif "FIRstandard2" in d['order']:
        secondpass=True
        d['order']=[]
        d['order'].insert(0,'archive')
        d['order'].insert(0,'diags')
        d['order'].insert(0,'fit')
        if (pos):
            d['order'].insert(0,'positions')        
        d['multikernels']=True
    elif "FIRstandard" in d['order']:
        d['order']=[]
        d['order'].insert(0,'plotdance')
        d['order'].insert(0,'dance')
        d['order'].insert(0,'diags')
        d['order'].insert(0,'fit')
        if (pos):
            d['order'].insert(0,'positions')
    elif d['multikernels']:
        secondpass=True

    d_input=d['order'][:]

    if not d.has_key('hiresfile'):
        print "No HRI specified in paramfile. Will abort if needed"
        d['hiresfile']="None"
    if d['hiresfile']=='None':
        print "No HRI specified in paramfile. Will abort if needed"  
    if pos:
        if ((not models) and (not hires)):
            if not d.has_key('kernelfile'):
                d['kernelfile']=d['psffile']

    # Re-organize pipeline
    if "priors" in d_input: 
        # generic prior processing
        print "Re-organizing the pipeline"
        a=d['order'].index('priors')
        d['order']=d['order'][a+1:]
        if "fit" in d['order']:
            a=d['order'].index('fit')
            d['order']=d['order'][a:]
        if (pos):
            if not d.has_key('psffile'):
                print "No LRI PSF specified in paramfile. Aborting"
                sys.exit()
            if d['psffile'] == "None":
                print "No LRI PSF specified in paramfile. Aborting"
                sys.exit()              
            d['order'].insert(0,'positions')
        if (models):
            if not d.has_key('modelsdir'):
                print "No models directory specified in paramfile. Aborting"
                sys.exit()
            if d['modelsdir'] == "None":
                print "No models directory specified in paramfile. Aborting"
                sys.exit()
            if not d['modelsdir'].endswith("/"):
                d['modelsdir']+='/'
            if not "convolve" in d['order']:
                d['order'].insert(0,'convolve')
            d['order'].insert(0,'models')
        if (hires):
            if not d['cutoutdir'].endswith("/"):
                d['cutoutdir']+='/'
            if (models):
                # do cutouts first; catalogs will be merged
                d['order'].insert(0,'cutout') 
            else:
                if not "convolve" in d['order']:
                    d['order'].insert(0,'convolve')
                if not d.has_key('savecut'):
                    d['savecut']=True
                if (("cutout" in d_input) or d['savecut']):
                    # originally wanted to store cutouts so do them
                    d['order'].insert(0,'cutout')
                    print "Changed pipeline:", d['order']

    if "convolve" in d['order']:
        if (models and (not hires)):
            if not d['modelsdir'].endswith("/"):
                d['modelsdir']+='/'
            d['cutoutdir']=d['modelsdir']
            d['cutoutcat']=d['modelscat']
        if not secondpass:
            if (hires and models):
                d['savecut']=True
                if not ("cutout" in d_input):
                    if d['order'][0]!='cutout':
                        d['order'].insert(0,'cutout')
                        print "Changed pipeline:", d['order']

    if "cutout" in d['order']:
        # do cutouts from scratch: if they exist, remove them
        if os.path.exists(d['cutoutdir']):
            shutil.rmtree(d['cutoutdir'])
        if os.path.exists(d['cutoutcat']):   
            os.remove(d['cutoutcat'])


    if d['order']!=d_input:
        print "New pipeline is:",d['order']
        if interactive:
            pip=raw_input("Use this pipeline? [Y]/N=back to input pipeline: ")
            if pip=="N":
                d['order']=d_input

    if d.has_key('multikernels'):
        if d['multikernels']==True:
            d['mk_ext']=False
            if interactive:
                mk=raw_input("Use external multikernel files? Y/[N]: ")
                if mk=="Y":
                    if not (os.path.exists("multikern/") & os.path.exists("multikern/_multikern.txt") & os.path.exists("multikern/_multicutouts.cat")):
                        print "Sorry, can't find multikernel directory"
                        print "and list files multikern/_multikern.txt, multikern/_multicutouts.cat."
                        print "Please check and be sure they exist. Aborting"
                        sys.exit()
                    else:
                        d['mk_ext']=True

    if d.has_key('apertures'):
        if type(d['apertures'])==float:
            d['apertures']=[d['apertures']]
        
    for sname in d['order']:
        pipe.append(init.StageFactory(sname,d))

    #---------------------------------------------------
    #Display & run the pipeline:

    pipe.show()
    pipe.run()
    pipe.answers() 

    #---------------------------------------------------
    #Clean up and finalize:

    print "End: ",time.time(),time.clock()
    print "Finished. Bye"

    #---------------------------------------------------



########################################################
#                  T-PHOT MASTER CODE                  #
########################################################

if __name__ == '__main__':
    """ 
    *********************** T-PHOT ***********************
      A code for PSF-matched multiresolution photometry
                        E. Merlin 2014
                 USAGE: tphot [-i] <paramfile>
         For installation guidelines see README file
     For paramfile explanation see included example files
    For basic explanation refer to included documentation
    ******************************************************
    """
    import sys
    import numpy as np

    print "Version %s of T-PHOT package"%('2.0')
    print "[Add 'I' after parameterfile for interactive run]"

    interactive=False

    if np.size(sys.argv)<=1:
        print "Usage: tphot [-i] <paramfile> [options]"
        sys.exit()

    elif np.size(sys.argv)==2:

        parfile=sys.argv[1] # reads name of parameterfile
        if (parfile=="-h" or parfile=="h" or parfile=="H" or parfile=="-H"):
            print "Usage: tphot [-i] <paramfile> [options]"
            print " >> Options: to input a keyword add '-<keyword> <value>'"
            print " >> E.g.: tphot param.file -order fit -hiresfile newimage.fits" 
            sys.exit()
        if not os.path.exists(parfile):
            print "Usage: tphot [-i] <paramfile> [options]"
            print "No parameter file found. Aborting"
            sys.exit()     

    elif np.size(sys.argv)>=3:
        if (sys.argv[1]=='i' or sys.argv[1]=='I' or sys.argv[1]=='-i' or sys.argv[1]=='-I'):
            #interactive mode
            interactive=True
            parfile=sys.argv[2]
        else:            
            parfile=sys.argv[1]
        if not os.path.exists(parfile):
            print "Usage: tphot [-i] <paramfile> [options]"
            print "No parameter file found. Aborting"
            sys.exit()          
           
    run(parfile,sys.argv,interactive) # run it!

########################################################
