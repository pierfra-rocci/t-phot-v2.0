#!/usr/bin/python

"""
Copyright 2015 Emiliano Merlin

This file is part of T-PHOT.

T-PHOT is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

T-PHOT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with T-PHOT.  If not, see <http://www.gnu.org/licenses/>.
"""

""" Creates a dir with the name of the LRI
and archives all results from pass 1 and 2 """

import sys, os, glob, shutil

def arch(d):

    dirname=d['loresfile']
    dirname,junk=dirname.split('.fits',1)
    dirname.replace(".","_")

    if (d['fitting']=='coo'):
        dirname=dirname+'_CellsOnObjs'
    elif (d['fitting']=='single'):
        dirname=dirname+'_SingleFit'
    elif (',' in d['fitting']):
        XXX,YYY,junk=d['fitting'].split(',',2)
        dirname=dirname+'_Cells'+XXX+'x'+YYY
    elif (d['cell_xdim']==0 or d['cell_ydim']==0):
        dirname=dirname+'_CellsOnObjs'
    elif (d['cell_xdim']<0 or d['cell_ydim']<0):
        dirname=dirname+'_SingleFit'
    elif (d['cell_xdim']>0 and d['cell_ydim']>0):
        dirname=dirname+'_Cells'+str(d['cell_xdim'])+'x'+str(d['cell_ydim'])
    if d['threshold']:
        dirname=dirname+'_Thresh'+str(d['threshold'])
    if d['clip']:
        dirname=dirname+'_CLIP'

    i=0
    dirnametrue=dirname
    while os.path.exists(dirnametrue):
        i+=1            
        dirnametrue=dirname+"_"+str(i)
    os.makedirs(dirnametrue)

    mvlist=[d['templatedir'],d['fitpars'],
            d['ddiagfile'],d['dlogfile'],
            d['kernellookup'],"multikern"]

    lists=[glob.glob("*pass1*"),glob.glob("*pass2*"),
           glob.glob("*collage*"),glob.glob("*resid*"),
           glob.glob("*diags*"),glob.glob("*log"),
           glob.glob("danced*"),glob.glob("*_CULLED"),
           glob.glob("*_danced"),mvlist]

    for name in lists:
        for obj in name:
            if os.path.exists(obj):
                os.rename(obj,dirnametrue+"/"+obj)

