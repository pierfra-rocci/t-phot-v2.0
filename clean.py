#! /usr/bin/env python

""" This script cleans up TPHOT installation """

import os,sys,shutil,glob
from subprocess import call

os.chdir("lib/src")
os.chdir("fitter_src")
cmd = "make clean"
stat=call(cmd,shell=True)
if stat:
    print "Failed cleaning tphot_fitter. Aborting"
    sys.exit()
os.chdir("..")
os.chdir("c_srcs")
stat=call(cmd,shell=True)
if stat:
    print "Failed cleaning C codes. Aborting"
    sys.exit()
os.chdir("../..")

rmvlist=["tphot_fitter", "tphot_cutout", "tphot_convolve", "tphot_dance", "tphot_aper"]
for name in rmvlist:
    if os.path.exists(name):
        os.remove(name)
rmvlist=glob.glob("*pyc")
for name in rmvlist:
    os.remove(name)
os.chdir("..")
if os.path.exists("bin"):
    shutil.rmtree("bin")

